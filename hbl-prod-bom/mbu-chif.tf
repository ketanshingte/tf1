# #LOCALS FOR MBU CHIF APP
# locals {
#   ami_id_mbu_chif_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_chif_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU CHIF APP CLUSTER ############
# ################################################

# #KEYPAIR MBU CHIF CLUSTER
# module "hbl_mbu_chif_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-chif-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCYP2VF8LQ9pScDLP6XcC4KWqT0jvBVmEeYkgKBJcPfNmeZhFXrTkQihbgxYnOGdJIJ6exUan0wAE0nKsutYC00EIOQAIfisoGQOKBeKKW9g7+RiUv/Tj8kn5BWuQnN3vH4wWlNv25K4UDZYKQEQc/3lJMkvlPBFJxrxZuGMgnXMYNXDSuipbNLEaO2Y2Xyber2cexyWB1QVrN5gEgtHS7eEiT6g+I9q6yO6TLkhyAh6jhMYQDCP4cUKlhycw+qp1ODoIw0cAHy0IOLqLEbrckAlrLcs4x6wUHIL49GCZXIap50fxE2UIt1O1eclchImBX6nKmmCFaisNyEtALu2JGskO5EIMJUC3jZzOXcIZbBGi5X8vJDcqnrsm4wFPhXNAJmEuVH8EFgyc7ZyHOIm88df63CBkb6tmksdPRtYNyZWQIhz22v1Dn7jEnRPV7sBtkiR1klWoOIkE2yENks2m66CHdzDaPMCBZ6m5bd4QLRumEN4H5caJmlYAdzcGxdgVU="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-chif-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU CHIF APP CLUSTER
# module "hbl_mbu_chif_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "2"
#   name                    = "${local.aws_account_name}-mbu-chif-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_chif_app
#   instance_type           = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_chif_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_chif_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_chif_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-chif-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-chif-app"}
#   )
# }

# # SECURITY GROUP MBU CHIF APP CLUSTER
# module "hbl_mbu_chif_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-chif-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-chif-app" },
#     { "tier" = "app" }
#   )
# }