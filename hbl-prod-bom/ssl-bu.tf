# # SECURITY GROUP FOR SSL BU ASG
# module "hbl_ssl_bu_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-ssl-bu-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#  ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 9501
#       to_port     = 9501
#       protocol    = "tcp"
#       description = "Opening 9501 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8503
#       to_port     = 8503
#       protocol    = "tcp"
#       description = "Opening 8503 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "ssl-bu-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR SSL BU ASG
# module "hbl_ssl_bu_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-ssl-bu-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDWf8us13RSJtfE4C2eqFuM2Yzw8uPnGzZAQBiyOYqUveTaj0O//gC5Ld930pZ8gDSx8B4u20T7yqTHcxOV46vIItCixUE13RBtEabMHTMCnW0u1j9TNKo5+3pqE5IPUSbklZROrOmrf3cPzG9O21yZO0D8OXG9TUm6E6KkDtUKeXy3xhDlCZ+eE0u/ntMcL3VqKl8X8wbk6/qYhmRdEpuVvQE4Jsb6MUkZcryQ/Q/cVxTDmJWEV/ew285B1HMEM0zrD1yFKYFIPOLiLC/s2ac08MvAorsUGprjItKjLO8MbPSgkOCDZeal1WCYy9q4E0cpI5nWewfgH/MG6e/+7g3CLDrYqxfI5sMaEE6z35jDHCNm1+SKB2hzn7VSRhC2WPbaGAXQr6sI/UxtwrmxPbod/paAZbWbouwXYqYfPbvyylLa3L9k6c1EUCbRE8vJzr+ZFBtbFztknl8hZIXMru+apnhhP+6H3cIa+Nl7In9/kTk5/eGeDB8qAWW79eSxtOM="
#   tags = merge(
#     local.tags,
#     { "application" = "ssl-bu-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR SSL BU ASG
# module "hbl_ssl_bu_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-ssl-bu-app-lt"
#   security_groups    = [module.hbl_ssl_bu_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-ssl-bu-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   instance_type     = "m6a.large"
#   key_name          = module.hbl_ssl_bu_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "ssl-bu-app"},
#           { "teleport" = "hbl-ssl-bu-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "ssl-bu-app"},
#           { "teleport" = "hbl-ssl-bu-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "ssl-bu-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-ssl-bu-app"}
#     )
# }

# #ASG FOR THE SSL BU
# module "hbl_ssl_bu_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-ssl-bu-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 2
#   max_size                  = 0 # TO BE CHANGED TO 2
#   desired_capacity          = 0 # TO BE CHANGED TO 2
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_ssl_bu_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_ssl_bu_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_alb.target_group_arns[1]]

#   tags = merge(local.tags, {
#     	application = "ssl-bu-app"
#   },
#   { "teleport" = "hbl-ssl-bu-app"})
# }