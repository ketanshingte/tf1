# # SECURITY GROUP FOR GNG ASG
# module "hbl_gng_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8080
#       to_port     = 8080
#       protocol    = "tcp"
#       description = "Opening 8080 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR GNG ASG
# module "hbl_gng_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCh9Oc0A9JuLyRGha0qpE2oWeU3OyFMhfyEEXQVCV+nvNRjhihuNF+ZvRbEdMFVcK4sZN+kl/fwYBTFOIcGPydI2yAdTI3HO8YhkGjiJl/EOWtDQOKhKSN/+Li9TW4/yXagowncyj+rziECfBuCytNJOY/UcVJ9iYZidFU7llb8i7X932Bx6ddTcAwDPymtt2OtHcU6IM8WhMSleqLIMsm+CEPEZpeWfzMNUOpHxpApz4yVB2gLW48bVjZfu7bsW40K0i0JScmtEkyYYdzI45FiIY6oOJxGfZzYnMzSdiSoV8dgsvJlWb+H4deUWOkC2pn/XgRgz1ig2+f5IvgnrYz5Pi2oFJ4/VVi2x24s52nxPOyb0CuCiuJYGx0lUORBKLEo84pNFLkigNYJBavVjv2L/uooyNnLG4n15DWhysjGx7zsPBPoKVJKn9CQ22lttk1RiM7Xju8jt7VMbDCNoIxhwRuCC8rqMPJoAxfJmAdp3TEf8zpStKmiLqnaaEe4+q0="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR GNG ASG
# module "hbl_gng_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-gng-app-lt"
#   security_groups    = [module.hbl_gng_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-gng-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   #instance_type     = "c5a.4xlarge" ## TO BE CHANGED TO c6a.4xlarge # 2.TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type     = "c5a.2xlarge"  
#   key_name          = module.hbl_gng_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   user_data         = base64encode(var.teleport_userdata_asg["userdata"])
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "gng-app"},
#           { "teleport" = "hbl-gng-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "gng-app"},
#           { "teleport" = "hbl-gng-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "gng-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-gng-app"}
#     )
# }

# #ASG FOR THE GNG
# module "hbl_gng_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-gng-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 14
#   max_size                  = 0 # TO BE CHANGED TO 14
#   desired_capacity          = 0 # TO BE CHANGED TO 14
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_gng_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_gng_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_alb.target_group_arns[4]]

#   tags = merge(local.tags, {
#     	application = "gng-app"
#   },
#   { "teleport" = "hbl-gng-app"})
# }