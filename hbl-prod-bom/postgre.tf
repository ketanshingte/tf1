#LOCALS FOR POSTGRE
locals {
  ami_id_postgre          = "ami-08b4be6f593c11d75"
  data_subnet_ids_postgre = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
  data_subnet_az_postgre  = ["${local.region}a", "${local.region}b", "${local.region}c"]

  ## HBL MB POSTGRE CLUSTER PRIVATE IPS
  hbl_mb_psql_db_ec2_01_private_ip = "10.26.13.129"
  hbl_mb_psql_db_ec2_02_private_ip = "10.26.14.130"
  hbl_mb_psql_db_ec2_03_private_ip = "10.26.15.254"
}


################################################
######  POSTGRE  CLUSTER ############
################################################

#KEYPAIR POSTGRE CLUSTER
module "hbl_mb_psql_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-mb-psql-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC9mT1Z1VeMEkUQ51zh6nUz4TeR9YS6YJ02YDFqU12jBwRPcJV3nCImkhK3mv4UpEq5A0TqV/if4zL8ydD9+EC1Yuvh0hslbyYvT1lEA7s1LAzYnQKkKCp9lrhcwaU8KHmhOHcw2pLX0DInuyWrD6Us/LrcVOvMMKbFMik2ye3EN0S2n0x8XPleL4eo5qS5ohHKRGZHKblvbN4VhyxGylrdDabSRbRpIgUW05kkFRau6pBADidkRTrVATZONIaCcGAz73koC/HSZWid8HRyOx3TEbQtkkUE1q8jGY0CAN1vCP0rrpmlLUd+Q3po6VabZHHe4qXyxIi31e4byhOMryLBIduHCleZaYPtcHdp8PicRMl1ZrLzwW1qUxnfw7BKJOZDMnXGehWAWiwY3xxH9fCXeNCTw6j/BzXdYHj2uLYpRuhjPJ/XkTceFcODuzivu5sJxPBWf/Q8Ghl9i0gm7/wrHCAb35f7OCuhE//Vu5/ySb6h2lBjqnCDDUl6xZmtTOM="
  tags = merge(
    local.tags,
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"}
  )
}

# # EC2 POSTGRE CLUSTER TO BE DELETED
# module "hbl_mb_psql_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-mb-psql-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_postgre
#   instance_type           = "c5a.4xlarge" # TO BE CHANGED TO c6a.4xlarge
#   subnet_id               = element(local.data_subnet_ids_postgre, count.index)
#   vpc_security_group_ids  = [module.hbl_mb_psql_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   user_data         = base64encode(var.teleport_userdata["userdata"])
#   key_name                = module.hbl_mb_psql_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 2500
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 25
#       encrypted   = true
#     },
#       {
#       device_name = "/dev/sdm"
#       volume_type = "gp3"
#       volume_size = 16
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdi"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdk"
#       volume_type = "gp3"
#       volume_size = 10
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdl"
#       volume_type = "gp3"
#       volume_size = 1000
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"}
#   )
# }

# SECURITY GROUP POSTGRE CLUSTER
module "hbl_mb_psql_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-mb-psql-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 2380
      to_port     = 2380
      protocol    = "tcp"
      description = "Opening port 2380 for postgre cluster"
      source_security_group_id = "${module.hbl_mb_psql_db_sg_01.security_group_id}"
    },
    {
      from_port   = 8679
      to_port     = 8679
      protocol    = "tcp"
      description = "Opening port 8679 for postgre cluster sg"
      source_security_group_id = "${module.hbl_mb_psql_db_sg_01.security_group_id}"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      description = "Opening port 1885 for postgre cluster sg"
      source_security_group_id = "${module.hbl_mb_psql_db_sg_01.security_group_id}"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      description = "Opening port 2379 for postgre cluster sg"
      source_security_group_id = "${module.hbl_mb_psql_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 1885
    #   to_port     = 1885
    #   protocol    = "tcp"
    #   description = "Opening port 1885 for mbu heap security group"
    #   source_security_group_id = "${module.hbl_mbu_heap_app_sg_01.security_group_id}"
    # },
    # {
    #   from_port   = 1885
    #   to_port     = 1885
    #   protocol    = "tcp"
    #   description = "Opening port 1885 for mbu docldr security group"
    #   source_security_group_id = "${module.hbl_mbu_docldr_app_sg_01.security_group_id}"
    # },
    # {
    #   from_port   = 1885
    #   to_port     = 1885
    #   protocol    = "tcp"
    #   description = "Opening port 1885 for mbu docwkr security group"
    #   source_security_group_id = "${module.hbl_mbu_docwkr_app_sg_01.security_group_id}"
    # }
    ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.105/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.106/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.107/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.108/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.109/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.96.110/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.97.167/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.97.168/32"
    }, 
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.105/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.106/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.107/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.108/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.109/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.96.110/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.97.167/32"
    },
    {
      from_port   = 5430
      to_port     = 5430
      protocol    = "tcp"
      self        = true
      description = "Opening 5430 port for postgre"
      cidr_blocks = "172.30.97.168/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.105/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.106/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.107/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.108/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.109/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.96.110/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.97.167/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.97.168/32"
    }, 
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.75.77/32"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      self        = true
      description = "Opening 2379 port for postgre"
      cidr_blocks = "172.30.75.78/32"
    },
    {
      from_port   = 2380
      to_port     = 2380
      protocol    = "tcp"
      self        = true
      description = "Opening 2380 port for postgre"
      cidr_blocks = "172.30.75.77/32"
    },
    {
      from_port   = 2380
      to_port     = 2380
      protocol    = "tcp"
      self        = true
      description = "Opening 2380 port for postgre"
      cidr_blocks = "172.30.75.78/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.75.77/32"
    },
    {
      from_port   = 1885
      to_port     = 1885
      protocol    = "tcp"
      self        = true
      description = "Opening 1885 port for postgre"
      cidr_blocks = "172.30.75.78/32"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      self        = true
      description = "Opening 22 port for postgre"
      cidr_blocks = "172.30.75.77/32"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      self        = true
      description = "Opening 22 port for postgre"
      cidr_blocks = "172.30.75.78/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"}
  )
}

# EC2 POSTGRE CLUSTER INSTANCE 01 
module "hbl_mb_psql_db_ec2_1" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-mb-psql-db-ec2-01"
  ami                     = local.ami_id_postgre
  instance_type           = "c5a.4xlarge" # TO BE CHANGED TO c6a.4xlarge
  subnet_id               = element(local.data_subnet_ids_postgre, 0)
  vpc_security_group_ids  = [module.hbl_mb_psql_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_mb_psql_db_kp_01.key_pair_key_name 
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  private_ip              = local.hbl_mb_psql_db_ec2_01_private_ip
  user_data_base64        = base64encode(var.teleport_userdata["userdata"])
  tags = merge(
    local.tags,
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"}
  )
}

# EBS VOLUME POSTGRE CLUSTER INSTANCE 01
module "hbl_mb_psql_db_ec2_1_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 2500
    device_name = "/dev/sdf"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-01"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}
module "hbl_mb_psql_db_ec2_1_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 25
    device_name = "/dev/sdg"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-02"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}

module "hbl_mb_psql_db_ec2_1_ebs_03" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 16
    device_name = "/dev/sdh"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-03"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}

module "hbl_mb_psql_db_ec2_1_ebs_04" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 20
    device_name = "/dev/sdi"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-04"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}

module "hbl_mb_psql_db_ec2_1_ebs_05" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 10
    device_name = "/dev/sdj"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-05"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}

module "hbl_mb_psql_db_ec2_1_ebs_06" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mb_psql_db_ec2_1]

    availability_zone = element(local.data_subnet_az_postgre, 0)
    size              = 1000
    device_name = "/dev/sdk"
    instance_id = module.hbl_mb_psql_db_ec2_1.ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-01-ebs-06"},
    { "application" = "postgre" },
    { "tier" = "db" },
    { "service" = "mb"},
    { "teleport" = "hbl-mb-postgre"},
    local.tags)  
}

# # EC2 POSTGRE CLUSTER INSTANCE 02 
# module "hbl_mb_psql_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-mb-psql-db-ec2-02"
#   ami                     = local.ami_id_postgre
#   instance_type           = "c5a.4xlarge" # TO BE CHANGED TO c6a.4xlarge
#   subnet_id               = element(local.data_subnet_ids_postgre, 1)
#   vpc_security_group_ids  = [module.hbl_mb_psql_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mb_psql_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_mb_psql_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"}
#   )
# }

# # EBS VOLUME POSTGRE CLUSTER INSTANCE 02
# module "hbl_mb_psql_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 2500
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-01"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }
# module "hbl_mb_psql_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 25
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-02"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_02_ebs_03" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 16
#     device_name = "/dev/sdh"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-03"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_02_ebs_04" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 20
#     device_name = "/dev/sdi"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-04"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_02_ebs_05" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 10
#     device_name = "/dev/sdj"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-05"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_02_ebs_06" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_postgre, 1)
#     size              = 1000
#     device_name = "/dev/sdk"
#     instance_id = module.hbl_mb_psql_db_ec2_02.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-06"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }


# # EC2 POSTGRE CLUSTER INSTANCE 03 
# module "hbl_mb_psql_db_ec2_03" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-mb-psql-db-ec2-02"
#   ami                     = local.ami_id_postgre
#   instance_type           = "c5a.4xlarge" # TO BE CHANGED TO c6a.4xlarge
#   subnet_id               = element(local.data_subnet_ids_postgre, 2)
#   vpc_security_group_ids  = [module.hbl_mb_psql_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mb_psql_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_mb_psql_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"}
#   )
# }

# # EBS VOLUME POSTGRE CLUSTER INSTANCE 03
# module "hbl_mb_psql_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 2500
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-01"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }
# module "hbl_mb_psql_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 25
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-02"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_03_ebs_03" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 16
#     device_name = "/dev/sdh"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-03"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_03_ebs_04" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 20
#     device_name = "/dev/sdi"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-04"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_03_ebs_05" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 10
#     device_name = "/dev/sdj"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-05"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }

# module "hbl_mb_psql_db_ec2_03_ebs_06" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mb_psql_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_postgre, 2)
#     size              = 1000
#     device_name = "/dev/sdk"
#     instance_id = module.hbl_mb_psql_db_ec2_03.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-mb-psql-db-ec2-02-ebs-06"},
#     { "application" = "postgre" },
#     { "tier" = "db" },
#     { "service" = "mb"},
#     { "teleport" = "hbl-mb-postgre"},
#     local.tags)  
# }