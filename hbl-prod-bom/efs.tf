# CREATING EFS POSTGRE FOR THREE AZS IN HBL ACCOUNT
module "hbl_efs_postgre_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//efs"
  name   = "${local.aws_account_name}-efs-postgre-01"
  enabled = true
  encrypted= true
  in_transit_encryption_policy = true
  efs_backup_policy_enabled= false
  create_security_group = false
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  subnets= aws_subnet.data[*].id
  region = local.region
  allowed_security_group_ids= [module.hbl_efs_postgre_sg_01.security_group_id]
  kms_key_id= "${data.terraform_remote_state.audit_tf_state.outputs.hbl_efs_kms_arn}"

  tags = merge(
    local.tags,
    { "service" = "efs" },
    { "application" = "postgre" },
    { "efs-backup" = "true" }
  )
}

#CREATING SECURITY GROUP TO ASSOCIATE WITH THE EFS POSTGRE 
module "hbl_efs_postgre_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-efs-postgre-sg-01"
  use_name_prefix = false
  description = "Security Group for EFS created for Postgre"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 2049
      to_port     = 2049
      protocol    = "tcp"
      description = "Opening 2049 port for mb psql db sg"
      source_security_group_id = "${module.hbl_mb_psql_db_sg_01.security_group_id}"
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "service" = "efs" },
    { "application" = "postgre" }
  )
}


