# # SECURITY GROUP FOR MBU NGINX IRP ASG
# module "hbl_mbu_nginx_irp_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-nginx-irp-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-nginx-irp-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR MBU NGINX IRP ASG
# module "hbl_mbu_nginx_irp_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-nginx-irp-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChNkwQXpsbsdL4BHXj+msGEsyS1XP5gB6qWu5liEtO11NE/SbbZhyN9wrQkYXXygTSPLPjgpRSmFRMdWWxDDt55w/A0WvV/JoDxbXAg1QeLwB2s1AlvDG8IBYb6oRazrizIOoVq8ED1rS+N7OL/QcS/tdMCTqxyhNXE7FCMvoyMaA5fSGFMUB7zLZP7Sw5LWtobSTsg5QrwbLH3TWDM/09vQfyzb5/aY8GrZ4U8ALopPl/+eiC0D+vSlrSa67+w4RP/iqSdWblaGi4UGPJoua5LAil+Ynte9mFMx9PkeAFOK9SOL1pX1VPQOStJhRX22PZ082hGBY2PuQdvafFChMXCc7+9P3KS7SvKKaZXoOSnYeERu8QrxOvb38ubxD7aa+tv61t9E56tZVA12tjUsO0X3KKcveBd+B98Dmn75SXh3fXvUekj52AkZ/XF28c7c45lpEmb8mmvLaxXZizNn67ZeB4BK98IwLzilWB78pSZw9jdrI97JO3DodjA+v5FA8="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-nginx-irp-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR MBU NGINX IRP ASG
# module "hbl_mbu_nginx_irp_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-mbu-nginx-irp-app-lt"
#   security_groups    = [module.hbl_mbu_nginx_irp_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-mbu-nginx-irp-app-lt-01"
#   image_id          = "ami-01c23c40d42acc4cb"
#   instance_type     = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   key_name          = module.hbl_mbu_nginx_irp_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 50
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "mbu-nginx-irp-app"},
#           { "teleport" = "hbl-mbu-nginx-irp-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "mbu-nginx-irp-app"},
#           { "teleport" = "hbl-mbu-nginx-irp-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "mbu-nginx-irp-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-mbu-nginx-irp-app"}
#     )
# }

# #ASG FOR THE MBU NGINX IRP
# module "hbl_mbu_nginx_irp_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-mbu-nginx-irp-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHABGED TO 3
#   max_size                  = 0 # TO BE CHABGED TO 3
#   desired_capacity          = 0 # TO BE CHABGED TO 3
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_mbu_nginx_irp_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_mbu_nginx_irp_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   tags = merge(local.tags, {
#     	application = "mbu-nginx-irp-app"
#   },
#   { "teleport" = "hbl-mbu-nginx-irp-app"})
# }