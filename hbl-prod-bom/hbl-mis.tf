#LOCALS FOR HBL MIS APP
locals {
  ami_id_mis_app          = "ami-0f03795d74a547daf"
  app_subnet_ids_mis_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
}


################################################
#############  HBL MIS APP CLUSTER ############
################################################

#IAM ROLE HBL MIS CLUSTER
resource "aws_iam_instance_profile" "s3_access_instance_profile" {
  name = "${local.aws_account_name}-ec2-s3-access-role-01"
  role = aws_iam_role.hbl_mis_job_role.name
  tags = merge(local.tags,
  {"application"="session-manager"}
  )
}

resource "aws_iam_role" "hbl_mis_job_role" {
  name = "${local.aws_account_name}-mis-job-role-01"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"]
  inline_policy {
    name = "hbl-mis-s3"
    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "${module.hbl_mis_s3_01.s3_bucket_arn}",
                "${module.hbl_mis_s3_01.s3_bucket_arn}/*",
            ]
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt",
                "kms:GenerateDataKey"
            ],
            "Resource": [
                "${data.terraform_remote_state.audit_tf_state.outputs.ldl_hbl_s3_kms_arn}"
            ]
        }
    ]
 })
 }
tags = local.tags
}

resource "aws_iam_role_policy" "mis_job_assume_role_policy_for_cross_account" {
  name = "${local.aws_account_name}-mis-job-assume-role-cross-account-policy-01"
  role = aws_iam_role.hbl_mis_job_role.id
 policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "*"
        }
    ]
}
EOF
}

#KEYPAIR HBL MIS CLUSTER
module "hbl_mis_app_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-mis-app-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCvO33m7eqEzaEJXdPu5xlzKEvttV90Iz6v2RImEsaQ4py8Qw3BSzslD30kdO9Sc/tKbx+E65QgHKPtIJ5extBSw8/dCj7NATR8ga5yd26eyrq1rTdfbDPcctCttwRfhGSxH1O5/pkRubweuuJThbZJLBVuGor1cITBWvqeQcEUABJPEfALQSv91EePnUxCTMxGuE9K/8lVH/7GkctedHLOOryFqOxa5XSPgW9SpfCooC6SWZsi0j6dwINpgRWPLemz4IHv/jna2e2JlYEy77Ye8jlbFmkp5WaTKH7KTdjEO2ryJs9V70E8HFh23oxtZ2wrc1F+VVrFKoeL/92C/1OI+ynOYD8WZdEl6d0MNJpEPsr9dKjR5OXbqfv2YfSd846+oxuwgEFf2xZd1e9RLQjh+bWZOOhyt09UGY6zHwmunB0pBr3fe4Ds7N4oWEWxnORkqOPUt3kfMwa1dhbvg4qoFgiHle+pWIDFYfbSE1MyveBdSfHcodxC3ZF/m8ReCM="
  tags = merge(
    local.tags,
    { "application" = "mis-app" },
    { "tier" = "app" }
  )
}

# EC2 HBL MIS APP CLUSTER
module "hbl_mis_app_ec2_01" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-mis-app-ec2-01"
  ami                     = local.ami_id_mis_app
  instance_type           = "c5a.large"
  subnet_id               = element(local.app_subnet_ids_mis_app, 0)
  vpc_security_group_ids  = [module.hbl_mis_app_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.s3_access_instance_profile.name
  key_name                = module.hbl_mis_app_kp_01.key_pair_key_name
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
  tags = merge(
    local.tags,
    { "application" = "mis-app" },
    { "tier" = "app" },
    { "teleport" = "${local.aws_account_name}-mis-app"}
  )
}

# SECURITY GROUP HBL MIS APP CLUSTER
module "hbl_mis_app_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-mis-app-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "mis-app" },
    { "tier" = "app" }
  )
}