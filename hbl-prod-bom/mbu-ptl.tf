# #LOCALS FOR MBU PTL APP
# locals {
#   ami_id_mbu_ptl_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_ptl_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU PTL APP CLUSTER #############
# ################################################

# #KEYPAIR MBU PTL CLUSTER
# module "hbl_mbu_ptl_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-ptl-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCsXa97FTeji9n6Iq+ib2MM6ZpytEAiRYAeWnbXN9pqj4mi9VElXM4YxQGk8Wa+9At/K3Tybvic/2IvOi7yLm9Psbaw+wBBLX3lwk2h2xuUjtEc3J4C4/sjuSeNsRLeVscIYG1Pxr/m2p7gj75Z/Nbw9hT7bXaky7t5TkNFitDbs2vdihcIwH97Erv9u+QSOAzmhMVpihxhe+j8vg8W0ZFg1sx4NGVHkVJxFILQqnD5GD5Q6Um8lSOqhkfriG95mvpPAtNKzoyYTkCJsT8Hvi462C/n4GVDlZKLP8oqjVuWVwXy1xoJECpaLFc1IO2ZnneZwDge81VEPpWIZEFZ+nVcXSTnZFBCdArcigv1pasWNJcKt9Ky10l+rkPf4jty5BQqWNLGjcViGIqGWo/pKY4yMrNB/vMibvlZ+s4klstO0TZcFlEezv/YN4ePU6wo6JprIEX1ntsiHh020TlO9H2AL7VkFtujpZAGQJaadf/lhjpruT4ceOFzvfdnd5nrRUM="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-ptl-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU PTL APP CLUSTER
# module "hbl_mbu_ptl_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-mbu-ptl-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_ptl_app
#   instance_type           = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_ptl_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_ptl_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_ptl_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 80
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-ptl-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-ptl-app"}
#   )
# }

# # SECURITY GROUP MBU PTL APP CLUSTER
# module "hbl_mbu_ptl_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-ptl-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-ptl-app" },
#     { "tier" = "app" }
#   )
# }