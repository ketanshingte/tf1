# # SECURITY GROUP FOR UAM ASG
# module "hbl_uam_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-uam-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8080
#       to_port     = 8080
#       protocol    = "tcp"
#       description = "Opening 8080 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "uam-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR UAM ASG
# module "hbl_uam_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-uam-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCiJoz93I8HCBjDyxpciSpPi/DOZUpMNuvBDvV/aCNhAa6gwJ1GEDxNyWJeOt3gj7kcH1qrMFrpQpvsXPULHNDPvAyoX401qXzc+VJayklCQh/7cMU2S9XdYi8wSkhFSoxyWEnKvYsgX8fM8Jp7MPU4MQSkQNAvFMuKqC96o2LiWHVbPijcRFO8sR6DCDdHGfBFwZwDYakSANryulBF+Ads3JW+8M4q5WGWpBCNbpPhxFGbtFMSqYx8bVzSuUJXC0PZl54SHzbXY/jcC4wyDVu8gmQlCQ2uJfinWVlkNIfHoGETyLDXOS8P6gxM9LF1oFPY++Wy+QyZXOgp16OT72TcVpKbVBVBnu8YSXSVfJdviWMhEPPlrlbm+0rS1hMzYg86o91TUw780zSzh6i4fFUUojoyoKJW3XspFT87QuMhja5H5lF1P8XTlI96EDWzwG26T+0q2d6PafLrPtxtyy6KlFn1Yn5/OHwmdK/ycDFdQyhQ3HLJitBRf8u73TBo/1c="
#   tags = merge(
#     local.tags,
#     { "application" = "uam-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR UAM ASG
# module "hbl_uam_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-uam-app-lt"
#   security_groups    = [module.hbl_uam_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-uam-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   instance_type     = "m6a.xlarge"
#   key_name          = module.hbl_uam_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "uam-app"},
#           { "teleport" = "hbl-uam-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "uam-app"},
#           { "teleport" = "hbl-uam-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "uam-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-uam-app"}
#     )
# }

# #ASG FOR THE UAM
# module "hbl_uam_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-uam-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 2
#   max_size                  = 0 # TO BE CHANGED TO 2
#   desired_capacity          = 0 # TO BE CHANGED TO 2
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_uam_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_uam_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_alb.target_group_arns[5]]

#   tags = merge(local.tags, {
#     	application = "uam-app"
#   },
#   { "teleport" = "hbl-uam-app"})
# }