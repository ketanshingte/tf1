# #LOCALS FOR MBU EXP APP
# locals {
#   ami_id_mbu_exp_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_exp_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU EXP APP CLUSTER #############
# ################################################

# #KEYPAIR MBU EXP CLUSTER
# module "hbl_mbu_exp_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-exp-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChGXw9TnvmS+/E+B70VdXmupMg1iMpTZnNgcYp4RbYIKLRr5THq8WJ6Yx4IXz+nDl5KSUfThyes7oP9K5ufCVwqshIuOVZAGgIy3HbO/rHrUlfDw+XVcUUG0vWPqkQNai6z7vDPYXADGu6fIlYd7mit/o0yJu43F8xoSa2YJLsgSqvaah038u6EiqT84c3I51BWTq6tJ7syKPyHujR1L8kJZWS2IUKllO8VbfppFcw+BkJciy1yywLb0MHzbLHK3HXBa1q9T9nn10/bI1GRWTUDIPMWvhEmROOYg0vpyRDT5xzpFADfygxR6gHyIHwwz6C55af47iSX4zgixjrCVXD836bOdVE3T88YSx4YycNq9Ibnhx7DyQAG2ZAxw7v56niP61cb5r/oHlB1SbkEFSH6O1fumkDvIZcnXDOU7iLNdS/HRJ//VAAUcttyAfYDStjdQ3XPHqP31+rCKTmnjRym1/w/oq9rEsLw69m4Zr73Gl6sKgJLvDYGa3vixCVTA0="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-exp-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU EXP APP CLUSTER
# module "hbl_mbu_exp_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "2"
#   name                    = "${local.aws_account_name}-mbu-exp-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_exp_app
#   instance_type           = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_exp_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_exp_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_exp_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-exp-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-exp-app"}
#   )
# }

# # SECURITY GROUP MBU EXP APP CLUSTER
# module "hbl_mbu_exp_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-exp-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-exp-app" },
#     { "tier" = "app" }
#   )
# }