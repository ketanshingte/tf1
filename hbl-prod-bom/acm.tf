module "serviceurl_acm" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//acm"

  certificate_transparency_logging_preference = true
  create_certificate = true
  create_route53_records = false
  domain_name               = "*.serviceurl.in"
  validate_certificate = false
  validation_method         = "DNS"
  #validation_record_fqdns = var.validation_record_fqdns
  tags = local.tags
}