# # SECURITY GROUP FOR GNG MBU ASG
# module "hbl_gng_mbu_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-mbu-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-mbu-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR GNG MBU ASG
# module "hbl_gng_mbu_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-mbu-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC9M5PFAnY8KC1DVapGAVQk5xKEg6suL0+QMvrFIRHw5vx7g+Mc5ixBTRR+DaZUUz1WHVL3F/jiVMeeY9hC4OQBJObHvJGLHtDtgppa1hX94ZcRJRY2Da3DFqDU2NnlUGa3ipxgL3avi224xAbCug0klZLeZXN58a5TcM3FUseUd2niDmgy6cvYGvcTwJEmRFJuzR6RW7gqWKDHd4P5rWmayzPfIywnRZk6poE5Z+igA6TjMUso6HUI0wD5jHOYLubDYCTRwJQnn0UloxzWEkJL6111+HFgNLUa850B1P1MTAau1McrQxMMBCMveIWj3Ls8J5tLVRly5DvjWFCpeA6A/ng2102iqLLuoMWvn++EwGM/18vZvwpKO0RVDdXrS/wmuKKXtt74kOEvbwW55KxuSMlt2zkMJSwH17bh2/WCYBVW/Pe5dM7h8PylGS/XW2CbahA9XhTmd9X6wLOuxWJGn118fiDfpAwPwGXIV+Q9cPog3hhU+or9FKpWWYCluTs="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-mbu-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR GNG MBU ASG
# module "hbl_gng_mbu_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-gng-mbu-app-lt"
#   security_groups    = [module.hbl_gng_mbu_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-gng-mbu-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   instance_type     = "c6a.2xlarge"
#   key_name          = module.hbl_gng_mbu_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "gng-mbu-app"},
#           { "teleport" = "hbl-gng-mbu-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "gng-mbu-app"},
#           { "teleport" = "hbl-gng-mbu-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "gng-mbu-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-gng-mbu-app"}
#     )
# }

# #ASG FOR THE GNG MBU
# module "hbl_gng_mbu_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-gng-mbu-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 6
#   max_size                  = 0 # TO BE CHANGED TO 6
#   desired_capacity          = 0 # TO BE CHANGED TO 6
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_gng_mbu_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_gng_mbu_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   tags = merge(local.tags, {
#     	application = "gng-mbu-app"
#   },
#   { "teleport" = "hbl-gng-mbu-app"})
# }