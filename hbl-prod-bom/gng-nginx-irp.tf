# # SECURITY GROUP FOR GNG NGINX IRP ASG
# module "hbl_gng_nginx_irp_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-nginx-irp-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 80
#       to_port     = 80
#       protocol    = "tcp"
#       description = "allowing port 80 network account traffic for http health check"
#       cidr_blocks = "10.66.4.0/22"
#     },
#     {
#       from_port   = 80
#       to_port     = 80
#       protocol    = "tcp"
#       description = "allowing port 80 for summarized ip range of loadbalancer subnets"
#       cidr_blocks = "10.26.4.0/22"
#     }
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-nginx-irp-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR GNG NGINX IRP ASG
# module "hbl_gng_nginx_irp_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-nginx-irp-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC15y9omySbQXSHNB3EfcanDYNgxHKK20IX9spoeZgHS5Xrf7B0unNpj67xgjG7JZm/3t8JAnDWgqLM7bSVUX35HzutLXQESXesR6qWJRh9W3YtpaWyLlLdaPj3zy7sevKzbWiKbbh/6f7WKjm+vZcMa1OBm9ZjjZgiUG4h8pFo0u4pLZ2F5xu0wRm2RGMxxCrH3TOBDyOacQh6ASnvB0Dhh36Jhdr+wH4qQyXUspeV3CsZBhrbnpa65NPSisrfVX+2lE5r0G+/cJCRsFCt6yjvT+khYOgOKSreU0ZIyYlihGSJ2pkM2f15Oyz8FwfC4eFUmEAochu+UQ3zAe4ujtPvtEgEb1ZMoWE/0ltxHvzSd7Z7GnV24GaQfU+RoSYKRR7zV4Xv3Zxjl+eYO/0FCX+1hzanN9Tij7all5dFmKXesfB3XcVBZW7t/S54CC+8nG8JSYBRlSjwMzOTo5hO5F0ttoqm5ILXVyxAdvcAmsXJck0RJuupPFnfMZ1Hlt55rA8="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-nginx-irp-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR GNG NGINX IRP ASG
# module "hbl_gng_nginx_irp_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-gng-nginx-irp-app-lt"
#   security_groups    = [module.hbl_gng_nginx_irp_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-gng-nginx-irp-app-lt-01"
#   image_id          = "ami-01c23c40d42acc4cb"
#   instance_type     = "c6a.large"
#   key_name          = module.hbl_gng_nginx_irp_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "gng-nginx-irp-app"},
#           { "teleport" = "hbl-gng-nginx-irp-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "gng-nginx-irp-app"},
#           { "teleport" = "hbl-gng-nginx-irp-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "gng-nginx-irp-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-gng-nginx-irp-app"}
#     )
# }

# #ASG FOR THE GNG NGINX IRP
# module "hbl_gng_nginx_irp_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-gng-nginx-irp-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 3
#   max_size                  = 0 # TO BE CHANGED TO 3
#   desired_capacity          = 0 # TO BE CHANGED TO 3
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_gng_nginx_irp_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_gng_nginx_irp_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_nlb.target_group_arns[0]]

#   tags = merge(local.tags, {
#     	application = "gng-nginx-irp-app"
#   },
#   { "teleport" = "hbl-gng-nginx-irp-app"})
# }