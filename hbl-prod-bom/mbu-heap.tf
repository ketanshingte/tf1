# #LOCALS FOR MBU HEAP APP
# locals {
#   ami_id_mbu_heap_app          = "ami-092825e13eafaacc6"
#   app_subnet_ids_mbu_heap_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU HEAP APP  ###################
# ################################################

# #KEYPAIR MBU HEAP 
# module "hbl_mbu_heap_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-heap-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDalAWPzTwFsg2MskxXB5pAlhOS0urLOynL6yoc3WtHdZcNufePFHuZXcrsUokB0BR+rVa0nHFFVFziyQF+Pe4AAdHKfuBxO7xFGOQFKPwGJ6DEezviBCNPx0V14ISrUblQ4SUZiPb1cMDZ+zz3tyJzAsZVxth4SYxEt+sLIXzu+y5gr7EY4uc2B884njzNQPuecmzEhP1B1LkhOgnQich7mSREqQmAKajDeiWygihF7eK5hLlCNFtF3OdhVu36PPj7ZUL7nR1PpRmNuqEKsWrhr5nQ9+L7WzzS9tD6azUFzGz1ewHyDeCfksEdGsyevxZ9s787/hS9QWTNYunKo//3HbOSFqRAlQwwcgrXdBBPRYVyXFqAUScl6PzflMrmIsm2xL32Py0J2hWegNWbFPBFMZhRC6tNEe6l7xoGInauhtU+9Gm7nq22ShxnEv/KzqFi/HC0UcJUoiZVqgEyb/VtB+XmHo51XwVIBuc4mBXhTUDzQGP+MdkU119pNTxUKeM="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-heap-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU HEAP APP 
# module "hbl_mbu_heap_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "1"
#   name                    = "${local.aws_account_name}-mbu-heap-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_heap_app
#   instance_type           = "c6a.large"
#   subnet_id               = element(local.app_subnet_ids_mbu_heap_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_heap_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_heap_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-heap-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-heap-app"}
#   )
# }

# # SECURITY GROUP MBU HEAP APP 
# module "hbl_mbu_heap_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-heap-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 6432
#       to_port     = 6432
#       protocol    = "tcp"
#       description = "allow 6432 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 5010
#       to_port     = 5010
#       protocol    = "tcp"
#       description = "allow 5010 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 5011
#       to_port     = 5011
#       protocol    = "tcp"
#       description = "allow 5011 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 5012
#       to_port     = 5012
#       protocol    = "tcp"
#       description = "allow 5012 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-heap-app" },
#     { "tier" = "app" }
#   )
# }