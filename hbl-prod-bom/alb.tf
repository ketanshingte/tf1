## CREATING APPLICATION LOADBALANCER USED FOR ALL HBL APPLICATIONS
module "hbl_shared_prod_app_alb" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//loadbalancer"

  name = "${local.aws_account_name}-shared-prod-app-alb"
  load_balancer_type = "application"
  internal      = true

  vpc_id             = module.hbl_networking_vpc_01.vpc_id
  subnets = module.hbl_networking_vpc_01.private_subnets
  security_groups    = [module.hbl_prod_app_alb_sg_01.security_group_id]

  access_logs = {
    bucket = "${data.terraform_remote_state.logs_tf_state.outputs.logs_loadbalancer_access_logs_s3_bucket_id}"
    prefix = "${local.aws_account_name}/alb/shared-hbl-app"
  }

  # ATTRIBUTES
  drop_invalid_header_fields       = false
  preserve_host_header             = false
  enable_deletion_protection       = true


## TARGET GROUPS CREATION
  target_groups = [
    ## GNG PTL APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-ptl-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-ptl-app"},
              {"tier" = "target-group"}
            )

    },
    ## SSL BU APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-ssl-bu-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "ssl-bu-app"},
              {"tier" = "target-group"}
            )
    },
    ## SSL OEM APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-ssl-oem-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "ssl-oem-app"},
              {"tier" = "target-group"}
            )
    },
    ## SSL APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-ssl-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "ssl-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-app"},
              {"tier" = "target-group"}
            )
    },
    ## UAM APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-uam-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "uam-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG CD 3PI APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-cd-3pi-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-cd-3pi-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG BRE APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-bre-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-bre-app"},
              {"tier" = "target-group"}
            )
    },
    ## UAM AUTH APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-uam-auth-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "uam-auth-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG OD APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-od-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-od-app"},
              {"tier" = "target-group"}
            )
    },
    ## BREX APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-brex-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "brex-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG LMS APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-lms-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-lms-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG TW BRE APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-tw-bre-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-tw-bre-app"},
              {"tier" = "target-group"}
            )
    },
    ## GNG TW APPLICATION TARGET GROUP
    {
      name         = "${local.aws_account_name}-gng-tw-app-tg"
      backend_protocol     = "HTTPS"
      backend_port         = 443
      target_type          = "instance"
      protocol_version = "HTTP1"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 5
        timeout             = 5
        protocol            = "HTTPS"
        matcher             = "200"
      }

      tags = merge(
              local.tags,
              {"application" = "gng-tw-app"},
              {"tier" = "target-group"}
            )
    },
  ]

##ALB LISTENER AND LISTENER RULES CONFIGURATION

## HTTP LISTENER
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
      action_type        = "fixed-response"
      fixed_response = {
        content_type = "text/html"
        message_body = "You are not authorized to access resource."
        status_code  = "200"
      },
    }
  ]

## HTTPS LISTENER
  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      target_group_index = 0
      action_type        = "fixed-response"
      fixed_response = {
        content_type = "text/html"
        message_body = "You are not authorized to access this resource."
        status_code  = "200"
      },
      certificate_arn    = module.serviceurl_acm.acm_certificate_arn
    },
  ]
  
  https_listener_rules = [
  #PATH FOR THE GNG PTL APPLICATION - TO BE UPDATED
    {
      https_listener_index = 0
      priority             = 1

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/hbl/gonogo/*", "/gonogo/od/web/*", "/gonogo/od/eSign-verify/web/*", "/lms/web/*"]
       }]
    },
    #PATH FOR THE GNG PTL APPLICATION - TO BE UPDATED
    {
      https_listener_index = 0
      priority             = 2

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/admin/web/*", "/gotrust/od/web/*", "/bre/cd/web/*", "/bre/tw/web/*"]
       }]
    },
    #PATH FOR THE GNG PTL APPLICATION - TO BE UPDATED
    {
      https_listener_index = 0
      priority             = 3

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gonogo/cd/schemes/web/*", "/gonogo/tw/cpv/web/*", "/gonogo/tw/admin/web/*", "/gonogo/cd/admin/web/*"]
       }]
    },
    #PATH FOR THE GNG PTL APPLICATION
    {
      https_listener_index = 0
      priority             = 4

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gonogo/cd/web/*", "/gonogo/tw/web/*", "/gotrust/web/*", "/hbl/tw/gonogo/*"]
       }]
    },
    #PATH FOR THE SSL BU APPLICATION
    {
      https_listener_index = 0
      priority             = 5

      actions = [
        {
          type               = "forward"
          target_group_index = 1
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/bank-utility/api/*","/gogetr/bank-stmt-merge-utility/api/*"]
       }]
    },
    #PATH FOR THE SSL OEM APPLICATION
    {
      https_listener_index = 0
      priority             = 6

      actions = [
        {
          type               = "forward"
          target_group_index = 2
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/oem/api/*"]
       }]
    },
    #PATH FOR THE SSL APP APPLICATION
    {
      https_listener_index = 0
      priority             = 7

      actions = [
        {
          type               = "forward"
          target_group_index = 3
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/admin/api/*","/gogetr/smart-interfaces/api/image-service/*"]
       }]
    },
    #PATH FOR THE SSL APP APPLICATION
    {
      https_listener_index = 0
      priority             = 8

      actions = [
        {
          type               = "forward"
          target_group_index = 3
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/hubx/api/client-connector/*","/gogetr/102/api/*","/gogetr/106/api/*","/gogetr/107/api/*"]
       }]
    },
    #PATH FOR THE SSL APP APPLICATION
    {
      https_listener_index = 0
      priority             = 9

      actions = [
        {
          type               = "forward"
          target_group_index = 3
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gogetr/hubx/api/*","/gogetr/hubx/api/data-transformation/*","/gogetr/hubx/api/workflow/*","/gogetr/hubx/api/connector/*"]
       }]
    },
    #PATH FOR THE GNG APPLICATION
    {
      https_listener_index = 0
      priority             = 10

      actions = [
        {
          type               = "forward"
          target_group_index = 4
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gonogo/cd/api/*"]
       }]
    },
    #PATH FOR THE UAM APPLICATION
    {
      https_listener_index = 0
      priority             = 11

      actions = [
        {
          type               = "forward"
          target_group_index = 5
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gotrust/api/*"]
       }]
    },
    #PATH FOR THE GNG CD 3PI APPLICATION
    {
      https_listener_index = 0
      priority             = 12

      actions = [
        {
          type               = "forward"
          target_group_index = 6
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gonogo/smsservice/api/*","/gonogo/connector/api/*"]
       }]
    },
    #PATH FOR THE GNG BRE APPLICATION
    {
      https_listener_index = 0
      priority             = 13

      actions = [
        {
          type               = "forward"
          target_group_index = 7
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/bre/cd/*"]
       }]
    },
    #PATH FOR THE UAM AUTH APPLICATION
    {
      https_listener_index = 0
      priority             = 14

      actions = [
        {
          type               = "forward"
          target_group_index = 8
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gotrust/oauth2/*"]
       }]
    },
    #PATH FOR THE GNG OD APPLICATION
    {
      https_listener_index = 0
      priority             = 15

      actions = [
        {
          type               = "forward"
          target_group_index = 9
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/od/api/mis-report/*","/gonogo/od/api/*"]
       }]
    },
    #PATH FOR THE BREX APPLICATION
    {
      https_listener_index = 0
      priority             = 16

      actions = [
        {
          type               = "forward"
          target_group_index = 10
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/brex/credit-model/api/*"]
       }]
    },
    #PATH FOR THE GNG LMS APPLICATION
    {
      https_listener_index = 0
      priority             = 17

      actions = [
        {
          type               = "forward"
          target_group_index = 11
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/lms/api/*"]
       }]
    },
    #PATH FOR THE GNG TW BRE APPLICATION
    {
      https_listener_index = 0
      priority             = 18

      actions = [
        {
          type               = "forward"
          target_group_index = 12
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/bre/tw/*"]
       }]
    },
    #PATH FOR THE GNG TW APPLICATION
    {
      https_listener_index = 0
      priority             = 19

      actions = [
        {
          type               = "forward"
          target_group_index = 13
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/gonogo/tw/api/*"]
       }]
    },
    #PATH FOR THE GNG PTL APPLICATION
    {
      https_listener_index = 0
      priority             = 20

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/hbl/tw/admin-panel/*", "/hbl/tw/cpv-tvr/*", "/hbl/od/gonogo/*", "/hbl/od/gonogo/eSign-verify/*"]
       }]
    },
    #PATH FOR THE GNG PTL APPLICATION
    {
      https_listener_index = 0
      priority             = 21

      actions = [
        {
          type               = "forward"
          target_group_index = 0
        }
      ]

      conditions = [{
        host_headers = ["02684972991.serviceurl.in"]
       },
       {
        path_patterns = ["/hbl/admin-panel/*", "/hbl/scheme/*"]
       }]
    },
  ]
}

##C REATING THE SECURITY GROUP USED WITH LOADBALANCER
module "hbl_prod_app_alb_sg_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name        = "${local.aws_account_name}-prod-app-alb-sg-01"
  description = "Security Group for Application ALB of the prod applications"
  vpc_id      = module.hbl_networking_vpc_01.vpc_id
  use_name_prefix = false
 ingress_with_source_security_group_id = [
    # {
    #   from_port   = 443
    #   to_port     = 443
    #   protocol    = "tcp"
    #   description = "Opening the HTTPS port from Nginx cluster layer"
    #   source_security_group_id = "${module.hbl_gng_nginx_irp_app_sg.security_group_id}"
    # },
 ]

  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "Allow all outgoing"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  tags = merge(
    local.tags,
    {"application" = "alb"},
    {"tier" = "loadbalancer"}
  )
}

# # CREATING CLOUDWATCH ALARMS FOR ALB
# module "hbl_alb_ClientTLSNegotiationErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-ClientTLSNegotiationErrorCount-alarm-01" 
#   alarm_description   = "ClientTLSNegotiationErrorCount alb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "ClientTLSNegotiationErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_alb_HTTPCode_ELB_5XX_Count_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-HTTPCode_ELB_5XX_Count-alarm-01" 
#   alarm_description   = "HTTPCode_ELB_5XX_Count alb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 60

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "HTTPCode_ELB_5XX_Count"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_alb_RejectedConnectionCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-RejectedConnectionCount-alarm-01" 
#   alarm_description   = "RejectedConnectionCount alb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "RejectedConnectionCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_alb_TargetConnectionErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-TargetConnectionErrorCount-alarm-01" 
#   alarm_description   = "TargetConnectionErrorCount alb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "TargetConnectionErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_alb_UnHealthyHostCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-UnHealthyHostCount-alarm-01" 
#   alarm_description   = "UnHealthyHostCount alb alarm"
#   comparison_operator = "GreaterThanOrEqualToThreshold"
#   evaluation_periods  = 1
#   threshold           = 1
#   period              = 60

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "UnHealthyHostCount"
#   statistic   = "Maximum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_alb_TargetTLSNegotiationErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-alb-TargetTLSNegotiationErrorCount-alarm-01" 
#   alarm_description   = "TargetTLSNegotiationErrorCount alb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_alb.lb_arn_suffix
#   }

#   namespace   = "AWS/ApplicationELB"
#   metric_name = "TargetTLSNegotiationErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }