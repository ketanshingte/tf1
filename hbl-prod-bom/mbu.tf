#LOCALS FOR MBU APP
locals {
  ami_id_mbu_app          = "ami-0143cc1040af5cb1c"
  app_subnet_ids_mbu_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
}


################################################
#############  MBU APP CLUSTER ################
################################################

#KEYPAIR MBU CLUSTER
module "hbl_mbu_app_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-mbu-app-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCkVVt9fG5QkNEWD4mrM2Y9FeTqpSLSavkP1F2BYhv1AmoWdmZAxT/c2kfK+IWzgoM5STCoRGbB4OWPwUgdpnZWXvyICS0O94LQKaJmlaAjDDklouMIOnMnwEyXwvN623yMGsyxJRBQJmZqxaCrthS2YdL2xANowtgM7c83wDvgqXx5YDVuXn4PXu+4P4RQWzhFruVe+HH8S0h/Wq15hPRtbL9zR3Pc5sXzdJnINMH+JsLu1lAdsFZc2loQPSslw1iZ0LvmUsRNT5PjlDddvrrIYouMKdK/2bStmIn1VVAIfV2cdg2az3TL1lbNMPvTvDZn+En5vsAIK7oXNq7TBE9jSbETvoS8t94eMZJgW9K5KKIm6SNHgpNMEhxE16AZglXlSW1dPEqhycLGS2Jn3u5HuGMa9bjnppVyWZrXf9hz/rFgHRZs3jDxjdaN16/bAwsFy/CeEwnwwuusqxKLLgXzQNHgOxP3Zmvxh5NZH6o6QCQ47Bi/IrvzJqJGirqzMHc="
  tags = merge(
    local.tags,
    { "application" = "${local.aws_account_name}-mbu-app" },
    { "tier" = "app" }
  )
}

# EC2 MBU APP CLUSTER
module "hbl_mbu_app_ec2_01" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  count                   = "1"
  name                    = "${local.aws_account_name}-mbu-app-ec2-${format("%02d", count.index + 1)}"
  ami                     = local.ami_id_mbu_app
  #instance_type           = "c5a.4xlarge" # 1. TO BE CHANGED TO c6a.4xlarge # 2. TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
  instance_type           = "c5a.xlarge"
  subnet_id               = element(local.app_subnet_ids_mbu_app, count.index)
  vpc_security_group_ids  = [module.hbl_mbu_app_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_mbu_app_kp_01.key_pair_key_name
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
  volume_tags = merge(
    local.tags,
    { "application" = "${local.aws_account_name}-mbu-app" },
    { "tier" = "app" },
    { "teleport" = "${local.aws_account_name}-mbu-app" }
  )
  tags = merge(
    local.tags,
    { "application" = "${local.aws_account_name}-mbu-app" },
    { "tier" = "app" },
    { "teleport" = "${local.aws_account_name}-mbu-app"}
  )
}

# SECURITY GROUP MBU APP CLUSTER
module "hbl_mbu_app_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-mbu-app-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    # {
    #   from_port   = 443
    #   to_port     = 443
    #   protocol    = "tcp"
    #   description = "Opening 443 port for alb sg"
    #   source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
    # }
  ]
  ingress_with_cidr_blocks = [
    # {
    #   from_port   = 22
    #   to_port     = 22
    #   protocol    = "tcp"
    #   description = "allow ssh port to sharedservices app subnet range for jenkins"
    #   cidr_blocks = "10.81.8.0/21"
    # },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    },
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "mbu-app" },
    { "tier" = "app" }
  )
}