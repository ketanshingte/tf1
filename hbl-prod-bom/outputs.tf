# NETWORK LOADBALANCER DNS OUTPUT
# output "nlb_dns_name" {
#   description = "The DNS name of the Prod Network load balancer."
#   value       = module.shared_prod_app_nlb.lb_dns_name
# }

# TRANSIT GATEWAY ATTACHMENT ID OUTPUT
output "hbl_vpc_attachment_id" {
  description = "The Attachment ID of the hbl prod vpc in the network account."
  value       = module.hbl_networking_vpc_01_tgw_attachment.tgw_vpc_attachment_id
}

#TERRAFORM VPC OUTPUTS
output "hbl_networking_vpc_id" {
  description = "The ID of the VPC"
  value       = module.hbl_networking_vpc_01.vpc_id
}

output "hbl_networking_vpc_arn" {
  description = "The ARN of the VPC"
  value       = module.hbl_networking_vpc_01.vpc_arn
}

output "hbl_networking_vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = module.hbl_networking_vpc_01.vpc_cidr_block
}

##OUTPUT FOR THE APP SUBNETS
output "app_subnets" {
  description = "List of IDs of App subnets"
  value       = aws_subnet.app.*.id
}

##OUTPUT NLB IPS FROM NETWORK INTERFACE DATASOURCE
output "hbl_shared_prod_app_nlb_network_interface_ips" {
  value = "${sort(flatten([data.aws_network_interface.ifs.*.private_ips]))}"
}

output "hbl_aws_account_id" {
  description = "Accout ID"
  value       = local.aws_account_id
}

##OUTPUT BACKUP VAULT
output "hbl_efs_postgre_vault_id" {
  description = "The name of the vault"
  value       = module.hbl_efs_postgre_vault_01.id
}