#######################################################################################################
##  HBL EFS BACKUP VAULT
#######################################################################################################

# BACKUP VAULT FOR HBL EFS POSTGRE MOUNTPOINT
module "hbl_efs_postgre_vault_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-bom.git//backup//sub-module/vault"

  name        = "${local.aws_account_name}-efs-postgre-vault-01"
  kms_key_arn = data.terraform_remote_state.audit_tf_state.outputs.hbl_others_kms_arn
  tags        = local.tags
}