locals {
  sns_subscriptions_backup_failure_alerts_email_list = ["harshjm@lentra.ai","manju@lentra.ai"]
}

## CREATING ACCESS POLICY FOR AWS BACKUP IN SNS TOPIC
data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
     actions = ["SNS:Publish"]

     principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
        resources = [
              "*"
    ]
  }
}

## CREATING SNS TOPIC AND SUBSCRIPTION FOR ALERTS
resource "aws_sns_topic" "hbl_backup_failure_sns_topic_01" {
  
  #SNS TOPIC
  name         = "${local.aws_account_name}-backup-failure-alerts-sns-topic-01"
  display_name = "${local.aws_account_name}-backup-failure-alerts" 

  kms_master_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_others_kms_arn}"
  tags = local.tags
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

## CREATING SNS NOTIFICATIONS FROM BACKUP VAULT
resource "aws_backup_vault_notifications" "hbl_efs_postgre_backup_vault" {
  backup_vault_name   = "${local.aws_account_name}-efs-postgre-vault-01"
  sns_topic_arn       = aws_sns_topic.hbl_backup_failure_sns_topic_01.arn
  backup_vault_events = ["BACKUP_JOB_COMPLETED"]
}


## CREATING SUBSCRIPTION FOR SNS TOPIC
resource "aws_sns_topic_subscription" "hbl_backup_failure_alerts_sns_subscription" {
  topic_arn = aws_sns_topic.hbl_backup_failure_sns_topic_01.arn
  for_each  = toset(local.sns_subscriptions_backup_failure_alerts_email_list)
  endpoint  = each.value
  protocol  = "email-json"
  endpoint_auto_confirms = true
  filter_policy_scope = "MessageAttributes"
  filter_policy=jsonencode({
  "State": [
    {
      "anything-but": "COMPLETED"
    }
  ]
})
}