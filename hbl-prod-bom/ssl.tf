# # SECURITY GROUP FOR SSL ASG
# module "hbl_ssl_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-ssl-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8011
#       to_port     = 8011
#       protocol    = "tcp"
#       description = "Opening 8011 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8006
#       to_port     = 8007
#       protocol    = "tcp"
#       description = "Opening 8006 & 8007 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 8002
#       to_port     = 8002
#       protocol    = "tcp"
#       description = "Opening 8002 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },{
#       from_port   = 8000
#       to_port     = 8000
#       protocol    = "tcp"
#       description = "Opening 8000 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 7206
#       to_port     = 7206
#       protocol    = "tcp"
#       description = "Opening 7206 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 9207
#       to_port     = 9207
#       protocol    = "tcp"
#       description = "Opening 9207 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 9201
#       to_port     = 9204
#       protocol    = "tcp"
#       description = "Opening 9201 to 9204 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     },
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "ssl-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR SSL ASG
# module "hbl_ssl_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-ssl-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCfT4APizOBQDaXvs39B2oiwhilsdZ55em3+1BGFqAtK4rL7FxpRzqT1k6mvB/Qfe/hF+znUPQlwJTEfHsxbKFSit6KgGF5tuWRj5xLgeCLqu7QxFmDE193coVXE5IjZY+L4LAFgXWb72MroRNmFtH/qM8MFEyEGmkhJGp90voQbvUeooTUv4JpkWVxeHAxI9zROxSclzVACRjVxSk+TWJR9O0NiMVHUzbflys9I5FqYm9ec3YHHnhMDK7Tyy4rGO4i5hxTYCc+Cy5dM//3+V+uV/64eK6haCWTIf59paM39Wo7raqXDuHDiT3X3sCY6THqOtT6L5gqHyg8R3tzpFdIveOB9e4IMK68oKf8GaRfseCNNlna+57bVXAOWf/c9UT3eQ542kh/UpJCZHdJGRsE/HedNgGsTs61TZQtX6EJE5NfSKt6j1CbSKG0if9S9WXMx13z2U3ZT/wLbAxsubHYoB2XBKOCXtAcHAQMAEtTo74Hnfn51Z7jtdQDc718N68="
#   tags = merge(
#     local.tags,
#     { "application" = "ssl-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR SSL ASG
# module "hbl_ssl_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-ssl-app-lt"
#   security_groups    = [module.hbl_ssl_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-ssl-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   #instance_type     = "c5a.4xlarge" ## 1.TO BE CHANGED TO c6a.4xlarge # 2.TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type     = "c5a.2xlarge"
#   key_name          = module.hbl_ssl_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 60
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "ssl-app"},
#           { "teleport" = "hbl-ssl-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "ssl-app"},
#           { "teleport" = "hbl-ssl-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "ssl-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-ssl-app"}
#     )
# }

# #ASG FOR THE SSL
# module "hbl_ssl_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-ssl-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 2
#   max_size                  = 0 # TO BE CHANGED TO 2
#   desired_capacity          = 0 # TO BE CHANGED TO 2
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_ssl_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_ssl_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_alb.target_group_arns[3]]

#   tags = merge(local.tags, {
#     	application = "ssl-app"
#   },
#   { "teleport" = "hbl-ssl-app"})
# }