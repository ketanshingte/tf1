#LOCALS FOR MONGODB
locals {
  ami_id_mongodb          = "ami-076e3a557efe1aa9c"
  ami_id_mongodb_v4       = "ami-0330b8861a7e6efe0"
  data_subnet_ids_mongodb = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
  data_subnet_az_mongodb  = ["${local.region}a", "${local.region}b", "${local.region}c"]

  ## HBL GNG ARCHIVE MONGODB PRIVATE IPS
  hbl_mongo_archive_ec2_01_private_ip = "10.26.13.220"
  #hbl_mongo_archive_ec2_02_private_ip = "10.26.14.220"

  ## HBL GNG OD MONGODB PRIVATE IPS
  hbl_gng_od_mongo_db_ec2_01_private_ip = "10.26.13.183"
  #hbl_gng_od_mongo_db_ec2_02_private_ip = "10.26.14.230"
  # hbl_gng_od_mongo_db_ec2_03_private_ip = "10.26.15.21"

  ## HBL GNG TXN MONGODB PRIVATE IPS
  hbl_gng_txn_mongo_db_ec2_01_private_ip = "10.26.13.157"
  #hbl_gng_txn_mongo_db_ec2_02_private_ip = "10.26.14.114"
  # hbl_gng_txn_mongo_db_ec2_03_private_ip = "10.26.15.233"


  ## HBL GNG PDT MONGODB PRIVATE IPS
  #hbl_gng_pdt_mongo_db_ec2_01_private_ip = "10.26.13.221"
  # hbl_gng_pdt_mongo_db_ec2_02_private_ip = "10.26.14.111"
  hbl_gng_pdt_mongo_db_ec2_03_private_ip = "10.26.15.156"

  ## HBL GNG IMAGE MONGODB PRIVATE IPS
  hbl_gng_image_mongo_db_ec2_01_private_ip = "10.26.13.58"
  #hbl_gng_image_mongo_db_ec2_02_private_ip = "10.26.14.254"
  # hbl_gng_image_mongo_db_ec2_03_private_ip = "10.26.15.127"

  ## HBL GNG UAM MONGODB PRIVATE IPS
  hbl_gng_uam_mongo_db_ec2_01_private_ip = "10.26.13.112"
  #hbl_gng_uam_mongo_db_ec2_02_private_ip = "10.26.14.57"
  # hbl_gng_uam_mongo_db_ec2_03_private_ip = "10.26.15.151"

  ## HBL GNG TW MONGODB PRIVATE IPS
  hbl_gng_tw_mongo_db_ec2_01_private_ip = "10.26.13.16"
  #hbl_gng_tw_mongo_db_ec2_02_private_ip = "10.26.14.135"
  # hbl_gng_tw_mongo_db_ec2_03_private_ip = "10.26.15.148"
}


################################################
######  MONGODB TRANSACTION CLUSTER ############
################################################

#KEYPAIR MONGODB TRANSACTION CLUSTER
module "hbl_gng_txn_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-mongo_txn-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCjTyww+0Ng8rI+14BMrFgG7D5RyiCjz1sJ6JizVxCXoWxVuknlbDlFJnSE81CC7cuSeLH32EpK92MIurm4AiDU+JyP177doKsg5oN3EJsncS/Gc2PF3fOYI1exFaX05fL0b3o6DMBPt1b5QRcshwXVQrpn+FDrpL3oFXzZsHhShSKiNa5EyKZp0KRyz0EI5c3jVBEE8hyp0xKXmLZWiP25M3Ebd4o2zywyMFdS2BeCrgRL0s3PIIQywGRBNdlGA9/3JRv4av+OtWTT3gSYWktbSBAIdMSSY70SKkEjfROREKYIZQqMpVE7MnTsy9aHQEPo5RG9L87PVUJ1uhFT0Td/Nn4dSWb/hj48gfffsZD3SyzMWMfmPq+y/T85RZQDeCwp1nQm2IPBNkAOdPu1JPW85N0c4tGK4umIUufQ1R2CbW7uxensmYe0rI5c6Y+lNmJNGlsgQANWiWyvIUqqbVO0Gr316hmnAnTgoK4yOBOCgFUQmXAg4XZ3pJXdCG4bEfU="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

#KEYPAIR MONGODB TRANSACTION CLUSTER
module "hbl_gng_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-mongo-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCjTyww+0Ng8rI+14BMrFgG7D5RyiCjz1sJ6JizVxCXoWxVuknlbDlFJnSE81CC7cuSeLH32EpK92MIurm4AiDU+JyP177doKsg5oN3EJsncS/Gc2PF3fOYI1exFaX05fL0b3o6DMBPt1b5QRcshwXVQrpn+FDrpL3oFXzZsHhShSKiNa5EyKZp0KRyz0EI5c3jVBEE8hyp0xKXmLZWiP25M3Ebd4o2zywyMFdS2BeCrgRL0s3PIIQywGRBNdlGA9/3JRv4av+OtWTT3gSYWktbSBAIdMSSY70SKkEjfROREKYIZQqMpVE7MnTsy9aHQEPo5RG9L87PVUJ1uhFT0Td/Nn4dSWb/hj48gfffsZD3SyzMWMfmPq+y/T85RZQDeCwp1nQm2IPBNkAOdPu1JPW85N0c4tGK4umIUufQ1R2CbW7uxensmYe0rI5c6Y+lNmJNGlsgQANWiWyvIUqqbVO0Gr316hmnAnTgoK4yOBOCgFUQmXAg4XZ3pJXdCG4bEfU="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB TRANSACTION CLUSTER
module "hbl_gng_mongo_txn_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-txn-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_txn_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for jump-server sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-app-sg"
    #   source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.71/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.72/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.75/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.161/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.162/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-txn" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# EC2 MONGODB TXN CLUSTER INSTANCE 01 
module "hbl_gng_txn_mongo_db_ec2_1" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-gng_txn-mongo-db-ec2-01"
  ami                     = local.ami_id_mongodb
  instance_type           = "r6a.4xlarge"
  subnet_id               = element(local.data_subnet_ids_mongodb, 0)
  vpc_security_group_ids  = [module.hbl_gng_mongo_txn_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_gng_txn_mongo_db_kp_01.key_pair_key_name 
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  private_ip              = local.hbl_gng_txn_mongo_db_ec2_01_private_ip
  user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"},
    # { "teleport" = "hbl-gng_txn-mongodb"}
  )
}

# EBS VOLUME MONGODB TXN CLUSTER INSTANCE 01
module "hbl_gng_txn_mongo_db_ec2_1_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_txn_mongo_db_ec2_1]

    availability_zone = element(local.data_subnet_az_mongodb, 0)
    size              = 20
    device_name = "/dev/sdf"
    instance_id = module.hbl_gng_txn_mongo_db_ec2_1.ec2_complete_id
}
module "hbl_gng_txn_mongo_db_ec2_1_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_txn_mongo_db_ec2_1]

    availability_zone = element(local.data_subnet_az_mongodb, 0)
    size              = 8100
    device_name = "/dev/sdg"
    instance_id = module.hbl_gng_txn_mongo_db_ec2_1.ec2_complete_id
}

# # EC2 MONGODB TXN CLUSTER INSTANCE 02 
# module "hbl_gng_txn_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_txn-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb
#   instance_type           = "r5a.4xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_txn_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_txn_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_txn_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_txn-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB TXN CLUSTER INSTANCE 02
# module "hbl_gng_txn_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_txn_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 8295
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_txn_mongo_db_ec2_02.ec2_complete_id
# }

# # EBS VOLUME MONGODB TXN CLUSTER INSTANCE 02
# module "hbl_gng_txn_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_txn_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_txn_mongo_db_ec2_02.ec2_complete_id
# }

# EC2 MONGODB TXN CLUSTER INSTANCE 03 
# module "hbl_gng_txn_mongo_db_ec2_03" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_txn-mongo-db-ec2-03"
#   ami                     = local.ami_id_mongodb
#   instance_type           = "r6a.8xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 2)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_txn_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_txn_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_txn_mongo_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_txn-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB TXN CLUSTER INSTANCE 03
# module "hbl_gng_txn_mongo_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_txn_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_txn_mongo_db_ec2_03.ec2_complete_id
# }
# module "hbl_gng_txn_mongo_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_txn_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 8100
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_txn_mongo_db_ec2_03.ec2_complete_id
# }

################################################
############ MONGODB IMAGE CLUSTER #############
################################################


###WE NEED TO CHANGE THE KEYPAIR. CURRENTLY IT IS USING THE SAME KEYPAIR AS THE TRANSACTION CLUSTER. 

#KEYPAIR MONGODB IMAGE CLUSTER
module "hbl_gng_mongo_image_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-mongo-image-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDWySEsDD0d38r+ExeoMePO/I8fwiIjmSIwuFn6TuOeNd1iOlxe3KTVJHay7Nb+7v9u3JtwzqTrwTPVpc9BhtOzW4tGgtXoDWgNznupxr+koUbJFwBKSk4s4ZAO5KUQtERf/SNbMjS6g1yYqqoVW02o8H+qxZJKvOgykPdhm8I+aTIWYi63i5FAIAuU6Xl4Xk3+RRihL6ajs+fsVR3OqUGL5U6oQPAAtw3R06l4C3Y50dUaC3bCbeXhShkU50gpJywkmWOW6Y/PQE3lQ/VBrRV0PzLRtclkOCvaIZxzvhNOWenII6E0O3rU7iRuZKspjmnzzb9rRtrmHhwT7yRm6B1+puQBx9jhFIiXbll5+nL7AKGUJPkVKEUzd8FM7vDwcc4KBbUclUybXCqrfeIJhAQNC0usRqDtysWPfl7DbOQnv2sbuVd7fGUM7E618kpZWCT9BQXsEY66xi997lMfQjgOdjeNY+haGGDkj4iZVqkI9TEIKJmfSmb++FUU57BIoPE="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB IMAGE CLUSTER
module "hbl_gng_mongo_image_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-image-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_image_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for jump-server sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-app-sg"
    #   source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-od-app-sg"
    #   source_security_group_id = "${module.hbl_gng_od_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-tw-app-sg"
    #   source_security_group_id = "${module.hbl_gng_tw_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.92/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.94/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.163/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.75.68/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.84/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.84/32"
    },
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-image" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# EC2 MONGODB IMAGE CLUSTER INSTANCE 01 
module "hbl_gng_image_mongo_db_ec2_1" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-gng_image-mongo-db-ec2-01"
  ami                     = local.ami_id_mongodb
  # instance_type           = "r6a.4xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
  instance_type           = "r6a.2xlarge"
  subnet_id               = element(local.data_subnet_ids_mongodb, 0)
  vpc_security_group_ids  = [module.hbl_gng_mongo_image_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_gng_mongo_db_kp_01.key_pair_key_name #SHOULD BE CHANGED TO THE IMAGE KEYPAIR WHENEVER WE RECREATE THIS INSTANCE.
  #key_name                = module.hbl_gng_mongo_image_db_kp_01.key_pair_key_name #THIS KEYPAIR PATH TO BE USED DURING THE INSTANCE RECREATION.
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  private_ip              = local.hbl_gng_image_mongo_db_ec2_01_private_ip
  user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"},
    # { "teleport" = "hbl-gng_image-mongodb"}
  )
}

# EBS VOLUME MONGODB IMAGE CLUSTER INSTANCE 01
module "hbl_gng_image_mongo_db_ec2_1_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_image_mongo_db_ec2_1]

    availability_zone = element(local.data_subnet_az_mongodb, 0)
    size              = 20
    device_name = "/dev/sdf"
    instance_id = module.hbl_gng_image_mongo_db_ec2_1.ec2_complete_id
}
module "hbl_gng_image_mongo_db_ec2_1_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_image_mongo_db_ec2_1]

    availability_zone = element(local.data_subnet_az_mongodb, 0)
    size              = 11000
    device_name = "/dev/sdg"
    instance_id = module.hbl_gng_image_mongo_db_ec2_1.ec2_complete_id
}

# # EC2 MONGODB IMAGE CLUSTER INSTANCE 02 
# module "hbl_gng_image_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_image-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb
#   # instance_type           = "r6a.4xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type           = "r6a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_image_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_mongo_image_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_image_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_image-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB IMAGE CLUSTER INSTANCE 02
# module "hbl_gng_image_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_image_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_image_mongo_db_ec2_02.ec2_complete_id
# }
# module "hbl_gng_image_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_image_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 12288
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_image_mongo_db_ec2_02.ec2_complete_id
# }

# module "hbl_gng_image_mongo_db_ec2_02_ebs_03" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_image_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 1000
#     device_name = "/dev/sdh"
#     instance_id = module.hbl_gng_image_mongo_db_ec2_02.ec2_complete_id
# }


# # EC2 MONGODB IMAGE CLUSTER INSTANCE 03 
# module "hbl_gng_image_mongo_db_ec2_03" {
#  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_image-mongo-db-ec2-03"
#   ami                     = local.ami_id_mongodb
#   # instance_type           = "r6a.4xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type           = "r5a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 2)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_image_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_mongo_image_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_image_mongo_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_image-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_image_mongo_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_image_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 13312
#     device_name = "/dev/sdf"
#     type        = "sc1"
#     instance_id = module.hbl_gng_image_mongo_db_ec2_03.ec2_complete_id
# }

# # EBS VOLUME MONGODB IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_image_mongo_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_image_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 20
#     device_name = "/dev/sdg"
#     type        = "gp3"
#     instance_id = module.hbl_gng_image_mongo_db_ec2_03.ec2_complete_id
# }

# THIS CODE IS REDUNDANT SINCE WE HAVE LAUNCHED THE MONGO IMAGE CLUSTER INDIVIDUALLY INSTEAD OF USING COUNT. NEEDS TO BE REMOVED
# # EC2 MONGODB IMAGE CLUSTER
# module "hbl_gng_image_mongo_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-gng_image-mongo-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb
#   instance_type           = "r6a.4xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_image_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 11000
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_image-mongodb"}
#   )
# }
################################################
##########  MONGODB PRODUCT CLUSTER ############
################################################

#KEYPAIR PRODUCT MONGODB CLUSTER
module "hbl_gng_pdt_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-pdt-mongo-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/cCAwcGJK/6i+OPNMFcDQdJ93B/xM4t66rSeWYVSGyPZ+5cdQnEpY5u1OR+fnEn6PfkvxbAjSd6+bXNJYx90csuTpOHaPR6bY2Ktle6IjzbltDkIen8HR70DTnevvSaZE0ki43cH80k5SbfpWDE1bqVxVqdQjfFsXHUCsxEXaFBYfFgNY0rsUnJmQv58ubbl5ANejcHA8Xn8BJy/hbyrQk74naVwnAZnhZQyI4WF3egoELmpAicKpBKcCIW9pFzFTtJxnDMLboJEBprOUIqDEdj0W64eSyLdXYMRSF88ldiwlxP0GyaHBltbdgYKrKt3z6b9/FJPqkRuhJgzIhfSuX0FRoBGiT0dCRbauMv0XGwjS+1966aslnJpezHk1d0KAQ1qp6mT3CCCfwFpgL3Manna/s58ShN3qucX6j7G4WN3O5Vog9DP96yYuOArb49lw3uunALpu9H5OS4DQXfTBWtV1w5C1vPPEMDzQ9oASttpKhaaqZkf9Z666wOoNID8="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB PRODUCT CLUSTER
module "hbl_gng_mongo_pdt_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-pdt-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_pdt_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "allowing port 10085 from hbl_jump_server_db_sg sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-tw-bre-app-sg"
    #   source_security_group_id = "${module.hbl_gng_tw_bre_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for ssl-oem-app-sg"
    #   source_security_group_id = "${module.hbl_ssl_oem_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for ssl-app-sg"
    #   source_security_group_id = "${module.hbl_ssl_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for ssl-bu-app-sg"
    #   source_security_group_id = "${module.hbl_ssl_bu_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-bre-ct-app-sg"
    #   source_security_group_id = "${module.hbl_gng_bre_ct_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-bre-app-sg"
    #   source_security_group_id = "${module.hbl_gng_bre_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.76/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.90/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.91/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.165/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.166/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.72/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.73/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.97.12/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.97.11/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.154/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.155/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.19/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.53.6/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.56.230/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for app server lentradc"
      cidr_blocks = "172.30.56.231/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-image" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# THIS CODE IS REDUNDANT SINCE WE HAVE LAUNCHED THE MONGO IMAGE CLUSTER INDIVIDUALLY INSTEAD OF USING COUNT. NEEDS TO BE REMOVED
# EC2 MONGODB PRODUCT CLUSTER INSTANCE
# module "hbl_gng_pdt_mongo_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-gng_pdt-mongo-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.4xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_pdt_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_pdt_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 3500
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_pdt-mongodb"}
#   )
# }

# # EC2 MONGODB PDT CLUSTER INSTANCE 01
# module "hbl_gng_pdt_mongo_db_ec2_1" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_pdt-mongo-db-ec2-01"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.4xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 0)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_pdt_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_pdt_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_pdt_mongo_db_ec2_01_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_pdt-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB PDT CLUSTER INSTANCE 01
# module "hbl_gng_pdt_mongo_db_ec2_1_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_pdt_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 40
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_pdt_mongo_db_ec2_1.ec2_complete_id 
# }
# module "hbl_gng_pdt_mongo_db_ec2_1_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_pdt_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 6424
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_pdt_mongo_db_ec2_1.ec2_complete_id
# }

# # EC2 MONGODB PDT CLUSTER INSTANCE 02 
# module "hbl_gng_pdt_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_pdt-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r5a.4xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_pdt_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_pdt_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_pdt_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_pdt-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB PDT CLUSTER INSTANCE 02
# module "hbl_gng_pdt_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_pdt_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 6247
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_pdt_mongo_db_ec2_02.ec2_complete_id
# }

# # EBS VOLUME MONGODB PDT CLUSTER INSTANCE 02
# module "hbl_gng_pdt_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_pdt_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_pdt_mongo_db_ec2_02.ec2_complete_id
# }

# EC2 MONGODB PDT CLUSTER INSTANCE 03 
module "hbl_gng_pdt_mongo_db_ec2_03" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-gng_pdt-mongo-db-ec2-03"
  ami                     = local.ami_id_mongodb_v4
  instance_type           = "r6a.2xlarge"
  subnet_id               = element(local.data_subnet_ids_mongodb, 2)
  vpc_security_group_ids  = [module.hbl_gng_mongo_pdt_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_gng_pdt_mongo_db_kp_01.key_pair_key_name 
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  private_ip              = local.hbl_gng_pdt_mongo_db_ec2_03_private_ip
  user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"},
    # { "teleport" = "hbl-gng_pdt-mongodb"}
  )
}

# EBS VOLUME MONGODB PDT CLUSTER INSTANCE 03
module "hbl_gng_pdt_mongo_db_ec2_03_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_pdt_mongo_db_ec2_03]

    availability_zone = element(local.data_subnet_az_mongodb, 2)
    size              = 20
    device_name = "/dev/sdf"
    instance_id = module.hbl_gng_pdt_mongo_db_ec2_03.ec2_complete_id
}
module "hbl_gng_pdt_mongo_db_ec2_03_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_gng_pdt_mongo_db_ec2_03]

    availability_zone = element(local.data_subnet_az_mongodb, 2)
    size              = 4500
    device_name = "/dev/sdg"
    instance_id = module.hbl_gng_pdt_mongo_db_ec2_03.ec2_complete_id
}

################################################
############  MONGODB UAM CLUSTER ##############
################################################

#KEYPAIR UAM MONGODB CLUSTER
module "hbl_gng_uam_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-uam-mongo-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQColnFsmqvmiIVAa8+MjIeXPyroz9+6h5ZuWAJpFZvuTGUxYC0pZ+nz9HWYPhmftfpLRcFMdPD0UFNuppwp0F7tX+bEOt/nUdubmUrrtVpnBK+oH0eeCd3a5ASb47y5BEeCKG4Ou92Axktx588k6M9zBhuMeyKHJnvL+49ifmu+MlyIo/NBfJw9uP6pmgmDTcBCwXnYz6UNuIAnYo5/tS9J1/sR7tD2B+CTx2yN1NP9Lil0sF01Es0ubvXRSvSabS/QN/kN2cEQ8cupBZ7hQsMzHpRq2cTaoUUDKwWJEVVs7nSGnk5lX+SJhZAMGu2quXkyz5oHeYBKW5AiZRAU8Li1Xi6SsBL2liyJS9OI/fmqTmVabp8nFQzrHELacbZyWp8LT9f55WOiGIo8+gdZqwekjYk0dwosQorUpTfpG+mnkJCoYVwUJyykeYpsjBCOLcNr/94+1nR/FpiRC1N4U4iZ1FxRLTAnXjWqpHfbzI6m4d2FpyQELvO9AtPOu4YaJB8="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB UAM CLUSTER
module "hbl_gng_mongo_uam_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-uam-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_uam_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "allowing port 10085 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for uam-app-sg"
    #   source_security_group_id = "${module.hbl_uam_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for uam-auth-app-sg"
    #   source_security_group_id = "${module.hbl_uam_auth_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.119/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.120/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.121/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.101/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.102/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-image" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# # EC2 MONGODB UAM CLUSTER INSTANCE 01 
# module "hbl_gng_uam_mongo_db_ec2_1" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_uam-mongo-db-ec2-01"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 0)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_uam_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_uam_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_uam_mongo_db_ec2_01_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"}
#   )
# }

# # EBS VOLUME UAM IMAGE CLUSTER INSTANCE 01
# module "hbl_gng_uam_mongo_db_ec2_1_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_1]
#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_1.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng_uam-mongo-db-ec2-01-ebs-01"},
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"},
#     local.tags)  
# }
# module "hbl_gng_uam_mongo_db_ec2_1_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_1]
#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 200
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_1.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng_uam-mongo-db-ec2-01-ebs-02"},
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"},
#     local.tags)  
# }

# # EC2 MONGODB UAM CLUSTER INSTANCE 02 
# module "hbl_gng_uam_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_uam-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_uam_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_uam_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_uam_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"}
#   )
# }

# # EBS VOLUME UAM IMAGE CLUSTER INSTANCE 02
# module "hbl_gng_uam_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_02.ec2_complete_id
# }
# module "hbl_gng_uam_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 200
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_02.ec2_complete_id
# }

# # EC2 MONGODB UAM CLUSTER INSTANCE 03 
# module "hbl_gng_uam_mongo_db_ec2_03" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_uam-mongo-db-ec2-03"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r5a.xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 2)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_uam_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_uam_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_uam_mongo_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"}
#   )
# }

# # EBS VOLUME UAM IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_uam_mongo_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_03]
#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 200
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_03.ec2_complete_id
# }

# # EBS VOLUME UAM IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_uam_mongo_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_uam_mongo_db_ec2_03]
#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_uam_mongo_db_ec2_03.ec2_complete_id
# }

# THIS CODE IS REDUNDANT SINCE WE HAVE LAUNCHED THE MONGO IMAGE CLUSTER INDIVIDUALLY INSTEAD OF USING COUNT. NEEDS TO BE REMOVED
# # EC2 MONGODB UAM CLUSTER INSTANCE
# module "hbl_gng_uam_mongo_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-gng_uam-mongo-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_uam_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_uam_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 200
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_uam-mongodb"}
#   )
# }

################################################
############  MONGODB TW CLUSTER ###############
################################################

#KEYPAIR TW MONGODB CLUSTER
module "hbl_gng_tw_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-tw-mongo-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/VTUIiRtdMBUA3/jU0crgOw45AR5pd96TFpx/dVUyh6GZqQ1sk00F7ec9gZ7tJ5y4Vj+UpkzAokx77/y7a3xWB0SY5jg8OfFYnEAMrDVH4qJ4kdxbERpgqmpXtnI55ebdOcXMWobXWBv8is6TCiY5XVzgTG8JszbzsnwaA01hobBrEdrZCu8yyuHi3deFCPYlkfvDnQeKVlS+wbRQLZPkxkYeZ/mI0VBbSoII5Gixy/0ZquLdPbZa/crQyHCIXCFHGxlppvk2l8GuudRvtgkE/Ua13JX7zxJHJp5P8G3xA7ZXcufQC1KSbf+WkSOe4FYq2BFdsySgdyGfh9vxjhyWFn1P2KQxp3mjQjLpDruyrCWyHA/DW1TmOGw2tJE+ckXUc5N5agtYSQN78X9YUapdYESIVIP+qIT+yKxYGsX8ef0392a5pixrHGCB9mVyHCrMW2y+QeY7QQGp8Zhu/8iBu3jbLsu/jMMOcNbW3WTwCfpFxweUjLOBl8/fUZBeUpE="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB TW CLUSTER
module "hbl_gng_mongo_tw_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-tw-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_tw_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for hbl jump server db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-tw-app-sg"
    #   source_security_group_id = "${module.hbl_gng_tw_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.71.47/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.71.48/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.71.49/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.174/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.175/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-image" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# # EC2 MONGODB TW CLUSTER INSTANCE 01 
# module "hbl_gng_tw_mongo_db_ec2_1" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_tw-mongo-db-ec2-01"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 0)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_tw_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_tw_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_tw_mongo_db_ec2_01_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"}
#   )
# }

# # EBS VOLUME TW IMAGE CLUSTER INSTANCE 01
# module "hbl_gng_tw_mongo_db_ec2_1_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_1.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng_tw-mongo-db-ec2-1-ebs-01"},
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"},
#     local.tags)  
# }
# module "hbl_gng_tw_mongo_db_ec2_1_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 100
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_1.ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng_tw-mongo-db-ec2-1-ebs-02"},
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"},
#     local.tags)  
# }

# # EC2 MONGODB TW CLUSTER INSTANCE 02 
# module "hbl_gng_tw_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_tw-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb_v4
#   # instance_type           = "r6a.xlarge"
#   instance_type           = "m6a.2xlarge" ## CHANGED FROM "r6a.xlarge" TO "m6a.2xlarge" BECAUSE OF DATABASE TEAM REQUESTED FOR EXACT VCPU AND MEMORY
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_tw_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_tw_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_tw_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"}
#   )
# }

# # EBS VOLUME TW IMAGE CLUSTER INSTANCE 02
# module "hbl_gng_tw_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_02.ec2_complete_id
# }
# module "hbl_gng_tw_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 500
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_02.ec2_complete_id 
# }

# # EC2 MONGODB TW CLUSTER INSTANCE 03 
# module "hbl_gng_tw_mongo_db_ec2_03" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_tw-mongo-db-ec2-03"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "m5a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 2)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_tw_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_tw_mongo_db_kp_01.key_pair_key_name 
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_tw_mongo_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"}
#   )
# }

# # EBS VOLUME TW IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_tw_mongo_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_03]
#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 500
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_03.ec2_complete_id
# }

# # EBS VOLUME TW IMAGE CLUSTER INSTANCE 03
# module "hbl_gng_tw_mongo_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_tw_mongo_db_ec2_03]
#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_tw_mongo_db_ec2_03.ec2_complete_id
# }

####THIS CODE IS REDUNDANT AFTER CREATING THE ABOVE 3 INSTANCES IN THE CLUSTER
# # EC2 MONGODB TW CLUSTER INSTANCE
# module "hbl_gng_tw_mongo_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "3"
#   name                    = "${local.aws_account_name}-gng_tw-mongo-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_tw_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_tw_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 100
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_tw-mongodb"}
#   )
# }



################################################
############  MONGODB OD CLUSTER ###############
################################################

#KEYPAIR OD MONGODB CLUSTER
module "hbl_gng_od_mongo_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-gng-od-mongo-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC1M2FYDRul+Ytq7PxLPNAQ5xfaWSpli7I/NSJX2h8k6Czu82hh45fMNl7j9kzSsBurRHiwyQbQfj7vG/fhacs22Ps2IExhU5C9pnA9rSTnHv/M8RkkrDFGHS0/F0lcvFzBr7Mvxj1jitFIeQYk5/v25/hn73cX7wcPx7u8O4h/Tt4MbOWi6ho0fTZ6pZZB00xBWMhJIfIZ5Ybk3AvQK/XEH0cTNFBbApJqbAr2HpMfD5KCMJFqepE+Appg8SMI0wNpWDOZbWrGOX37OOtHzdVAhuOf40atLWRbyAwwW5JJYXQTJfdAYnhZMFd5cLbaoBVJHyGvDgR6zL9xCesxck2RN8gNQ8jzG9xtWWM6p+GI5Co0APvgaM4NbsLk+9zTm5PJopeISa4Eyyw2AFV2dZ5+vw4f0KQhZchkdNtEy2H5gEcENPKTUnVtnuKJk0xtoHyELhgl3B33Xvb5PrBeN9h+6qbW55b9V/brWuWFf/R7gmUZSlsShrki8eP36iugSH0="
  tags = merge(
    local.tags,
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP MONGODB OD CLUSTER
module "hbl_gng_mongo_od_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-gng-mongo-od-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_gng_mongo_od_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "allowing port 10085 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_jump_server_db sg"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng-od-app-sg"
    #   source_security_group_id = "${module.hbl_gng_od_app_sg.security_group_id}"
    # },
  ]
  ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.116/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.117/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.96.118/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.171/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.172/32"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb lentradc"
      cidr_blocks = "172.30.97.173/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongodb-image" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# # EC2 MONGODB OD INSTANCE 01 
# module "hbl_gng_od_mongo_db_ec2_1" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_od-mongo-db-ec2-01"
#   ami                     = local.ami_id_mongodb_v4
#   # instance_type           = "r6a.xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type           = "r6a.large"
#   subnet_id               = element(local.data_subnet_ids_mongodb, 0)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_od_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_od_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_od_mongo_db_ec2_01_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_od-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB OD CLUSTER INSTANCE 01
# module "hbl_gng_od_mongo_db_ec2_1_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_1.ec2_complete_id 
# }
# module "hbl_gng_od_mongo_db_ec2_1_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_1]

#     availability_zone = element(local.data_subnet_az_mongodb, 0)
#     size              = 200
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_1.ec2_complete_id
# }

# # EC2 MONGODB OD INSTANCE 02 
# module "hbl_gng_od_mongo_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_od-mongo-db-ec2-02"
#   ami                     = local.ami_id_mongodb_v4
#   # instance_type           = "r6a.xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type           = "m6a.2xlarge" ## CHANGED FROM "r6a.large" TO "m6a.2xlarge" BECAUSE OF DATABASE TEAM REQUESTED FOR EXACT VCPU AND MEMORY
#   subnet_id               = element(local.data_subnet_ids_mongodb, 1)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_od_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_od_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_od_mongo_db_ec2_02_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_od-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB OD CLUSTER INSTANCE 02
# module "hbl_gng_od_mongo_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_02.ec2_complete_id
# }
# module "hbl_gng_od_mongo_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_02]

#     availability_zone = element(local.data_subnet_az_mongodb, 1)
#     size              = 600
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_02.ec2_complete_id
# }

# # EC2 MONGODB OD INSTANCE 03 
# module "hbl_gng_od_mongo_db_ec2_03" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-gng_od-mongo-db-ec2-03"
#   ami                     = local.ami_id_mongodb_v4
#   # instance_type           = "r6a.xlarge" # TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type           = "m5a.2xlarge" ## CHANGED FROM "r6a.large" TO "m6a.2xlarge" BECAUSE OF DATABASE TEAM REQUESTED FOR EXACT VCPU AND MEMORY
#   subnet_id               = element(local.data_subnet_ids_mongodb, 2)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_od_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_od_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   private_ip              = local.hbl_gng_od_mongo_db_ec2_03_private_ip
#   user_data_base64        = base64encode(var.teleport_userdata["userdata_13_0_3"])
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     # { "teleport" = "hbl-gng_od-mongodb"}
#   )
# }

# # EBS VOLUME MONGODB OD CLUSTER INSTANCE 03
# module "hbl_gng_od_mongo_db_ec2_03_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 600
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_03.ec2_complete_id
# }

# # EBS VOLUME MONGODB OD CLUSTER INSTANCE 03
# module "hbl_gng_od_mongo_db_ec2_03_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_od_mongo_db_ec2_03]

#     availability_zone = element(local.data_subnet_az_mongodb, 2)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_gng_od_mongo_db_ec2_03.ec2_complete_id
# }

####THIS CODE IS REDUNDANT AFTER CREATING THE ABOVE 3 INSTANCES IN THE CLUSTER
# # EC2 MONGODB OD CLUSTER INSTANCE
# module "hbl_gng_od_mongo_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "0"
#   name                    = "${local.aws_account_name}-gng_od-mongo-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_mongo_od_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_od_mongo_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "gp3"
#       volume_size = 100
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongodb" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_od-mongodb"}
#   )
# }



################################################
##########  MONGODB ARCHIVE SERVER ############
################################################

#KEYPAIR FOR MONGO ARCHIVE SERVER
module "hbl_mongo_archive_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-mongo-archive-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDbQrRut9l6ETGBZLvd4/s07/s+1n2aRPRQxEP+6JRkP52/GJ9fHi9Agyba1nwU2ADIb8e+KdMZ0UrfpeU1w16rJi/grnStBlvdh+VTA46rLMHmRV1vk+BvOSGysL+sYPoD7aU9kAMinOhpJoEl+ELghHfZpvVJZu3ctf4mpI1OX6qtsojtO67PXPFGUvp+PIGPATAQDvA0N1muiIsjjmKypc7f1Jgi93x+wVEKCiDW4nXcfV0ybQkH6o+IpG8sYuI908uG9h4BLdcriLqXjgPwUaOWxl2JNkJEiD1Ht0Ds1NQ1r3m67OBMarqhTFQfXHBOcuUSmvNYxnqI3tSDRLRWgu87SKcqzjshjgCtnv7k8aQGyAApzFUvqN3EGWkU/JdJwqlofqLAooQcq3wQdXVzxusfoPhPT94pC+uIve3Hw1LHaXJyiieBXCDxjU8hNlTWLCwzA1A8QIzcr3ZMnGNiz9Bii+l4Wa0VdeqGdIiWpWHn2sr7q+T8LCBXQWjjW9k="
  tags = merge(
    local.tags,
    { "application" = "mongo-archive" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# SECURITY GROUP FOR MONGO ARCHIVE SERVER
module "hbl_mongo_archive_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-mongo-archive-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening port 10085 for mongo-archive server"
      source_security_group_id = "${module.hbl_gng_mongo_txn_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening port 10085 for mongo-archive server"
      source_security_group_id = "${module.hbl_gng_mongo_image_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening port 10085 for jump-server"
      source_security_group_id = "${module.hbl_jump_server_db_sg_01.security_group_id}"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for self sg"
      source_security_group_id = "${module.hbl_mongo_archive_sg_01.security_group_id}"
    },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening port 10085 for gng app asg"
    #   source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
    # },
    # {
    #   from_port   = 10085
    #   to_port     = 10085
    #   protocol    = "tcp"
    #   description = "Opening 10085 port for gng nginx irp app asg"
    #   source_security_group_id = "${module.hbl_gng_nginx_irp_app_sg.security_group_id}"
    # },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for mbu app sg"
      source_security_group_id = "${module.hbl_mbu_app_sg_01.security_group_id}"
    },
    ]
   ingress_with_cidr_blocks = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      self        = true
      description = "Opening 10085 port for mongodb DC server"
      cidr_blocks = "172.30.71.55/32"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
   ]
  egress_with_cidr_blocks = [
    # {
    #   from_port   = -1
    #   to_port     = -1
    #   protocol    = -1
    #   description = "allow all traffic"
    #   cidr_blocks = "0.0.0.0/0"
    # },
  ]
  tags = merge(
    local.tags,
    { "application" = "mongo-archive" },
    { "tier" = "db" },
    { "service" = "gng"}
  )
}

# # EC2 FOR MONGO ARCHIVE SERVER
# module "hbl_mongo_archive_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "1"
#   name                    = "${local.aws_account_name}-mongo-archive-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.xlarge"
#   subnet_id               = element(local.data_subnet_ids_mongodb, count.index)
#   private_ip              = local.hbl_mongo_archive_ec2_01_private_ip
#   vpc_security_group_ids  = [module.hbl_mongo_archive_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mongo_archive_kp_01.key_pair_key_name
#   root_volume_size        = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdh"
#       volume_type = "sc1"
#       volume_size = 16384
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdg"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongo-archive" },
#     { "tier" = "db" },
#     { "service" = "gng"},
#     #{ "teleport" = "hbl-gng_archive-mongodb"}
#   )
# }

# EC2 FOR MONGO ARCHIVE SERVER 1 REFACTORED
module "hbl_mongo_archive_ec2_1" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  name                    = "${local.aws_account_name}-mongo-archive-ec2-01"
  ami                     = local.ami_id_mongodb_v4
  instance_type           = "r5a.4xlarge"
  subnet_id               = element(local.data_subnet_ids_mongodb, 0)
  vpc_security_group_ids  = [module.hbl_mongo_archive_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_mongo_archive_kp_01.key_pair_key_name
  user_data_base64        = base64encode(var.teleport_userdata_database["userdata"])
  root_volume_size        = 20
  enable_volume_tags      = true
  disable_api_termination = true
  tags = merge(
    local.tags,
    { "application" = "mongo-archive" },
    { "tier" = "db" },
    { "service" = "gng"},
    #{ "teleport" = "hbl-gng_archive-mongodb"}
  )
}
# 1ST EBS VOLUME MONGO ARCHIVE SERVER 1
module "hbl_mongo_archive_ec2_1_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mongo_archive_ec2_1]
    availability_zone = element(local.data_subnet_az_mongodb, 0)
    type        = "sc1"
    size        = "16384"
    device_name = "/dev/sdf"
    instance_id = module.hbl_mongo_archive_ec2_1.ec2_complete_id
}
# 2ND EBS VOLUME MONGO ARCHIVE SERVER 1
module "hbl_mongo_archive_ec2_1_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mongo_archive_ec2_1]
    availability_zone = element(local.data_subnet_az_mongodb, 0)
    size              = 20
    device_name = "/dev/sdg"
    instance_id = module.hbl_mongo_archive_ec2_1.ec2_complete_id
}
# 3RD EBS VOLUME MONGO ARCHIVE SERVER 1
module "hbl_mongo_archive_ec2_1_ebs_03" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_mongo_archive_ec2_1]
    availability_zone = element(local.data_subnet_az_mongodb, 0)
    type        = "sc1"
    size        = "2048"
    device_name = "/dev/sdh"
    instance_id = module.hbl_mongo_archive_ec2_1.ec2_complete_id
}

################################################
##########  MONGODB ARCHIVE SERVER 02 ############
################################################

# # EC2 FOR MONGO ARCHIVE SERVER 02
# module "hbl_mongo_archive_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "1"
#   name                    = "${local.aws_account_name}-mongo-archive-ec2-02"
#   ami                     = local.ami_id_mongodb_v4
#   instance_type           = "r6a.xlarge"
#   subnet_id               = local.data_subnet_ids_mongodb[1]
#   vpc_security_group_ids  = [module.hbl_mongo_archive_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mongo_archive_kp_01.key_pair_key_name
#   private_ip              = local.hbl_mongo_archive_ec2_02_private_ip
#   root_volume_size        = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "sc1"
#       volume_size = 16384
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdg"
#       volume_type = "sc1"
#       volume_size = 16384
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdh"
#       volume_type = "sc1"
#       volume_size = 8192
#       encrypted   = true
#     },
#     {
#       device_name = "/dev/sdi"
#       volume_type = "gp3"
#       volume_size = 20
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mongo-archive" },
#     { "tier" = "db" },
#     { "service" = "gng" },
#     #{ "teleport" = "hbl-gng_archive-mongodb" }
#   )
# }
