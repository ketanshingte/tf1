# CREATING AN S3 BUCKET FOR CLOUDTRAIL LOGS.
module "hbl_cloudtrail_logs_bucket_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//s3"

  bucket = "${local.aws_account_name}-cloudtail-logs-bucket-01"
  acl = "private"
  restrict_public_buckets = true
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true

  versioning = {
    enabled = true
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_s3_kms_arn}"
        sse_algorithm     = "aws:kms"
         }
    }
  }

  logging = {
    target_bucket = module.hbl_cloudtrail_access_logs_bucket_01.s3_bucket_id
    target_prefix = "hbl/"
  }

  tags = local.tags
}

resource "aws_s3_bucket_policy" "hbl_cloudtrail_logs_bucket_policy_01" {
  bucket = module.hbl_cloudtrail_logs_bucket_01.s3_bucket_id
  policy       = data.aws_iam_policy_document.hbl_cloudtrail_logs_bucket_policy_01.json

}

data "aws_iam_policy_document" "hbl_cloudtrail_logs_bucket_policy_01" {
  statement {
    sid = "AWSCloudTrailAclCheck20150319"

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    actions = ["s3:GetBucketAcl"]

    resources = ["${module.hbl_cloudtrail_logs_bucket_01.s3_bucket_arn}"]

    condition {
      test     = "StringEquals"
      variable = "aws:SourceArn"

      values = [
        "arn:aws:cloudtrail:${local.region}:${local.aws_account_id}:trail/${aws_cloudtrail.hbl_security_trail_01.id}"
      ]
    }
  }

  statement {
    sid = "AWSCloudTrailWrite20150319"

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    actions = ["s3:PutObject"]

    resources = ["${module.hbl_cloudtrail_logs_bucket_01.s3_bucket_arn}/AWSLogs/${local.aws_account_id}/*"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"

      values = [
        "bucket-owner-full-control"
        ]
    }

    condition {
      test     = "StringEquals"
      variable = "aws:SourceArn"

      values = [
        "arn:aws:cloudtrail:${local.region}:${local.aws_account_id}:trail/${aws_cloudtrail.hbl_security_trail_01.id}"
      ]
    }
  }

  statement {
    sid = "AllowSSLRequestsOnly"

    principals {
      type = "*"
      identifiers = ["*"]
    }
    
    effect = "Deny"

    actions = ["s3:*"]

    resources = [
      "${module.hbl_cloudtrail_logs_bucket_01.s3_bucket_arn}",
      "${module.hbl_cloudtrail_logs_bucket_01.s3_bucket_arn}/*"
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = [
        "false"
        ]
    }
  }
}

## CREATING AN S3 BUCKET FOR CLOUDTRAIL ACCESS LOGS.
## NOTE AWS-KMS IS NOT SUPPORTED ONLY AES256 (SSE-S3) IS SUPPORTED
module "hbl_cloudtrail_access_logs_bucket_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//s3"

  bucket = "${local.aws_account_name}-cloudtail-access-logs-bucket-01"
  acl = "private"
  restrict_public_buckets = true
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true

  versioning = {
    enabled = true
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = local.tags
}

resource "aws_s3_bucket_policy" "hbl_cloudtrail_access_logs_bucket_policy_01" {
  bucket = module.hbl_cloudtrail_access_logs_bucket_01.s3_bucket_id
  policy       = data.aws_iam_policy_document.hbl_cloudtrail_access_logs_bucket_policy_01.json

}

data "aws_iam_policy_document" "hbl_cloudtrail_access_logs_bucket_policy_01" {
  statement {
    sid = "S3PolicyStmt"

    principals {
      type        = "Service"
      identifiers = ["logging.s3.amazonaws.com"]
    }

    actions = ["s3:PutObject"]

    resources = ["arn:aws:s3:::${module.hbl_cloudtrail_access_logs_bucket_01.s3_bucket_id}/*"]
    
    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"

      values = [
        "arn:aws:s3:::${module.hbl_cloudtrail_logs_bucket_01.s3_bucket_id}"
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "aws:SourceAccount"

      values = [
        "${local.aws_account_id}"
        ]
    }
  }

  statement {
    sid = "AllowSSLRequestsOnly"

    principals {
      type = "*"
      identifiers = ["*"]
    }
    
    effect = "Deny"

    actions = ["s3:*"]

    resources = [
      "${module.hbl_cloudtrail_access_logs_bucket_01.s3_bucket_arn}",
      "${module.hbl_cloudtrail_access_logs_bucket_01.s3_bucket_arn}/*"
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = [
        "false"
        ]
    }
  }
}

#CREATING THE ROLE POLICY DOCUMENT FOR CLOUDTRAIL TO PUSH LOGS IN CLOUDWATCH 
data "aws_iam_policy_document" "hbl_cloudtrail_role_policy_document_01" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

## CREATING CUSTOM ROLE POLICY FOR CLOUDWATCH
resource "aws_iam_role_policy" "hbl_cloudtrail_role_policy_01" {
  name = "${local.aws_account_name}-assume-role-cross-account-policy-01"
  role = aws_iam_role.hbl_cloudtrail_role_01.id
 policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailCreateLogStream2014110",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream"
            ],
            "Resource": [
                "arn:aws:logs:${local.region}:${local.aws_account_id}:log-group:${aws_cloudwatch_log_group.hbl_security_log_group_01.name}:log-stream:${local.aws_account_id}_CloudTrail_${local.region}*"
            ]
        },
        {
            "Sid": "AWSCloudTrailPutLogEvents20141101",
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:${local.region}:${local.aws_account_id}:log-group:${aws_cloudwatch_log_group.hbl_security_log_group_01.name}:log-stream:${local.aws_account_id}_CloudTrail_${local.region}*"
            ]
        }
    ]
}
EOF
}

#CREATING THE ROLE FOR CLOUDTRAIL TO PUSH LOGS IN CLOUDWATCH 
resource "aws_iam_role" "hbl_cloudtrail_role_01" {
  name                = "${local.aws_account_name}-cloudtrail-role-01"
  path               = "/system/"
  assume_role_policy  = data.aws_iam_policy_document.hbl_cloudtrail_role_policy_document_01.json
  tags = local.tags
}

## CREATING CLOUDWATCH LOG GROUP
resource "aws_cloudwatch_log_group" "hbl_security_log_group_01" {
  name = "${local.aws_account_name}-security-log-group-01"
  tags = local.tags
  retention_in_days = 7
  kms_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_others_kms_arn}"
}

## NOTE 
## WE TRIED USING OTHERS KMS KEY TO ENCRYPT CLOUDTRAIL
## ISSUE - CLOUDTRAIL SENDS OBJECTS ENCRYPTED WITH OTHERS KEY TO S3 BUCKET WHILE S3 BUCKET ENCRYPTS WITH S3 KMS KEY SO WE UNABLE TO DECRYPT  2 DIFFERENT ENCRYPTION ON SAME OBJECT
## SOLUTION - WE USED S3 KMS KEY FOR CLOUDTRAIL
## CREATING CLOUDTRAIL
resource "aws_cloudtrail" "hbl_security_trail_01" {
  name                          = "${local.aws_account_name}-security-trail-01"
  s3_bucket_name                = module.hbl_cloudtrail_logs_bucket_01.s3_bucket_id
  include_global_service_events = true
  cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.hbl_security_log_group_01.arn}:*"
  cloud_watch_logs_role_arn = "${aws_iam_role.hbl_cloudtrail_role_01.arn}"
  enable_logging = true
  is_multi_region_trail = true
  is_organization_trail = false
  enable_log_file_validation = true
  kms_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_s3_kms_arn}"
  tags = local.tags
}

###########################################
###### AuthorizationFailureCount ##########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER AuthorizationFailureCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_AuthorizationFailureCount_01" {
  name           = "AuthorizationFailureCount"
  pattern        = <<EOT
  { ($.errorCode = "*UnauthorizedOperation") || ($.errorCode = "AccessDenied*") }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "AuthorizationFailureCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER AuthorizationFailureCount ALARM
module "hbl_AuthorizationFailureCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-AuthorizationFailureCount-alarm-01" 
  alarm_description   = "AuthorizationFailureCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 3
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "AuthorizationFailureCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
#### ConsoleSignInFailuresCount ###########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER ConsoleSignInFailuresCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_ConsoleSignInFailuresCount_01" {
  name           = "ConsoleSignInFailuresCount"
  pattern        = <<EOT
  { ($.eventName = ConsoleLogin) && ($.errorMessage = "Failed authentication") }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "ConsoleSignInFailuresCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER ConsoleSignInFailuresCount ALARM
module "hbl_ConsoleSignInFailuresCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-ConsoleSignInFailuresCount-alarm-01" 
  alarm_description   = "ConsoleSignInFailuresCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 3
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "ConsoleSignInFailuresCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
########## RootAccountLoginCount ##########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER RootAccountLoginCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_RootAccountLoginCount_01" {
  name           = "RootAccountLoginCount"
  pattern        = <<EOT
  { $.userIdentity.type = "Root" && $.userIdentity.invokedBy NOT EXISTS && $.eventType != "AwsServiceEvent" }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "RootAccountLoginCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER RootAccountLoginCount ALARM
module "hbl_RootAccountLoginCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-RootAccountLoginCount-alarm-01" 
  alarm_description   = "RootAccountLoginCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "RootAccountLoginCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
#### IAMPolicyChangesEventCount ###########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER IAMPolicyChangesEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_IAMPolicyChangesEventCount_01" {
  name           = "IAMPolicyChangesEventCount"
  pattern        = <<EOT
  {($.eventName=DeleteGroupPolicy)||($.eventName=DeleteRolePolicy)||($.eventName=DeleteUserPolicy)||($.eventName=PutGroupPolicy)||($.eventName=PutRolePolicy)||($.eventName=PutUserPolicy)||($.eventName=CreatePolicy)||($.eventName=DeletePolicy)||($.eventName=CreatePolicyVersion)||($.eventName=DeletePolicyVersion)||($.eventName=AttachRolePolicy)||($.eventName=DetachRolePolicy)||($.eventName=AttachUserPolicy)||($.eventName=DetachUserPolicy)||($.eventName=AttachGroupPolicy)||($.eventName=DetachGroupPolicy)}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "IAMPolicyChangesEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER IAMPolicyChangesEventCount ALARM
module "hbl_IAMPolicyChangesEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-IAMPolicyChangesEventCount-alarm-01" 
  alarm_description   = "IAMPolicyChangesEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "IAMPolicyChangesEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
###### CloudTrailChangesEventCount ########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER CloudTrailChangesEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_CloudTrailChangesEventCount_01" {
  name           = "CloudTrailChangesEventCount"
  pattern        = <<EOT
  { ($.eventName = CreateTrail) || ($.eventName = UpdateTrail) || ($.eventName = DeleteTrail) || ($.eventName = StartLogging) || ($.eventName = StopLogging) }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "CloudTrailChangesEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER CloudTrailChangesEventCount ALARM
module "hbl_CloudTrailChangesEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-CloudTrailChangesEventCount-alarm-01" 
  alarm_description   = "CloudTrailChangesEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "CloudTrailChangesEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
##### KMSKeyPendingDeletionErrorCount #####
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER KMSKeyPendingDeletionErrorCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_KMSKeyPendingDeletionErrorCount_01" {
  name           = "KMSKeyPendingDeletionErrorCount"
  pattern        = <<EOT
  {($.eventSource=kms.amazonaws.com) && (($.eventName=DisableKey) || ($.eventName=ScheduleKeyDeletion))}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "KMSKeyPendingDeletionErrorCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER KMSKeyPendingDeletionErrorCount ALARM
module "hbl_KMSKeyPendingDeletionErrorCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-KMSKeyPendingDeletionErrorCount-alarm-01" 
  alarm_description   = "KMSKeyPendingDeletionErrorCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "KMSKeyPendingDeletionErrorCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
####### S3BucketActivityEventCount ########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER S3BucketActivityEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_S3BucketActivityEventCount_01" {
  name           = "S3BucketActivityEventCount"
  pattern        = <<EOT
  { ($.eventSource = s3.amazonaws.com) && (($.eventName = PutBucketAcl) || ($.eventName = PutBucketPolicy) || ($.eventName = PutBucketCors) || ($.eventName = PutBucketLifecycle) || ($.eventName = PutBucketReplication) || ($.eventName = DeleteBucketPolicy) || ($.eventName = DeleteBucketCors) || ($.eventName = DeleteBucketLifecycle) || ($.eventName = DeleteBucketReplication)) }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "S3BucketActivityEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER S3BucketActivityEventCount ALARM
module "hbl_S3BucketActivityEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-S3BucketActivityEventCount-alarm-01" 
  alarm_description   = "S3BucketActivityEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "S3BucketActivityEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
########### AWSConfigChangesCount #########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER AWSConfigChangesCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_AWSConfigChangesCount_01" {
  name           = "AWSConfigChangesCount"
  pattern        = <<EOT
  { ($.eventSource = config.amazonaws.com) && (($.eventName = StopConfigurationRecorder)||($.eventName = DeleteDeliveryChannel)||($.eventName = PutDeliveryChannel)||($.eventName = PutConfigurationRecorder)) }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "AWSConfigChangesCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER AWSConfigChangesCount ALARM
module "hbl_AWSConfigChangesCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-AWSConfigChangesCount-alarm-01" 
  alarm_description   = "AWSConfigChangesCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "AWSConfigChangesCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
####### SecurityGroupEventCount ###########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER SecurityGroupEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_SecurityGroupEventCount_01" {
  name           = "SecurityGroupEventCount"
  pattern        = <<EOT
  { ($.eventName = AuthorizeSecurityGroupIngress) || ($.eventName = AuthorizeSecurityGroupEgress) || ($.eventName = RevokeSecurityGroupIngress) || ($.eventName = RevokeSecurityGroupEgress) || ($.eventName = CreateSecurityGroup) || ($.eventName = DeleteSecurityGroup) }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "SecurityGroupEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER SecurityGroupEventCount ALARM
module "hbl_SecurityGroupEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-SecurityGroupEventCount-alarm-01" 
  alarm_description   = "SecurityGroupEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "SecurityGroupEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
########## NetworkACLEventCount ###########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER NetworkACLEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_NetworkACLEventCount_01" {
  name           = "NetworkACLEventCount"
  pattern        = <<EOT
  { ($.eventName = CreateNetworkAcl) || ($.eventName = CreateNetworkAclEntry) || ($.eventName = DeleteNetworkAcl) || ($.eventName = DeleteNetworkAclEntry) || ($.eventName = ReplaceNetworkAclEntry) || ($.eventName = ReplaceNetworkAclAssociation)}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "NetworkACLEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER NetworkACLEventCount ALARM
module "hbl_NetworkACLEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-NetworkACLEventCount-alarm-01" 
  alarm_description   = "NetworkACLEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "NetworkACLEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
########## GatewayEventCount ##############
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER GatewayEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_GatewayEventCount_01" {
  name           = "GatewayEventCount"
  pattern        = <<EOT
  {($.eventName=CreateCustomerGateway) || ($.eventName=DeleteCustomerGateway) || ($.eventName=AttachInternetGateway) || ($.eventName=CreateInternetGateway) || ($.eventName=DeleteInternetGateway) || ($.eventName=DetachInternetGateway)}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "GatewayEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER GatewayEventCount ALARM
module "hbl_GatewayEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-GatewayEventCount-alarm-01" 
  alarm_description   = "GatewayEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "GatewayEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
############## VPCEventCount ##############
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER VPCEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_VPCEventCount_01" {
  name           = "VPCEventCount"
  pattern        = <<EOT
  {($.eventName=CreateVpc) || ($.eventName=DeleteVpc) || ($.eventName=ModifyVpcAttribute) || ($.eventName=AcceptVpcPeeringConnection) || ($.eventName=CreateVpcPeeringConnection) || ($.eventName=DeleteVpcPeeringConnection) || ($.eventName=RejectVpcPeeringConnection) || ($.eventName=AttachClassicLinkVpc) || ($.eventName=DetachClassicLinkVpc) || ($.eventName=DisableVpcClassicLink) || ($.eventName=EnableVpcClassicLink)}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "VPCEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER VPCEventCount ALARM
module "hbl_VPCEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-VPCEventCount-alarm-01" 
  alarm_description   = "VPCEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "VPCEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
###### RouteTableChangesEventCount ########
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER RouteTableChangesEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_RouteTableChangesEventCount_01" {
  name           = "RouteTableChangesEventCount"
  pattern        = <<EOT
  {($.eventName=CreateRoute) || ($.eventName=CreateRouteTable) || ($.eventName=ReplaceRoute) || ($.eventName=ReplaceRouteTableAssociation) || ($.eventName=DeleteRouteTable) || ($.eventName=DeleteRoute) || ($.eventName=DisassociateRouteTable)}
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "RouteTableChangesEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER RouteTableChangesEventCount ALARM
module "hbl_RouteTableChangesEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-RouteTableChangesEventCount-alarm-01" 
  alarm_description   = "RouteTableChangesEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "RouteTableChangesEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}

###########################################
#### MgmtConsoleSignInMFAEventCount #######
###########################################

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER MgmtConsoleSignInMFAEventCount
resource "aws_cloudwatch_log_metric_filter" "hbl_log_group_metric_filter_MgmtConsoleSignInMFAEventCount_01" {
  name           = "MgmtConsoleSignInMFAEventCount"
  pattern        = <<EOT
  { ($.eventName = "ConsoleLogin") && ($.additionalEventData.MFAUsed != "Yes") && ($.userIdentity.type = "IAMUser") && ($.responseElements.ConsoleLogin = "Success") }
  EOT
  log_group_name = aws_cloudwatch_log_group.hbl_security_log_group_01.name

  metric_transformation {
    name      = "MgmtConsoleSignInMFAEventCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

## CREATING CLOUDWATCH LOG GROUP METRIC FILTER MgmtConsoleSignInMFAEventCount ALARM
module "hbl_MgmtConsoleSignInMFAEventCount_alarm_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

  alarm_name          = "${local.aws_account_name}-MgmtConsoleSignInMFAEventCount-alarm-01" 
  alarm_description   = "MgmtConsoleSignInMFAEventCount metric alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 300

  namespace   = "CloudTrailMetrics"
  metric_name = "MgmtConsoleSignInMFAEventCount"
  statistic   = "Sum"
  tags = local.tags
  alarm_actions = [module.hbl_security_alerts_sns_topic_01.sns_topic_arn]
}