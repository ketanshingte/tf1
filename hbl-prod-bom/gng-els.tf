# #LOCALS FOR GNG ELS DB
# locals {
#   ami_id_gng_els_db          = "ami-018ea12ac24443597"
#   data_subnet_ids_gng_els_db = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
#   data_subnet_az_gng_els_db  = ["${local.region}a", "${local.region}b", "${local.region}c"]
#   gng_els_db_count           = 4
# }


# ################################################
# #############  GNG ELS DB CLUSTER ##############
# ################################################

# #KEYPAIR GNG ELS CLUSTER
# module "hbl_gng_els_db_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-els-db-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDPoEM5zzGInckkXfR0E7NM++Usi96CXfUjK6DbeeHrW8ZHQ94j00WFlKYfCMtCP8aj4cd6gHS/Qd3qAqklcUkQq9M6l0jaIO6aPdQNUOxzquQlWh3rI/hOGUGKGS5fRf4Tr5id5sdCEO+cm7kk+GdriDaIyp3nU5xqBQQ9ASF7dlJqUFLQrZsYfKub4+drnz6iKLxLtW6Uwj6kxgw9zPGDyARs66KdB8+oD3Kn12IUYkN4FnVQsrbPHWxHpot9fCUjKnKzFPFhG2qmMI/3ucUg6gPpeCvMg+SdWe9pyHVVISOGQEGqkKfDPOzEc8hLkvn9E8OdbmlM7hosJ1bcBYBhTYncwmtrPjc+EhigekVgFs0CoxqbE8Bz/t656OleTj44HgQ5ApKlttBphNT41rfvCwJ0UeDOeiv2kkDZBdOSaCCJqqeNIS78uZxgxPa7hqWfeGbfF5XkkbS641+ECEYypXyQMXIbfPrptUiE+KW1P+Bc3E8FONEZcBEgYUBW8M="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-els-db" },
#     { "tier" = "db" }
#   )
# }

# # EC2 GNG ELS DB CLUSTER REFACTORED
# module "hbl_gng_els_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = local.gng_els_db_count
#   name                    = "${local.aws_account_name}-gng-els-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_gng_els_db
#   instance_type           = "m6a.xlarge" # TO BE CHANGED TO m6a.xlarge
#   subnet_id               = element(local.data_subnet_ids_gng_els_db, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_els_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_els_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "gng-els-db" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-gng-els-db"}
#   )
# }

# # EBS GNG ELS DB CLUSTER REFACTORED
# module "hbl_gng_els_db_ec2_01_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_els_db_ec2_01]
#     count                   = local.gng_els_db_count

#     availability_zone = element(local.data_subnet_az_gng_els_db, count.index)
#     size              = 50
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_els_db_ec2_01[count.index].ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng-red-db-ec2-${format("%02d", count.index + 1)}-ebs-01"},
#     { "application" = "gng-els-db" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-gng-els-db"},
#     local.tags)  
# }


# # SECURITY GROUP GNG ELS DB CLUSTER
# module "hbl_gng_els_db_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-els-db-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 9202
#       to_port     = 9202
#       protocol    = "tcp"
#       description = "Opening 9202 port for self sg"
#       source_security_group_id = "${module.hbl_gng_els_db_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 9202
#       to_port     = 9202
#       protocol    = "tcp"
#       description = "Opening 9202 port for gng app asg"
#       source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 9202
#       to_port     = 9202
#       protocol    = "tcp"
#       description = "Opening 9202 port for gng od app asg"
#       source_security_group_id = "${module.hbl_gng_od_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 9202
#       to_port     = 9202
#       protocol    = "tcp"
#       description = "Opening 9202 port for gng_tw app asg"
#       source_security_group_id = "${module.hbl_gng_tw_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 9303
#       to_port     = 9303
#       protocol    = "tcp"
#       description = "Opening 9303 port for self sg"
#       source_security_group_id = "${module.hbl_gng_els_db_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 9303
#       to_port     = 9303
#       protocol    = "tcp"
#       description = "Opening 9303 port for gng app asg"
#       source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 9303
#       to_port     = 9303
#       protocol    = "tcp"
#       description = "Opening 9303 port for gng od app asg"
#       source_security_group_id = "${module.hbl_gng_od_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 9303
#       to_port     = 9303
#       protocol    = "tcp"
#       description = "Opening 9303 port for gng_tw app asg"
#       source_security_group_id = "${module.hbl_gng_tw_app_sg.security_group_id}"
#     },
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range "
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-els-db" },
#     { "tier" = "db" }
#   )
# }