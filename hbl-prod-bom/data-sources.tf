##NETWORK ACCOUNT TERRAFORM STATE SHARED WITH THIS ACCOUNT
data "terraform_remote_state" "network_tf_state" {
  backend   = "s3"
  config = {
    bucket = "shared-terraformstate-infra-s3-1"
    key    = "network.tfstate"
    region  = "ap-south-1"
    role_arn       = "arn:aws:iam::105828129156:role/terraform-admin"
  }
}

##DATASOURCE S3 BUCKET NAME FROM LOGS ACCOUNT REMOTE STATE
data "terraform_remote_state" "logs_tf_state" {
  backend   = "s3"
  config = {
    bucket = "shared-terraformstate-infra-s3-1"
    key    = "logs.tfstate"
    region  = "ap-south-1"
    role_arn       = "arn:aws:iam::105828129156:role/terraform-admin"
  }
}

##DATASOURCE AUDIT ACCOUNT TERRAFORM STATE SHARED WITH THIS ACCOUNT
data "terraform_remote_state" "audit_tf_state" {
  backend   = "s3"
  config = {
    bucket = "shared-terraformstate-infra-s3-1"
    key    = "audit.tfstate"
    region  = "ap-south-1"
    role_arn       = "arn:aws:iam::105828129156:role/terraform-admin"
  }
}

##DATASOURCE LDT PROD ACCOUNT TERRAFORM STATE SHARED WITH THIS ACCOUNT
data "terraform_remote_state" "ldtprod_tf_state" {
  backend   = "s3"
  config = {
    bucket = "shared-terraformstate-infra-s3-1"
    key    = "ldtprod.tfstate"
    region  = "ap-south-1"
    role_arn       = "arn:aws:iam::105828129156:role/terraform-admin"
  }
}

##DATASOURCE FOR NLB IPS
locals {
  nlb_interface_ids = "${flatten(["${data.aws_network_interfaces.this.ids}"])}"
}

data "aws_network_interfaces" "this" {
  filter {
    name = "description"
    values = ["ELB net/${local.aws_account_name}-shared-prod-app-nlb/*"]
  }
  filter {
    name = "vpc-id"
    values = ["${module.hbl_networking_vpc_01.vpc_id}"]
  }
  filter {
    name = "status"
    values = ["in-use"]
  }
  filter {
    name = "attachment.status"
    values = ["attached"]
  }
}

data "aws_network_interface" "ifs" {
  count = "${length(local.nlb_interface_ids)}"
  id = "${local.nlb_interface_ids[count.index]}"
}
