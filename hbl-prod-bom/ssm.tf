# CREATING SSM DOCUMENT FOR TELEPORT INSTALL
module "hbl_install_teleport_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"

    name            = "${local.aws_account_name}-install-teleport-ssm-document-01"
    document_format = "JSON"
    document_type   = "Command"
    tags            = local.tags
    content         = <<DOC
    {
  "schemaVersion": "2.2",
  "description": "This SSM Automation Document is used to install and configure teleport on EC2 & to join the cluster.",
  "parameters": {
    "TimeoutSeconds": {
      "type": "String",
      "description": "(Optional) The time in seconds for a command to be completed before it is considered to have failed.",
      "default": "3600"
    },
    "IAMRoleARN": {
      "type": "String",
      "description": "IAM Assume role ARN from Sharedservices Account",
      "default": "arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01"
    },
    "OutputProfile": {
      "type": "String",
      "description": "Output Profile",
      "default": "cross-account-shared-services-access"
    },
    "TokenParameterStore": {
      "type": "String",
      "description": "Name of Token Parameter Store from Sharedservices Account",
      "default": "/teleport/sharedservices-teleport/tokens/node"
    },
    "CAPINHashParameterStore": {
      "type": "String",
      "description": "Name of CA PIN HASH Parameter Store from Sharedservices Account",
      "default": "/teleport/sharedservices-teleport/ca-pin-hash"
    },
    "TeleportVersionParameterStore": {
      "type": "String",
      "description": "Name of teleport version Parameter Store from Sharedservices Account",
      "default": "teleport-v14.2.0-linux-amd64-bin.tar.gz"
    }
  },
  "mainSteps": [
    {
      "action": "aws:runShellScript",
      "name": "runShellScript",
      "inputs": {
        "timeoutSeconds": "{{ TimeoutSeconds }}",
        "runCommand": [
          "#!/bin/bash",
          "pkill -f teleport",
          "rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh",
          "rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt",
          "rm -rf /opt/teleport-location",
          "rm -rf /etc/systemd/system/teleport.service",
          "##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########",
          "ROLE_ARN={{ IAMRoleARN }}",
          "OUTPUT_PROFILE={{ OutputProfile }}",
          "aws configure set region ap-south-1 --profile $OUTPUT_PROFILE",
          "aws configure set output text --profile $OUTPUT_PROFILE",
          "echo \"Assuming role $ROLE_ARN and create temporary credentials\"",
          "sts=$(aws sts assume-role --role-arn \"$ROLE_ARN\" --role-session-name \"$OUTPUT_PROFILE\" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)",
          "sts=($sts)",
          "aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE",
          "aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE",
          "aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE",
          "echo \"credentials stored in the profile named $OUTPUT_PROFILE\"",
          "#environment variable for the location path of auth join tokens in the SSM parameter store",
          "export TOKEN=\"{{ TokenParameterStore }}\"",
          "export CAPIN=\"{{ CAPINHashParameterStore }}\"",
          "#environment variable of the teleport version",
          "export TeleportVersion=\"{{ TeleportVersionParameterStore }}\"",
          "#environment variable of the auth server ip address",
          "export AUTH_SERVER_URL=\"02949425121.serviceurl.in\"",
          "#To execute and store the installation files with date",
          "exec > /var/log/teleport-installation-files-$(date +\"%Y-%m-%d\").log 2>&1",
          "echo 'Creating install directories...'",
          "mkdir -p /opt/teleport-location",
          "mkdir -p /var/lib/teleport/auth-pin",
          "TELEPORTDIR=/opt/teleport-location",
          "#download the teleport release",
          "yum install perl-Digest-SHA -y",
          "curl https://get.gravitational.com/$TeleportVersion.sha256",
          "wget -c https://get.gravitational.com/$TeleportVersion -O - | tar -xz -C $TELEPORTDIR",
          "wget -c https://get.gravitational.com/$TeleportVersion",
          "##############To compare checksums##########################",
          "if diff -iq <(curl \"https://get.gravitational.com/$TeleportVersion.sha256\") <(shasum -a 256 \"$TeleportVersion\") > /dev/null ; then",
          "    echo \"checksum matches\"",
          "else",
          "    echo \"checksum differ\"",
          "fi",
          "rm $TeleportVersion",
          "echo \"Installing teleport...\"",
          "#install the teleport",
          "$TELEPORTDIR/teleport/install",
          "##############To configure teleport##########################",
          "$TELEPORTDIR/teleport/install",
          "cat > /etc/systemd/system/teleport.service <<- \"EOF\"",
          "[Unit]",
          "Description=Teleport SSH Service",
          "After=network.target",
          "[Service]",
          "Type=simple",
          "Restart=on-failure",
          "EnvironmentFile=-/etc/default/teleport",
          "ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid",
          "ExecReload=/bin/kill -HUP $MAINPID",
          "PIDFile=/run/teleport.pid",
          "LimitNOFILE=8192",
          "KillMode=process",
          "[Install]",
          "WantedBy=multi-user.target",
          "EOF",
          "touch /var/lib/teleport/token",
          "touch /var/lib/teleport/ca-pin",
          "#fetch the join token from the ssm parameter store",
          "aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption",
          "aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin",
          "#Write Teleport configuration file.",
          "cat >/etc/teleport.yaml <<EOF",
          "teleport:",
          "    data_dir: /var/lib/teleport",
          "    auth_token: /var/lib/teleport/token",
          "    ca_pin: /var/lib/teleport/ca-pin",
          "    auth_servers:",
          "            -  $AUTH_SERVER_URL:443",
          "    connection_limits:",
          "        max_connections: 1000",
          "        max_users: 250",
          "    log:",
          "        output: stderr",
          "        severity: info",
          "auth_service:",
          "    enabled: false",
          "ssh_service:",
          "    enabled: true",
          "    enhanced_recording:",
          "    # Enable or disable enhanced auditing for this node. Default value: false.",
          "     enabled: true",
          "     command_buffer_size: 8",
          "    # Optional: disk_buffer_size is optional with default value of 128 pages.",
          "     disk_buffer_size: 128",
          "    # Optional: network_buffer_size is optional with default value of 8 pages.",
          "     network_buffer_size: 8",
          "    # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:",
          "    # /cgroup2.",
          "     cgroup_path: /cgroup2",
          "proxy_service:",
          "    enabled: false",
          "EOF",
          "file=\"/etc/teleport.yaml\"",
          "if [ ! -f \"$file\" ]",
          "then",
          "    echo \"$0: teleport configuration file '$${file}' not found.\"",
          "else ",
          "echo \"teleport configuration file is being created\" >>/var/log/teleport-installation-files-$(date +\"%Y-%m-%d\").log",
          "fi",
          "systemctl daemon-reload",
          "systemctl enable teleport",
          "systemctl start teleport",
          "systemctl status teleport",
          "#############To check if the teleport service is running###############",
          "STATUS=\"$(systemctl is-active teleport.service)\"",
          "if [ \"$${STATUS}\" = \"active\" ]; then",
          "    echo \"teleport service is running\"",
          "else",
          "    echo \"Service not running..\"",
          "    exit 1",
          "fi"
        ]
      }
    }
  ]
}
DOC
}

# CREATING SSM DOCUMENT FOR ZABBIX AGENT INSTALL
module "hbl_install_zabbix_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"

    name            = "${local.aws_account_name}-install-zabbix-ssm-document-01"
    document_format = "YAML"
    document_type   = "Command"
    tags            = local.tags
    content         = <<DOC
---
description: "This SSM document will be used for Zabbix agent installation."
schemaVersion: "2.2"
parameters:
  TargetServerType:
    type: "String"
    allowedValues:
    - app
    - db
    description: "select target server is app or db"
mainSteps:
- action: aws:runShellScript
  name: 'ConfigureZabbixAgent'
  inputs:
    timeoutSeconds: 60
    runCommand:
      - "#!/bin/bash"
      - "#"
      - "# This script is generic configured to run on the APP and DB servers."
      - ""
      - "set -o errexit -o pipefail -o nounset"
      - "declare -r GREP_CMD=\"/bin/grep\""
      - "declare -r YUM_CMD=\"/bin/yum\""
      - "declare -r SYSTEMCTL_CMD=\"/bin/systemctl\""
      - "declare -r RPM_CMD=\"/bin/rpm\""
      - "declare -r CP_CMD=\"/bin/cp\""
      - "declare -r CAT_CMD=\"/bin/cat\""
      - "declare -r VIM_CMD=\"/bin/vim\""
      - "declare -r ZABBIX_AGENT_FILE=\"/etc/zabbix/zabbix_agentd.conf\""
      - "declare -r SUCCESS=0"
      - "declare -r FAILURE=1"
      - ""
      - "#########################################################################"
      - "#  Install Zabbix repository & zabbix-agent                             #"  
      - "#########################################################################"
      - ""
      - "if ! rpm -qa | grep -i zabbix-release; then"
      - "$${RPM_CMD} -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/7/x86_64/zabbix-release-6.0-4.el7.noarch.rpm"
      - "$${YUM_CMD} clean all"
      - "$${YUM_CMD} install zabbix-agent -y"
      - "$${CP_CMD} $${ZABBIX_AGENT_FILE} $${ZABBIX_AGENT_FILE}_$(date +%Y%m%d)"
      - ""
      - ""
      - "$${CAT_CMD} <<'EOF' > $${ZABBIX_AGENT_FILE}"
      - "HostnameItem=system.hostname"
      - "HostMetadata=Linux {{TargetServerType}}"
      - "Include=/etc/zabbix/zabbix_agentd.d/*.conf"
      - "LogFileSize=0"
      - "LogFile=/var/log/zabbix/zabbix_agentd.log"
      - "PidFile=/run/zabbix/zabbix_agentd.pid"
      - "Server=zabbix-mgr-01.aws.serviceurl.internal,zabbix-mgr-02.aws.serviceurl.internal"
      - "ServerActive=zabbix-mgr-01.aws.serviceurl.internal;zabbix-mgr-02.aws.serviceurl.internal"
      - "EOF"
      - ""
      - ""
      - "$${SYSTEMCTL_CMD} restart zabbix-agent"
      - "$${SYSTEMCTL_CMD} enable zabbix-agent"
      - ""
      - "if [ $? -eq $${SUCCESS} ]; then"
      - 'echo "Successfully Installed in $(hostname -I)";'
      - "else"
      - 'echo "Something is wrong in $(hostname -I)";'
      - "fi"
      - ""
      - "else"
      - "if ! rpm -qa | grep -i zabbix-agent; then"
      - "$${YUM_CMD} clean all"
      - "$${YUM_CMD} install zabbix-agent -y"
      - "$${CP_CMD} $${ZABBIX_AGENT_FILE} $${ZABBIX_AGENT_FILE}_$(date +%Y%m%d)"
      - ""
      - ""
      - "$${CAT_CMD} <<'EOF' > $${ZABBIX_AGENT_FILE}"
      - "HostMetadataItem=system.uname"
      - "HostMetadata=Linux {{TargetServerType}}"
      - "Include=/etc/zabbix/zabbix_agentd.d/*.conf"
      - "LogFileSize=0"
      - "LogFile=/var/log/zabbix/zabbix_agentd.log"
      - "PidFile=/run/zabbix/zabbix_agentd.pid"
      - "Server=zabbix-mgr-01.aws.serviceurl.internal,zabbix-mgr-02.aws.serviceurl.internal"
      - "ServerActive=zabbix-mgr-01.aws.serviceurl.internal;zabbix-mgr-02.aws.serviceurl.internal"
      - "EOF"
      - ""
      - ""
      - "$${SYSTEMCTL_CMD} restart zabbix-agent"
      - "$${SYSTEMCTL_CMD} enable zabbix-agent"
      - ""
      - "if [ $? -eq $${SUCCESS} ]; then"
      - 'echo "Successfully Installed in $(hostname -I)";'
      - "else"
      - 'echo "Something is wrong in $(hostname -I)";'
      - "fi"
      - "else"
      - 'echo "Already Installed in $(hostname -I)";'
      - "fi"
      - "fi"
DOC
}


# CREATING SSM DOCUMENT FOR UPDATING THE VERSION OF TELEPORT NODES
module "hbl_update_teleport_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"
    name            = "${local.aws_account_name}-update-teleport-ssm-document-01"
    document_format = "JSON"
    document_type   = "Command"
    tags            = local.tags
    content         = <<DOC
    {
  "schemaVersion": "2.2",
  "description": "This SSM Automation Document is used to update the version of nodes on teleport.",
  "parameters": {
    "TimeoutSeconds": {
      "type": "String",
      "description": "(Optional) The time in seconds for a command to be completed before it is considered to have failed.",
      "default": "3600"
    },
    "TeleportVersionParameterStore": {
      "type": "String",
      "description": "Name of teleport version Parameter Store from Sharedservices Account",
      "default": "teleport-v14.2.0-linux-amd64-bin.tar.gz"
    }
  },
  "mainSteps": [
    {
      "action": "aws:runShellScript",
      "name": "runShellScript",
      "inputs": {
        "timeoutSeconds": "{{ TimeoutSeconds }}",
        "runCommand": [
          "#!/bin/bash",
          "export TeleportVersion=\"{{ TeleportVersionParameterStore }}\"",
          "#To execute and store the installation files with date",
          "exec > /var/log/teleport-installation-files-$(date +\"%Y-%m-%d\").log 2>&1",
          "echo 'Creating install directories...'",
          "mkdir -p /opt/teleport-location",
          "TELEPORTDIR=/opt/teleport-location",
          "#download the teleport release",
          "yum install perl-Digest-SHA -y",
          "curl https://get.gravitational.com/$TeleportVersion.sha256",
          "wget -c https://get.gravitational.com/$TeleportVersion -O - | tar -xz -C $TELEPORTDIR",
          "wget -c https://get.gravitational.com/$TeleportVersion",
          "##############To compare checksums##########################",
          "if diff -iq <(curl \"https://get.gravitational.com/$TeleportVersion.sha256\") <(shasum -a 256 \"$TeleportVersion\") > /dev/null ; then",
          "    echo \"checksum matches\"",
          "else",
          "    echo \"checksum differ\"",
          "fi",
          "rm $TeleportVersion",
          "echo \"Installing teleport...\"",
          "#install the teleport",
          "$TELEPORTDIR/teleport/install",
          "##############To configure teleport##########################",
          "$TELEPORTDIR/teleport/install",
          "cat > /etc/systemd/system/teleport.service <<- \"EOF\"",
          "[Unit]",
          "Description=Teleport SSH Service",
          "After=network.target",
          "[Service]",
          "Type=simple",
          "Restart=on-failure",
          "EnvironmentFile=-/etc/default/teleport",
          "ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid",
          "ExecReload=/bin/kill -HUP $MAINPID",
          "PIDFile=/run/teleport.pid",
          "LimitNOFILE=8192",
          "KillMode=process",
          "[Install]",
          "WantedBy=multi-user.target",
          "EOF",
          "systemctl daemon-reload",
          "systemctl enable teleport",
          "systemctl restart teleport.service",
          "systemctl status teleport",
          "#############To check if the teleport service is running###############",
          "STATUS=\"$(systemctl is-active teleport.service)\"",
          "if [ \"$${STATUS}\" = \"active\" ]; then",
          "    echo \"teleport service is running\"",
          "else",
          "    echo \"Service not running..\"",
          "    exit 1",
          "fi",
          "teleport version"
        ]
      }
    }
  ]
}
DOC
}

# CREATING SSM DOCUMENT FOR WAZUH AGENT INSTALL
module "hbl_install_wazuh_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"

    name            = "${local.aws_account_name}-install-wazuh-ssm-document-01"
    document_format = "YAML"
    document_type   = "Command"
    tags            = local.tags
    content         = <<DOC
---
description: "This SSM document will be used for Wazuh agent installation."
schemaVersion: "2.2"
parameters:
  AccountName:
    type: "String"
    description: "select target server account name"
  AppName:
    type: "String"
    description: "select target server aplication name"
  RoleType:
    type: "String"
    allowedValues:
    - webapp
    - db
    description: "select target server is webapp or db"
  WazuhVersion:
    type: "String"
    description: "select wazuh package version"
    default: "https://packages.wazuh.com/4.x/yum/wazuh-agent-4.3.10-1.x86_64.rpm"
mainSteps:
- action: aws:runShellScript
  name: 'ConfigureWazuhAgent'
  inputs:
    timeoutSeconds: 60
    runCommand:
      - "rpm -qa | grep wazuh-agent || WAZUH_MANAGER='02100888889.aws.serviceurl.internal' WAZUH_AGENT_GROUP='default,{{AccountName}},{{ AppName }},{{ RoleType }}' yum install {{ WazuhVersion }} -y"
      - "cat /var/ossec/etc/local_internal_options.conf | grep 'sca.remote_commands=1' || echo 'sca.remote_commands=1' >> /var/ossec/etc/local_internal_options.conf"
      - "systemctl daemon-reload"
      - "systemctl enable wazuh-agent >> /dev/null 2>&1"
      - "systemctl start wazuh-agent"
      - "systemctl status wazuh-agent"

DOC
}

# CREATING SSM DOCUMENT FOR CHECKING STATUS OF ZABBIX AND WAZUH
module "hbl_zabbix_wazuh_installation_check_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"
    name            = "${local.aws_account_name}-zabbix-wazuh-installation-check-ssm-document-01"
    document_format = "JSON"
    document_type   = "Command"
    tags            = local.tags
    content         = <<DOC
    {
  "schemaVersion": "2.2",
  "description": "Check the installation status of zabbix-agent and wazuh-agent services",
  "mainSteps": [
    {
      "name": "CheckZabbixAndWazuhAgents",
      "action": "aws:runShellScript",
      "inputs": {
        "runCommand": [
          "echo zabbix status is :",
          "systemctl is-active zabbix-agent.service",
          "rpm -qa | grep zabbix-agent",
          "echo wazuh status is :",
          "systemctl is-active wazuh-agent.service",
          "rpm -qa | grep wazuh-agent",
          "exit 0"
        ],
        "timeoutSeconds": 60
      }
    }
  ]
}
DOC
}

# CREATING SSM DOCUMENT TO INSTALL WAZUH AGENT ON AMIS
module "hbl_wazuh_ami_ssm_document_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ssm/document"
    name            = "${local.aws_account_name}-wazuh-ami-ssm-document-01"
    document_format = "JSON"
    document_type   = "Automation"
    tags            = local.tags
    content         = <<DOC
{
  "schemaVersion": "0.3",
  "description": "Updates AMI with Linux distribution packages and Amazon software. For details,see https://docs.aws.amazon.com/systems-manager/latest/userguide/automation-awsdocs-linux.html",
  "assumeRole": "{{AutomationAssumeRole}}",
  "outputs": [
    "createImage.ImageId"
  ],
  "parameters": {
    "SourceAmiId": {
      "type": "String",
      "description": "(Required) The source Amazon Machine Image ID."
    },
    "IamInstanceProfileName": {
      "type": "String",
      "description": "(Required) The instance profile that enables Systems Manager (SSM) to manage the instance.",
      "default": "hbl-ec2-access-role-01"
    },
    "AutomationAssumeRole": {
      "type": "String",
      "description": "(Required) The ARN of the role that allows Automation to perform the actions on your behalf.",
      "default": "${aws_iam_role.hbl_wazuh_ami_ssm_doc_role.arn}"
    },
    "TargetAmiName": {
      "type": "String",
      "description": "(Optional) The name of the new AMI that will be created. Default is a system-generated string including the source AMI id, and the creation time and date.",
      "default": "UpdateLinuxAmi_from_{{SourceAmiId}}_on_{{global:DATE_TIME}}"
    },
    "AMINameTag": {
      "type": "String",
      "description": "The Name Tag of the new AMI that will be created. Default is a system-generated string including the source AMI id, and the creation time and date.",
      "default": "UpdateLinuxAmi_from_{{SourceAmiId}}_on_{{global:DATE_TIME}}"
    },
    "InstanceType": {
      "type": "String",
      "description": "(Optional) Type of instance to launch as the workspace host. Instance types vary by region. Default is t3.micro.",
      "default": "t3a.micro"
    },
    "SecurityGroupIds": {
      "type": "StringList",
      "description": "(Optional) A comma separated list of security group IDs with the required Inbound and Outbound connectivity rules.",
      "allowedPattern": "^sg-[a-z0-9]{8,17}$",
      "default": [
        "${module.hbl_wazuh_agent_ami_ssm_doc_sg_01.security_group_id}"
      ]
    },
    "SubnetId": {
      "type": "String",
      "description": "(Optional) Specify the SubnetId if you want to launch EC2 instance in a specific subnet.",
      "default": "subnet-0963087b7258ec6df"
    },
    "PreUpdateScript": {
      "type": "String",
      "description": "(Optional) URL of a script to run before updates are applied. Default (\"none\") is to not run a script.",
      "default": "none"
    },
    "PostUpdateScript": {
      "type": "String",
      "description": "(Optional) URL of a script to run after package updates are applied. Default (\"none\") is to not run a script.",
      "default": "none"
    },
    "IncludePackages": {
      "type": "String",
      "description": "(Optional) Only update these named packages. By default (\"all\"), all available updates are applied.",
      "default": "all"
    },
    "ExcludePackages": {
      "type": "String",
      "description": "(Optional) Names of packages to hold back from updates, under all conditions. By default (\"none\"), no package is excluded.",
      "default": "none"
    },
    "MetadataOptions": {
      "type": "StringMap",
      "description": "(Optional) The metadata options for the instance.",
      "default": {
        "HttpEndpoint": "enabled",
        "HttpTokens": "optional"
      }
    }
  },
  "mainSteps": [
    {
      "name": "launchInstance",
      "action": "aws:runInstances",
      "maxAttempts": 3,
      "timeoutSeconds": 1200,
      "onFailure": "Abort",
      "inputs": {
        "ImageId": "{{SourceAmiId}}",
        "InstanceType": "{{InstanceType}}",
        "UserData": "IyEvYmluL2Jhc2gKCmZ1bmN0aW9uIGdldF9jb250ZW50cygpIHsKICAgIGlmIFsgLXggIiQod2hpY2ggY3VybCkiIF07IHRoZW4KICAgICAgICBjdXJsIC1zIC1mICIkMSIKICAgIGVsaWYgWyAteCAiJCh3aGljaCB3Z2V0KSIgXTsgdGhlbgogICAgICAgIHdnZXQgIiQxIiAtTyAtCiAgICBlbHNlCiAgICAgICAgZGllICJObyBkb3dubG9hZCB1dGlsaXR5IChjdXJsLCB3Z2V0KSIKICAgIGZpCn0KCnJlYWRvbmx5IFRPS0VOX1VSTD0iaHR0cDovLzE2OS4yNTQuMTY5LjI1NC9sYXRlc3QvYXBpL3Rva2VuIgpmdW5jdGlvbiBnZXRfY29udGVudHNfSU1EU3YyKCkgewogICAgaWYgWyAteCAiJCh3aGljaCBjdXJsKSIgXTsgdGhlbgogICAgICAgIHJlYWRvbmx5IFRPS0VOPSQoY3VybCAtcyAtZiAtWCBQVVQgLUggIlgtYXdzLWVjMi1tZXRhZGF0YS10b2tlbi10dGwtc2Vjb25kczogMjE2MDAiICIkVE9LRU5fVVJMIikKICAgICAgICBjdXJsIC1zIC1mIC1IICJYLWF3cy1lYzItbWV0YWRhdGEtdG9rZW46ICRUT0tFTiIgIiQxIgogICAgZWxpZiBbIC14ICIkKHdoaWNoIHdnZXQpIiBdOyB0aGVuCiAgICAgICAgIyB3Z2V0IGRvZXNuJ3Qgc3VwcG9ydCAtLW1ldGhvZCBmbGFnIGluIG1hbnkgY2FzZXMsIHNvIGZhbGwgYmFjayBvbiBJTURTdjEKICAgICAgICB3Z2V0ICIkMSIgLU8gLQogICAgZWxzZQogICAgICAgIGRpZSAiTm8gZG93bmxvYWQgdXRpbGl0eSAoY3VybCwgd2dldCkiCiAgICBmaQp9CgpyZWFkb25seSBJREVOVElUWV9VUkw9Imh0dHA6Ly8xNjkuMjU0LjE2OS4yNTQvMjAxNi0wNi0zMC9keW5hbWljL2luc3RhbmNlLWlkZW50aXR5L2RvY3VtZW50LyIKcmVhZG9ubHkgVFJVRV9SRUdJT049JChnZXRfY29udGVudHNfSU1EU3YyICIkSURFTlRJVFlfVVJMIiB8IGF3ayAtRlwiICcvcmVnaW9uLyB7IHByaW50ICQ0IH0nKQpyZWFkb25seSBERUZBVUxUX1JFR0lPTj0idXMtZWFzdC0xIgpyZWFkb25seSBSRUdJT049IiR7VFJVRV9SRUdJT046LSRERUZBVUxUX1JFR0lPTn0iCgpmdW5jdGlvbiBkaWUoKSB7CiAgICBlY2hvICIkQCwgZXhpdGluZy4iID4mMgogICAgZXhpdCAxCn0KCmZ1bmN0aW9uIGlzc3VlX21hdGNoKCkgewogICAgZ3JlcCAtRSAtaSAtYyAiJDEiIC9ldGMvaXNzdWUgMj4mMSAmPi9kZXYvbnVsbAogICAgWyAkPyAtZXEgMCBdICYmIGVjaG8gInRydWUiIHx8IGVjaG8gImZhbHNlIgp9CgpmdW5jdGlvbiBpc19kZWJ1bnR1KCkgewogICAgZWNobyAiJChpc3N1ZV9tYXRjaCAnRGViaWFufFVidW50dScpIgp9CgpmdW5jdGlvbiBpc19yZWRoYXQoKSB7CiAgICBpZiBbIC1mICIvZXRjL3N5c3RlbS1yZWxlYXNlIiBdIHx8CiAgICAgICAgWyAtZiAiL2V0Yy9yZWRoYXQtcmVsZWFzZSIgXTsgdGhlbgogICAgICAgIGVjaG8gInRydWUiCiAgICBlbHNlCiAgICAgICAgZWNobyAiZmFsc2UiCiAgICBmaQp9CgpmdW5jdGlvbiBpc19zdXNlKCkgewogICAgaWYgWyAtZiAiL2V0Yy9vcy1yZWxlYXNlIiBdOyB0aGVuCiAgICAgICAgeD0kKGdyZXAgLUUgLWkgLWMgInN1c2UiIC9ldGMvb3MtcmVsZWFzZSAyPi9kZXYvbnVsbCkKICAgICAgICBpZiBbICR4IC1ndCAwIF07IHRoZW4KICAgICAgICAgICAgZWNobyAidHJ1ZSIKICAgICAgICBlbHNlCiAgICAgICAgICAgIGVjaG8gImZhbHNlIgogICAgICAgIGZpCiAgICBlbHNlCiAgICAgICAgZWNobyAiZmFsc2UiCiAgICBmaQp9CgpmdW5jdGlvbiBnZXRfYXJjaCgpIHsKICAgIGlmIFsgIiQodW5hbWUgLW0pIiA9PSAieDg2XzY0IiBdOyB0aGVuCiAgICAgICAgZWNobyAiYW1kNjQiCiAgICBlbGlmIFtbICIkKHVuYW1lIC1tKSIgPX4gaVszLTZdODYgXV07IHRoZW4KICAgICAgICBlY2hvICIzODYiCiAgICBlbHNlCiAgICAgICAgZGllICJVbnN1cHBvcnRlZCBhcmNoaXRlY3R1cmUgJCh1bmFtZSAtbSkiCiAgICBmaQp9CgpmdW5jdGlvbiBnZXRfcGFja2FnZV90eXBlKCkgewogICAgaWYgWyAiJChpc19kZWJ1bnR1KSIgPT0gInRydWUiIF07IHRoZW4KICAgICAgICBlY2hvICJkZWIiCiAgICBlbGlmIFsgIiQoaXNfcmVkaGF0KSIgPT0gInRydWUiIF07IHRoZW4KICAgICAgICBlY2hvICJycG0iCiAgICBlbGlmIFsgIiQoaXNfc3VzZSkiID09ICJ0cnVlIiBdOyB0aGVuCiAgICAgICAgZWNobyAicnBtIgogICAgZmkKfQoKCmZ1bmN0aW9uIGdldF9kaXN0KCkgewogICAgaWYgWyAiJChpc19kZWJ1bnR1KSIgPT0gInRydWUiIF07IHRoZW4KICAgICAgICBlY2hvICJkZWJpYW4iCiAgICBlbGlmIFsgIiQoaXNfcmVkaGF0KSIgPT0gInRydWUiIF07IHRoZW4KICAgICAgICBlY2hvICJsaW51eCIKICAgIGVsaWYgW1sgIiQoaXNfc3VzZSkiID09ICJ0cnVlIiBdXTsgdGhlbgogICAgICAgIGVjaG8gImxpbnV4IgogICAgZWxzZQogICAgICAgIGRpZSAiVW5rbm93biBkaXN0cmlidXRpb24iCiAgICBmaQp9CgpmdW5jdGlvbiBnZXRfc3NtX2FnZW50KCkgewogICAgZWNobyAiRmV0Y2hpbmcgU1NNIEFnZW50Li4uIgogICAgZXh0ZW5zaW9uPSIkKGdldF9wYWNrYWdlX3R5cGUpIgogICAgZGlzdD0iJChnZXRfZGlzdCkiCiAgICBhcmNoPSIkKGdldF9hcmNoKSIKCiAgICBwYWNrYWdlPSJhbWF6b24tc3NtLWFnZW50LiRleHRlbnNpb24iCiAgICB1cmxfYmFzZT0iaHR0cHM6Ly9hbWF6b24tc3NtLSRSRUdJT04uczMuYW1hem9uYXdzLmNvbSIKICAgIHVybD0iJHt1cmxfYmFzZX0vbGF0ZXN0LyR7ZGlzdH1fJHthcmNofS8ke3BhY2thZ2V9IgogICAgRklMRV9TSVpFPTAKCiAgICB3aGlsZSBbICRSRVRSWV9DT1VOVCAtbHQgJE1BWF9SRVRSWV9DT1VOVCBdIDsgZG8KICAgICAgZ2V0X2NvbnRlbnRzICIkdXJsIiA+ICIkcGFja2FnZSIKCiAgICAgIGlmIFsgLWYgIiRwYWNrYWdlIiBdOyB0aGVuCiAgICAgICAgRklMRV9TSVpFPSQoZHUgLWsgJHBhY2thZ2UgfCBjdXQgLWYxKQogICAgICAgIGlmIFsgJEZJTEVfU0laRSAtZ3QgMCBdOyB0aGVuCiAgICAgICAgICBicmVhawogICAgICAgIGZpCiAgICAgIGZpCiAgICAgIFJFVFJZX0NPVU5UPSQoKFJFVFJZX0NPVU5UKzEpKTsKICAgICAgZWNobyBBd3MtSW5zdGFsbC1Tc20tQWdlbnQ6IFJldHJ5aW5nIGRvd25sb2FkIHJldHJ5Q291bnQ6ICRSRVRSWV9DT1VOVCwgZmlsZVNpemU6ICRGSUxFX1NJWkUgdXJsOiR1cmwgcGFja2FnZTokcGFja2FnZQogICAgZG9uZQoKICAgIGlmIFsgISAtZiAiJHBhY2thZ2UiIF0gfHwgWyAkRklMRV9TSVpFIC1lcSAwIF07IHRoZW4KICAgICAgICBkaWUgIkNvdWxkIG5vdCBkb3dubG9hZCB0aGUgcGFja2FnZSBmcm9tICR1cmwgYWZ0ZXIgJFJFVFJZX0NPVU5UIHJldHJpZXMiCiAgICBmaQp9CgpmdW5jdGlvbiBzdGFydF9zc21fYWdlbnQoKSB7CiAgICBlY2hvICJTdGFydGluZyBTU00gQWdlbnQuLi4iCiAgICBpZiBbIC14ICIkKHdoaWNoIHN5c3RlbWN0bCkiIF07IHRoZW4KICAgICAgICAgIHN5c3RlbWN0bCBzdGFydCBhbWF6b24tc3NtLWFnZW50CiAgICBlbGlmIFsgLXggIiQod2hpY2ggc3RhcnQpIiBdOyB0aGVuCiAgICAgICAgICBzdGFydCBhbWF6b24tc3NtLWFnZW50CiAgICBlbHNlCiAgICAgICAgICBlY2hvICJDb3VsZCBub3QgZmluZCBjb21tYW5kIHRvIHN0YXJ0IFNTTSBBZ2VudC4gU2tpcHBpbmcgQWdlbnQgc3RhcnQuIgogICAgZmkKfQoKZnVuY3Rpb24gaW5zdGFsbF9zc21fYWdlbnQoKSB7CiAgICBlY2hvICJJbnN0YWxsaW5nIFNTTSBBZ2VudC4uLiIKICAgIHdoaWxlIFsgJFJFVFJZX0NPVU5UIC1sdCAkTUFYX1JFVFJZX0NPVU5UIF0gOyBkbwogICAgICBpZiBbICIkKGlzX2RlYnVudHUpIiA9PSAidHJ1ZSIgXTsgdGhlbgogICAgICAgICMgSWYgYW4gZXhpc3RpbmcgdmVyc2lvbiBpcyBpbnN0YWxsZWQgd2l0aCBzbmFwLCBpdCB3aWxsIGJsb2NrIHVwZ3JhZGUuCiAgICAgICAgc25hcCByZW1vdmUgYW1hem9uLXNzbS1hZ2VudAogICAgICAgIGRwa2cgLWkgYW1hem9uLXNzbS1hZ2VudC5kZWIKICAgICAgZWxpZiBbICIkKGlzX3JlZGhhdCkiID09ICJ0cnVlIiBdOyB0aGVuCiAgICAgICAgeXVtIGluc3RhbGwgLS1ub2dwZ2NoZWNrIC15IGFtYXpvbi1zc20tYWdlbnQucnBtCiAgICAgIGVsaWYgWyAiJChpc19zdXNlKSIgPT0gInRydWUiIF07IHRoZW4KICAgICAgICBycG0gLS1pbnN0YWxsIGFtYXpvbi1zc20tYWdlbnQucnBtCiAgICAgIGVsc2UKICAgICAgICBkaWUgIlVua25vd24gZGlzdHJpYnV0aW9uIgogICAgICBmaQoKICAgICAgaWYgWyAteCAiJCh3aGljaCBhbWF6b24tc3NtLWFnZW50KSIgXTsgdGhlbgogICAgICAgIGJyZWFrCiAgICAgIGZpCiAgICAgIFJFVFJZX0NPVU5UPSQoKFJFVFJZX0NPVU5UKzEpKTsKICAgICAgZWNobyBBd3MtSW5zdGFsbC1Tc20tQWdlbnQ6IFJldHJ5aW5nIGluc3RhbGxhdGlvbiByZXRyeUNvdW50OiAkUkVUUllfQ09VTlQKICAgIGRvbmUKCiAgICBpZiBbICEgLXggIiQod2hpY2ggYW1hem9uLXNzbS1hZ2VudCkiIF07IHRoZW4KICAgICAgZGllICJObyBTU00gYWdlbnQgd2FzIGluc3RhbGxlZCIKICAgIGZpCn0KCmZ1bmN0aW9uIG1haW4oKSB7CiAgICBNQVhfUkVUUllfQ09VTlQ9MwogICAgUkVUUllfQ09VTlQ9MAoKICAgIGNkIC90bXAKCiAgICB3aGlsZSBbICRSRVRSWV9DT1VOVCAtbHQgJE1BWF9SRVRSWV9DT1VOVCBdIDsgZG8KICAgICAgZ2V0X3NzbV9hZ2VudAogICAgICBpbnN0YWxsX3NzbV9hZ2VudAoKICAgICAgaWYgWyAhIC14ICIkKHdoaWNoIGFtYXpvbi1zc20tYWdlbnQpIiBdOyB0aGVuCiAgICAgICAgUkVUUllfQ09VTlQ9JCgoUkVUUllfQ09VTlQrMSkpOwogICAgICAgIGVjaG8gU1NNQWdlbnQgSW5zdGFsbGF0aW9uIGZhaWxlZCAkUkVUUllfQ09VTlQgdGltZXMsIHJldHJ5aW5nLi4uCiAgICAgICAgY29udGludWUKICAgICAgZWxzZQogICAgICAgIHN0YXJ0X3NzbV9hZ2VudAogICAgICAgIGV4aXQgMAogICAgICBmaQoKICAgIGRvbmUKfQoKbWFpbiAkQCAyPiYxIHwgdGVlIC90bXAvYXdzLWluc3RhbGwtc3NtLWFnZW50LmxvZwo=",
        "MinInstanceCount": 1,
        "MaxInstanceCount": 1,
        "IamInstanceProfileName": "{{IamInstanceProfileName}}",
        "SubnetId": "{{ SubnetId }}",
        "SecurityGroupIds": "{{SecurityGroupIds}}",
        "MetadataOptions": "{{MetadataOptions}}"
      }
    },
    {
      "name": "verifySSMInstall",
      "action": "aws:runCommand",
      "maxAttempts": 3,
      "timeoutSeconds": 1200,
      "onFailure": "Abort",
      "inputs": {
        "DocumentName": "AWS-RunShellScript",
        "InstanceIds": [
          "{{launchInstance.InstanceIds}}"
        ],
        "Parameters": {
          "commands": [
            "hostname"
          ]
        }
      }
    },
    {
      "name": "ConfigureWazuhAgent",
      "action": "aws:runCommand",
      "maxAttempts": 1,
      "timeoutSeconds": 3600,
      "onFailure": "Abort",
      "inputs": {
        "DocumentName": "AWS-RunShellScript",
        "InstanceIds": [
          "{{launchInstance.InstanceIds}}"
        ],
        "Parameters": {
          "commands": [
            "echo 'Step 1 : Checking Wazuh agent availability and remove if exist'",
            "rpm -qa | grep wazuh-agent && yum remove wazuh-agent -y",
            "echo 'Step 2 : Installing Wazuh Agent'",
            "WAZUH_MANAGER='02100888889.aws.serviceurl.internal' yum install https://packages.wazuh.com/4.x/yum/wazuh-agent-4.3.10-1.x86_64.rpm -y",
            "echo 'Step 3 : enabing remote cmd'",
            "echo 'sca.remote_commands=1' >> /var/ossec/etc/local_internal_options.conf",
            "echo 'Step 4 : daemon reload'",
            "sudo systemctl daemon-reload",
            "echo 'Step 5 : enabing service'",
            "sudo systemctl enable wazuh-agent"
          ]
        }
      }
    },
    {
      "name": "stopInstance",
      "action": "aws:changeInstanceState",
      "maxAttempts": 3,
      "timeoutSeconds": 1200,
      "onFailure": "Abort",
      "inputs": {
        "InstanceIds": [
          "{{launchInstance.InstanceIds}}"
        ],
        "DesiredState": "stopped"
      }
    },
    {
      "name": "createImage",
      "action": "aws:createImage",
      "maxAttempts": 3,
      "onFailure": "Abort",
      "inputs": {
        "InstanceId": "{{launchInstance.InstanceIds}}",
        "ImageName": "{{TargetAmiName}}",
        "NoReboot": true,
        "ImageDescription": "AMI Generated by EC2 Automation on {{global:DATE_TIME}} from {{SourceAmiId}}"
      }
    },
    {
      "name": "CreateTagsImage",
      "action": "aws:createTags",
      "maxAttempts": 1,
      "onFailure": "Continue",
      "inputs": {
        "ResourceType": "EC2",
        "ResourceIds": [
          "{{createImage.ImageId}}"
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": "{{AMINameTag}}"
          }
        ]
      }
    },
    {
      "name": "terminateInstance",
      "action": "aws:changeInstanceState",
      "maxAttempts": 3,
      "onFailure": "Continue",
      "inputs": {
        "InstanceIds": [
          "{{launchInstance.InstanceIds}}"
        ],
        "DesiredState": "terminated"
      }
    }
  ]
}
DOC
}

# SECURITY GROUP FOR SSM DOC FOR WAZUH AGENT AMI 
module "hbl_wazuh_agent_ami_ssm_doc_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-bom.git//security-group"
  name   = "${local.aws_account_name}-wazuh-agent-ami-ssm-doc-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_source_security_group_id = [
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "Allow all outgoing"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "wazuh-agent" },
    { "tier" = "app" }
  )
}