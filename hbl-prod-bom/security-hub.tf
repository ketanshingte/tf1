##ENABLING SECURITY HUB STANDARD IN HBL ACCOUNT
resource "aws_securityhub_standards_subscription" "cis_1_4" {
  standards_arn = "arn:aws:securityhub:ap-south-1::standards/cis-aws-foundations-benchmark/v/1.4.0"
}

resource "aws_securityhub_standards_subscription" "pci_321" {
  standards_arn = "arn:aws:securityhub:ap-south-1::standards/pci-dss/v/3.2.1"
}
