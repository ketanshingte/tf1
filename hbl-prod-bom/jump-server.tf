#LOCALS FOR HBL JUMP SERVER 
locals {
  ## DB
  ami_id_hbl_jump_server_db        = "ami-060dcc5a77c84080f"
  data_subnet_ids_jump_server = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
  data_subnet_az_jump_server  = ["${local.region}a", "${local.region}b", "${local.region}c"]
  
  ## APP
  ami_id_hbl_jump_server_app        = "ami-04fa96f19bd7939c5" ## BASE COMMON AMI 03
  app_subnet_ids_jump_server = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
  app_subnet_az_jump_server  = ["${local.region}a", "${local.region}b", "${local.region}c"]
}


################################################
############# HBL JUMP SERVER DB #############
################################################

#KEYPAIR HBL JUMP SERVER DB
module "hbl_jump_server_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-jump-server-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDasz6OPw8vEcIW33Sg0XHX8oDV7FskD/xPiBatCvg6SnbO2azdtjp3yfbdySXpa1F+nrDD/0zGDOg7ctQY1DlB2eEEKqyvQ0C9dv828TQ2LTpdzAhlBPBOJAEmCgTQOa3l+Fy1q1fp7R+/z3b5EC329qJzmEqOpDItbIvtKGwEe58/dUpCDxvDTIKvPFb6i66Fdmy0YoDu3eL7XamPPsg68Esywjz+D6KlsXk4KoiW7hjscafqFgo11UySKxusizhlcgxPc/qikGnysTf6BlB19VG7wzVGdzSO08bFEUHkU2u/8jXAk5Hze6qIrcul/iQjdh8+7eTjeulbHRYN0MVuP2uJ5X8/G6LoBQz35EAvES0vNiA1HBSicE9dzpAqXF9/shJVnRvk6ZhymhELr+FFmb8sqkSTsNEk53LYzA3rHGmQBFasqOcgHEHc9LtB7m8di1yWfnFuW3gpsunCUKNkewZV5Nl3s02JowDbrRonxttxDhFuE7rRfiac/dM8PSc="
  tags = merge(
    local.tags,
    { "application" = "jump-server-db" },
    { "tier" = "db" }
  )
}

# SECURITY GROUP HBL JUMP SERVER DB
module "hbl_jump_server_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-jump-server-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allow ssh port to sharedservices app subnet range for jenkins"
      cidr_blocks = "10.81.8.0/21"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    },
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for lentra temp dr mongo db sg"
      cidr_blocks = "10.10.13.17/32"
    },
  ]
  ingress_with_source_security_group_id = [
    {
      from_port   = 10085
      to_port     = 10085
      protocol    = "tcp"
      description = "Opening 10085 port for gng mongo tw db sg"
      source_security_group_id = "${module.hbl_gng_mongo_tw_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_pdt_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_pdt_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_txn_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_txn_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_tw_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_tw_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_od_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_od_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_od_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_image_db_sg_01.security_group_id}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allowing port 22 from hbl_gng_mongo_uam_db sg"
      source_security_group_id =  "${module.hbl_gng_mongo_uam_db_sg_01.security_group_id}"
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "jump-server-db" },
    { "tier" = "db" }
  )
}

####################################################################################
## IAM ROLE FOR HBL DB JUMP SERVER EC2 TO ACCESS S3 BUCKET - GET & PUT
####################################################################################

resource "aws_iam_role" "ec2_s3_jump_server_access_role" {
  name                = "${local.aws_account_name}-jump-server-ec2-access-role-01"
  path               = "/system/"
  assume_role_policy  = data.aws_iam_policy_document.instance_assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"]
  tags = merge(local.tags,
  {"application"="session-manager"}
  )
}

resource "aws_iam_instance_profile" "ec2_s3_jump_server_access_instance_profile" {
  name = "${local.aws_account_name}-jump-server-ec2-access-role-01"
  role = aws_iam_role.ec2_s3_jump_server_access_role.name
  tags = merge(local.tags,
  {"application"="session-manager"}
  )
}

## CREATING ECR READ POLICY FOR DOCKER SWARM
resource "aws_iam_role_policy" "ec2_s3_jump_server_access_role_ecr_read_policy" {
  name = "${local.aws_account_name}-ec2-s3-jump-server-role-ecr-read-policy-01"
  role = aws_iam_role.ec2_s3_jump_server_access_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:BatchGetImage",
                "ecr:ListTagsForResource",
                "ecr:DescribeImageScanFindings"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}

## CREATING CUSTOM SSM ASSUME ROLE POLICY FOR CROSS ACCOUNT ACCESS
resource "aws_iam_role_policy" "ec2_s3_jump_server_role_policy_01" {
  name = "${local.aws_account_name}-ec2_s3_jump_server_role_policy_01"
  role = aws_iam_role.ec2_s3_jump_server_access_role.id
 policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "*"
        }
    ]
}
EOF
}

## CREATING CUSTOM  ROLE POLICY FOR EC2 TO ACCESS S3 FOR  GET,PUT OBJECTS
resource "aws_iam_role_policy" "ec2_s3_jump_server_role_policy" {
  name = "${local.aws_account_name}-ec2-s3-jump-server-access-policy-01"
  role = aws_iam_role.ec2_s3_jump_server_access_role.id
 policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": [
          "kms:Decrypt",
          "kms:Encrypt",
          "kms:GenerateDataKey"
      ],
      "Resource": [
          "${data.terraform_remote_state.audit_tf_state.outputs.hbl_s3_kms_arn}"
      ]
    },
    {
      "Sid": "VisualEditor1",
      "Effect": "Allow",
      "Action": [
          "s3:PutObject*",
		      "s3:AbortMultipartUpload",
          "s3:GetBucketLocation"
      ],
      "Resource": [
          "arn:aws:s3:::${module.hbl_data_insights_src_s3_01.s3_bucket_id}/",
          "arn:aws:s3:::${module.hbl_data_insights_src_s3_01.s3_bucket_id}/*"
      ]
    },
      {
      "Sid": "VisualEditor2",
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": [
        "arn:aws:s3:::${module.hbl_data_insights_src_s3_01.s3_bucket_id}"
      ]
    }
  ]
}
EOF
}

# EC2 HBL JUMP SERVER DB
module "hbl_jump_server_db_ec2_01" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  count                   = "1"
  name                    = "${local.aws_account_name}-jump-server-db-ec2-${format("%02d", count.index + 1)}"
  ami                     = local.ami_id_hbl_jump_server_db 
  instance_type           = "m6a.large" 
  subnet_id               = element(local.data_subnet_ids_jump_server, 0)
  vpc_security_group_ids  = [module.hbl_jump_server_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_s3_jump_server_access_instance_profile.name
  key_name                = module.hbl_jump_server_db_kp_01.key_pair_key_name
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  user_data_base64        = base64encode(var.teleport_userdata["userdata"])
  tags = merge(
    local.tags,
    { "application" = "jump-server-db" },
    { "tier" = "db" },
    { "teleport" = "hbl-jump-server-db"}
  )
}

# EBS VOLUME FOR THE JUMPHOST
module "hbl_jump_server_db_ec2_01_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_jump_server_db_ec2_01]

    availability_zone = element(local.data_subnet_az_jump_server, 0)
    size              = 500
    device_name = "/dev/sdf"
    instance_id = module.hbl_jump_server_db_ec2_01[0].ec2_complete_id
    tags = merge({Name : "${local.aws_account_name}-jump-server-db-ec2-01-ebs-01"},
    { "application" = "mongodb" },
    { "tier" = "db" },
    { "service" = "gng"},
    { "teleport" = "hbl-jump-server-db"},
    local.tags)  
}

################################################
#### HBL JUMP SERVER APP
################################################

#KEYPAIR HBL JUMP SERVER APP
module "hbl_jump_server_app_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-jump-server-app-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC59EylgHCgR1tA11YVC5uCVoKIN+E6LKRo1+5S8BvuzHUcMMJCSaoaVIuUu6gWbXK24EgHmT6wAhwdQva4R6cxGDl4sfPJVE/LvuGUqCp1uV9mG0r+2jv5LDTH7TgvDC5b+TmWBjm2VldQtpXDg5kgWV+cE6UYPcyTx6rRWP3OKKX9HNkFDF3R3/b9P3URi25P5pzxd3rZatidr5pVUmHW4vchDTpB1eXgdBQJgiCokObJIO2QOIgT6W5kWXlMP4zNMOzf/9ojk3GtR/LyYUT57rZyL7pilHtIvQijIJ3APBDxIYpJgeyV3r1ZV5H4f/pb8cgHNtvWAw31WkWtXKviC6aO7p7iLAJwgUAij3NwPH73KYy6mmboxckMzDNH7wdmyo2RKrQ6uQc/bzrStqRAFKTbjHpN9t7af71Qiro3Mc6/iNYJDC3XooRyRAsz1bOliI5i3uUMP0jGOAtJwuXr1uAshcyvhPXCCWa+H8OTTKuMS96WrMBnmTXiq1YQ1Pk="
  tags = merge(
    local.tags,
    { "application" = "jump-server-app" },
    { "tier" = "app" }
  )
}

# SECURITY GROUP HBL JUMP SERVER APP
module "hbl_jump_server_app_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-jump-server-app-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  ingress_with_source_security_group_id = [
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "application" = "jump-server-app" },
    { "tier" = "app" }
  )
}

# EC2 HBL JUMP SERVER APP
module "hbl_jump_server_app_ec2_01" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  count                   = "4"
  name                    = "${local.aws_account_name}-jump-server-app-ec2-${format("%02d", count.index + 1)}"
  ami                     = local.ami_id_hbl_jump_server_app 
  instance_type           = "t3a.small" 
  subnet_id               = element(local.app_subnet_ids_jump_server, 0)
  vpc_security_group_ids  = [module.hbl_jump_server_app_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  key_name                = module.hbl_jump_server_app_kp_01.key_pair_key_name
  root_volume_size = 30
  enable_volume_tags      = true
  disable_api_termination = true
  user_data_base64        = base64encode(var.teleport_userdata["userdata"])
  tags = merge(
    local.tags,
    { "application" = "jump-server-app" },
    { "tier" = "app" },
    { "teleport" = "hbl-jump-server-app"}
  )
}