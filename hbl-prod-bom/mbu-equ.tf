# #LOCALS FOR MBU EQU APP
# locals {
#   ami_id_mbu_equ_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_equ_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU EQU APP CLUSTER #############
# ################################################

# #KEYPAIR MBU EQU CLUSTER
# module "hbl_mbu_equ_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-equ-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGAOZ9jdflrI1iF8uTgGxp245uAWaEsHU8yu8g6dZdO0TAHVHZqtQV5Nw8HO9WvPXMG9Jx7S4w/CEZJXWqM2JU0r3bPfeAGx7Soo5tf0pSQ6+aByPezGyrV3AhToh67kW8zTWrg4UG7BX2ZIpAUxwkjUcfHuIKFfwzlz4lE5JSX+FcWKVQBKKnu79rR0Sq6YJe2dC/mEjIlOrksOBBSBGxuIX3es2ishAM+FstwdE+Y1E25D1DymT8XSQEG8s4pgq8ATiYL9tJZxILl9Y8xzr8bJT+clYh1pHjSEqEAqCJXoqBuBSShujNASL5Fwz0RJ8428TmALmHanj+qaiQrhKODvTAp2UfJWj+WCYWdtvLED0GhvTdvatndPkH5ql1hEYRZRMtEPLfbWTCuNO4p0H8cZ7wg+sKoBL3AMnOG4eOYPvgkpMNiSx4Wrfz8kMxbAFYWfvyuXq1m73aj7pCoShuYo5OaXLVuvw0rM2G3XWPP3dlfum8xm0VuJAsag4yPzk="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-equ-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU EQU APP CLUSTER
# module "hbl_mbu_equ_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "1"
#   name                    = "${local.aws_account_name}-mbu-equ-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_equ_app
#   instance_type           = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_equ_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_equ_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_equ_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-equ-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-equ-app"}
#   )
# }

# # SECURITY GROUP MBU EQU APP CLUSTER
# module "hbl_mbu_equ_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-equ-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-equ-app" },
#     { "tier" = "app" }
#   )
# }