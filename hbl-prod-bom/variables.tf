#VARIABLES DEFINED FOR THE CREATION OF DATA SUBNET
variable "data_subnets" {
  description = "The cidrs to be used for the data subnets"
  type        = list(string)
  default     = []
}

variable "data_inbound_acl_rules" {
  description = "Inbound Network ACLs used for the data subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "data_outbound_acl_rules" {
  description = "Outbound Network ACLs used for the data subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

#VARIABLES DEFINED FOR THE CREATION OF APP SUBNET
variable "app_subnets" {
  description = "The cidrs to be used for the app subnets"
  type        = list(string)
  default     = []
}

variable "app_inbound_acl_rules" {
  description = "Inbound Network ACLs used for the app subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "app_outbound_acl_rules" {
  description = "Outbound Network ACLs used for the app subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

#VARIABLES DEFINED FOR THE CREATION OF WEB SUBNET
variable "web_subnets" {
  description = "The cidrs to be used for the Web subnets"
  type        = list(string)
  default     = []
}

variable "web_inbound_acl_rules" {
  description = "Inbound Network ACLs used for the Web subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "web_outbound_acl_rules" {
  description = "Outbound Network ACLs used for the Web subnets"
  type        = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

#VARIABLES REQUIRED FOR CREATION OF ADDITIONAL SUBNETS 
# variable "transit_gateway_id" {
#   description = "The ID of the transit gateway created in the Network account"
#   type        = string
#   default     = ""
# }

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
}

variable "teleport_userdata" {
  description = "Teleport Userdata"
  type        = map

  default = {
  userdata = <<-EOT
  #!/bin/bash
  pkill -f teleport
  rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh
  rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt
  rm -rf /opt/teleport-location
  rm -rf /etc/systemd/system/teleport.service
  ##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########
  ROLE_ARN=arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01
  OUTPUT_PROFILE=cross-account-shared-services-access
  aws configure set region ap-south-1 --profile $OUTPUT_PROFILE
  aws configure set output text --profile $OUTPUT_PROFILEfi
  echo "Assuming role $ROLE_ARN and create temporary credentials"
  sts=$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$OUTPUT_PROFILE" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)
  sts=($sts)
  aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE
  aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE
  aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE
  echo "credentials stored in the profile named $OUTPUT_PROFILE"
  #environment variable for the location path of auth join tokens in the SSM parameter store
  export TOKEN="/teleport/sharedservices-teleport/tokens/node"
  export CAPIN="/teleport/sharedservices-teleport/ca-pin-hash"
  #environment variable of the auth server ip address
  export AUTH_SERVER_URL="02949425121.serviceurl.in"
  #To execute and store the installation files with date
  exec > /var/log/teleport-installation-files-$(date +"%Y-%m-%d").log 2>&1
  echo 'Creating install directories...'
  mkdir -p /opt/teleport-location
  mkdir -p /var/lib/teleport/auth-pin
  TELEPORTDIR=/opt/teleport-location
  #download the teleport release
  yum install perl-Digest-SHA -y
  curl https://get.gravitational.com/teleport-v11.2.0-linux-amd64-bin.tar.gz.sha256
  wget -c https://get.gravitational.com/teleport-v11.2.0-linux-amd64-bin.tar.gz -O - | tar -xz -C $TELEPORTDIR
  wget -c https://get.gravitational.com/teleport-v11.2.0-linux-amd64-bin.tar.gz
  ##############To compare checksums##########################
  if diff -iq <(curl "https://get.gravitational.com/teleport-v11.2.0-linux-amd64-bin.tar.gz.sha256")  <(shasum -a 256 "teleport-v11.2.0-linux-amd64-bin.tar.gz") > /dev/null ; then
      echo "checksum matches"
  else
      echo "checksum differ"
  fi
  rm teleport-v11.2.0-linux-amd64-bin.tar.gz
  echo "Installing teleport..."
  #install the teleport
  $TELEPORTDIR/teleport/install
  ##############To configure teleport##########################
  $TELEPORTDIR/teleport/install
  cat > /etc/systemd/system/teleport.service <<- "EOF"
  [Unit]
  Description=Teleport SSH Service
  After=network.target
  [Service]
  Type=simple
  Restart=on-failure
  EnvironmentFile=-/etc/default/teleport
  ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid
  ExecReload=/bin/kill -HUP $MAINPID
  PIDFile=/run/teleport.pid
  LimitNOFILE=8192
  KillMode=process
  [Install]
  WantedBy=multi-user.target
  EOF
  touch /var/lib/teleport/token
  touch /var/lib/teleport/ca-pin
  #fetch the join token from the ssm parameter store
  aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption
  aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin
  #Write Teleport configuration file.
  cat >/etc/teleport.yaml <<EOF
  teleport:
      data_dir: /var/lib/teleport
      auth_token: /var/lib/teleport/token
      ca_pin: /var/lib/teleport/ca-pin
      auth_servers:
              -  $AUTH_SERVER_URL:443
      connection_limits:
          max_connections: 1000
          max_users: 250
      log:
          output: stderr
          severity: info
  auth_service:
      enabled: false
  ssh_service:
      enabled: true
      enhanced_recording:
        # Enable or disable enhanced auditing for this node. Default value: false.
        enabled: true
        command_buffer_size: 8
        # Optional: disk_buffer_size is optional with default value of 128 pages.
        disk_buffer_size: 128
        # Optional: network_buffer_size is optional with default value of 8 pages.
        network_buffer_size: 8
        # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:
        # /cgroup2.
        cgroup_path: /cgroup2
  proxy_service:
      enabled: false
  EOF
  file="/etc/teleport.yaml"
  if [ ! -f "$file" ]
  then
      echo "$0: teleport configuration file '$${file}' not found."
  else 
  echo "teleport configuration file is being created" >>/var/log/teleport-installation-files-$(date +"%Y-%m-%d").log
  fi
  systemctl daemon-reload
  systemctl enable teleport
  systemctl start teleport
  systemctl status teleport
  #############To check if the teleport service is running###############
  STATUS="$(systemctl is-active teleport.service)"
  if [ "$${STATUS}" = "active" ]; then
      echo "teleport service is running"
  else
      echo "Service not running.."
      exit 1
  fi
  EOT
  userdata_13_0_3 = <<-EOT
  #!/bin/bash
  pkill -f teleport
  rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh
  rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt
  rm -rf /opt/teleport-location
  rm -rf /etc/systemd/system/teleport.service
  ##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########
  ROLE_ARN=arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01
  OUTPUT_PROFILE=cross-account-shared-services-access
  aws configure set region ap-south-1 --profile $OUTPUT_PROFILE
  aws configure set output text --profile $OUTPUT_PROFILE
  echo "Assuming role $ROLE_ARN and create temporary credentials"
  sts=$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$OUTPUT_PROFILE" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)
  sts=($sts)
  aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE
  aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE
  aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE
  echo "credentials stored in the profile named $OUTPUT_PROFILE"
  #environment variable for the location path of auth join tokens in the SSM parameter store
  export TOKEN="/teleport/sharedservices-teleport/tokens/node"
  export CAPIN="/teleport/sharedservices-teleport/ca-pin-hash"
  #environment variable for the latest version of teleport
  export TeleportVersion="teleport-v13.0.3-linux-amd64-bin.tar.gz"
  #environment variable of the auth server ip address
  export AUTH_SERVER_URL="02949425121.serviceurl.in"
  #To execute and store the installation files with date
  exec > /var/log/teleport-installation-files-$(date +"%Y-%m-%d").log 2>&1
  echo 'Creating install directories...'
  mkdir -p /opt/teleport-location
  mkdir -p /var/lib/teleport/auth-pin
  TELEPORTDIR=/opt/teleport-location
  #download the teleport release
  yum install perl-Digest-SHA -y
  curl https://get.gravitational.com/$TeleportVersion.sha256
  wget -c https://get.gravitational.com/$TeleportVersion -O - | tar -xz -C $TELEPORTDIR
  wget -c https://get.gravitational.com/$TeleportVersion
  ##############To compare checksums##########################
  if diff -iq <(curl "https://get.gravitational.com/$TeleportVersion.sha256")  <(shasum -a 256 "$TeleportVersion") > /dev/null ; then
      echo "checksum matches"
  else
      echo "checksum differ"
  fi
  rm $TeleportVersion
  echo "Installing teleport..."
  #install the teleport
  $TELEPORTDIR/teleport/install
  ##############To configure teleport##########################
  $TELEPORTDIR/teleport/install
  cat > /etc/systemd/system/teleport.service <<- "EOF"
  [Unit]
  Description=Teleport SSH Service
  After=network.target
  [Service]
  Type=simple
  Restart=on-failure
  EnvironmentFile=-/etc/default/teleport
  ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid
  ExecReload=/bin/kill -HUP $MAINPID
  PIDFile=/run/teleport.pid
  LimitNOFILE=8192
  KillMode=process
  [Install]
  WantedBy=multi-user.target
  EOF
  touch /var/lib/teleport/token
  touch /var/lib/teleport/ca-pin
  #fetch the join token from the ssm parameter store
  aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption
  aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin
  #Write Teleport configuration file.
  cat >/etc/teleport.yaml <<EOF
  teleport:
      data_dir: /var/lib/teleport
      auth_token: /var/lib/teleport/token
      ca_pin: /var/lib/teleport/ca-pin
      auth_servers:
              -  $AUTH_SERVER_URL:443
      connection_limits:
          max_connections: 1000
          max_users: 250
      log:
          output: stderr
          severity: info
  auth_service:
      enabled: false
  ssh_service:
      enabled: true
      enhanced_recording:
        # Enable or disable enhanced auditing for this node. Default value: false.
        enabled: true
        command_buffer_size: 8
        # Optional: disk_buffer_size is optional with default value of 128 pages.
        disk_buffer_size: 128
        # Optional: network_buffer_size is optional with default value of 8 pages.
        network_buffer_size: 8
        # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:
        # /cgroup2.
        cgroup_path: /cgroup2
  proxy_service:
      enabled: false
  EOF
  file="/etc/teleport.yaml"
  if [ ! -f "$file" ]
  then
      echo "$0: teleport configuration file '$${file}' not found."
  else 
  echo "teleport configuration file is being created" >>/var/log/teleport-installation-files-$(date +"%Y-%m-%d").log
  fi
  systemctl daemon-reload
  systemctl enable teleport
  systemctl start teleport
  systemctl status teleport
  #############To check if the teleport service is running###############
  STATUS="$(systemctl is-active teleport.service)"
  if [ "$${STATUS}" = "active" ]; then
      echo "teleport service is running"
  else
      echo "Service not running.."
      exit 1
  fi
  EOT

  userdata_14_2_0 = <<-EOT
  #!/bin/bash
  pkill -f teleport
  rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh
  rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt
  rm -rf /opt/teleport-location
  rm -rf /etc/systemd/system/teleport.service
  ##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########
  ROLE_ARN=arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01
  OUTPUT_PROFILE=cross-account-shared-services-access
  aws configure set region ap-south-1 --profile $OUTPUT_PROFILE
  aws configure set output text --profile $OUTPUT_PROFILE
  echo "Assuming role $ROLE_ARN and create temporary credentials"
  sts=$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$OUTPUT_PROFILE" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)
  sts=($sts)
  aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE
  aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE
  aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE
  echo "credentials stored in the profile named $OUTPUT_PROFILE"
  #environment variable for the location path of auth join tokens in the SSM parameter store
  export TOKEN="/teleport/sharedservices-teleport/tokens/node"
  export CAPIN="/teleport/sharedservices-teleport/ca-pin-hash"
  #environment variable for the latest version of teleport
  export TeleportVersion="teleport-v14.2.0-linux-amd64-bin.tar.gz"
  #environment variable of the auth server ip address
  export AUTH_SERVER_URL="02949425121.serviceurl.in"
  #To execute and store the installation files with date
  exec > /var/log/teleport-installation-files-$(date +"%Y-%m-%d").log 2>&1
  echo 'Creating install directories...'
  mkdir -p /opt/teleport-location
  mkdir -p /var/lib/teleport/auth-pin
  TELEPORTDIR=/opt/teleport-location
  #download the teleport release
  yum install perl-Digest-SHA -y
  curl https://get.gravitational.com/$TeleportVersion.sha256
  wget -c https://get.gravitational.com/$TeleportVersion -O - | tar -xz -C $TELEPORTDIR
  wget -c https://get.gravitational.com/$TeleportVersion
  ##############To compare checksums##########################
  if diff -iq <(curl "https://get.gravitational.com/$TeleportVersion.sha256")  <(shasum -a 256 "$TeleportVersion") > /dev/null ; then
      echo "checksum matches"
  else
      echo "checksum differ"
  fi
  rm $TeleportVersion
  echo "Installing teleport..."
  #install the teleport
  $TELEPORTDIR/teleport/install
  ##############To configure teleport##########################
  $TELEPORTDIR/teleport/install
  cat > /etc/systemd/system/teleport.service <<- "EOF"
  [Unit]
  Description=Teleport SSH Service
  After=network.target
  [Service]
  Type=simple
  Restart=on-failure
  EnvironmentFile=-/etc/default/teleport
  ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid
  ExecReload=/bin/kill -HUP $MAINPID
  PIDFile=/run/teleport.pid
  LimitNOFILE=8192
  KillMode=process
  [Install]
  WantedBy=multi-user.target
  EOF
  touch /var/lib/teleport/token
  touch /var/lib/teleport/ca-pin
  #fetch the join token from the ssm parameter store
  aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption
  aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin
  #Write Teleport configuration file.
  cat >/etc/teleport.yaml <<EOF
  teleport:
      data_dir: /var/lib/teleport
      auth_token: /var/lib/teleport/token
      ca_pin: /var/lib/teleport/ca-pin
      auth_servers:
              -  $AUTH_SERVER_URL:443
      connection_limits:
          max_connections: 1000
          max_users: 250
      log:
          output: stderr
          severity: info
  auth_service:
      enabled: false
  ssh_service:
      enabled: true
      enhanced_recording:
        # Enable or disable enhanced auditing for this node. Default value: false.
        enabled: true
        command_buffer_size: 8
        # Optional: disk_buffer_size is optional with default value of 128 pages.
        disk_buffer_size: 128
        # Optional: network_buffer_size is optional with default value of 8 pages.
        network_buffer_size: 8
        # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:
        # /cgroup2.
        cgroup_path: /cgroup2
  proxy_service:
      enabled: false
  EOF
  file="/etc/teleport.yaml"
  if [ ! -f "$file" ]
  then
      echo "$0: teleport configuration file '$${file}' not found."
  else 
  echo "teleport configuration file is being created" >>/var/log/teleport-installation-files-$(date +"%Y-%m-%d").log
  fi
  systemctl daemon-reload
  systemctl enable teleport
  systemctl start teleport
  systemctl status teleport
  #############To check if the teleport service is running###############
  STATUS="$(systemctl is-active teleport.service)"
  if [ "$${STATUS}" = "active" ]; then
      echo "teleport service is running"
  else
      echo "Service not running.."
      exit 1
  fi
  EOT
}
}

variable "teleport_userdata_database" {
  description = "Teleport Userdata"
  type        = map

  default = {
  userdata = <<-EOT
  #!/bin/bash
  pkill -f teleport
  rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh
  rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt
  rm -rf /opt/teleport-location
  rm -rf /etc/systemd/system/teleport.service
  ##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########
  ROLE_ARN=arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01
  OUTPUT_PROFILE=cross-account-shared-services-access
  aws configure set region ap-south-1 --profile $OUTPUT_PROFILE
  aws configure set output text --profile $OUTPUT_PROFILEfi
  echo "Assuming role $ROLE_ARN and create temporary credentials"
  sts=$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$OUTPUT_PROFILE" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)
  sts=($sts)
  aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE
  aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE
  aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE
  echo "credentials stored in the profile named $OUTPUT_PROFILE"
  #environment variable for the location path of auth join tokens in the SSM parameter store
  export TOKEN="/teleport/sharedservices-teleport/tokens/node"
  export CAPIN="/teleport/sharedservices-teleport/ca-pin-hash"
  #environment variable of the auth server ip address
  export AUTH_SERVER_URL="02949425121.serviceurl.in"
  #To execute and store the installation files with date
  exec > /var/log/teleport-installation-files-$(date +"%Y-%m-%d").log 2>&1
  echo 'Creating install directories...'
  mkdir -p /opt/teleport-location
  mkdir -p /var/lib/teleport/auth-pin
  TELEPORTDIR=/opt/teleport-location
  #download the teleport release
  yum install perl-Digest-SHA -y
  curl https://get.gravitational.com/teleport-v11.3.3-linux-amd64-bin.tar.gz.sha256
  wget -c https://get.gravitational.com/teleport-v11.3.3-linux-amd64-bin.tar.gz -O - | tar -xz -C $TELEPORTDIR
  wget -c https://get.gravitational.com/teleport-v11.3.3-linux-amd64-bin.tar.gz
  ##############To compare checksums##########################
  if diff -iq <(curl "https://get.gravitational.com/teleport-v11.3.3-linux-amd64-bin.tar.gz.sha256")  <(shasum -a 256 "teleport-v11.3.3-linux-amd64-bin.tar.gz") > /dev/null ; then
      echo "checksum matches"
  else
      echo "checksum differ"
  fi
  rm teleport-v11.3.3-linux-amd64-bin.tar.gz
  echo "Installing teleport..."
  #install the teleport
  $TELEPORTDIR/teleport/install
  ##############To configure teleport##########################
  $TELEPORTDIR/teleport/install
  cat > /etc/systemd/system/teleport.service <<- "EOF"
  [Unit]
  Description=Teleport SSH Service
  After=network.target
  [Service]
  Type=simple
  Restart=on-failure
  EnvironmentFile=-/etc/default/teleport
  ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid
  ExecReload=/bin/kill -HUP $MAINPID
  PIDFile=/run/teleport.pid
  LimitNOFILE=8192
  KillMode=process
  [Install]
  WantedBy=multi-user.target
  EOF
  touch /var/lib/teleport/token
  touch /var/lib/teleport/ca-pin
  #fetch the join token from the ssm parameter store
  aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption
  aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin
  #Write Teleport configuration file.
  cat >/etc/teleport.yaml <<EOF
  teleport:
      data_dir: /var/lib/teleport
      auth_token: /var/lib/teleport/token
      ca_pin: /var/lib/teleport/ca-pin
      auth_servers:
              -  $AUTH_SERVER_URL:443
      connection_limits:
          max_connections: 1000
          max_users: 250
      log:
          output: stderr
          severity: info
  auth_service:
      enabled: false
  ssh_service:
      enabled: true
      enhanced_recording:
        # Enable or disable enhanced auditing for this node. Default value: false.
        enabled: true
        command_buffer_size: 8
        # Optional: disk_buffer_size is optional with default value of 128 pages.
        disk_buffer_size: 128
        # Optional: network_buffer_size is optional with default value of 8 pages.
        network_buffer_size: 8
        # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:
        # /cgroup2.
        cgroup_path: /cgroup2
  proxy_service:
      enabled: false
  EOF
  file="/etc/teleport.yaml"
  if [ ! -f "$file" ]
  then
      echo "$0: teleport configuration file '$${file}' not found."
  else 
  echo "teleport configuration file is being created" >>/var/log/teleport-installation-files-$(date +"%Y-%m-%d").log
  fi
  systemctl daemon-reload
  systemctl enable teleport
  systemctl start teleport
  systemctl status teleport
  #############To check if the teleport service is running###############
  STATUS="$(systemctl is-active teleport.service)"
  if [ "$${STATUS}" = "active" ]; then
      echo "teleport service is running"
  else
      echo "Service not running.."
      exit 1
  fi
  EOT
}
}

variable "teleport_userdata_asg" {
  description = "Teleport Userdata specific for autoscaling instances"
  type        = map

  default = {
  userdata = <<-EOT
  #!/bin/bash
  pkill -f teleport
  rm -rf /usr/local/bin/teleport /usr/local/bin/tctl /usr/local/bin/tsh
  rm -rf /etc/teleport.yaml /var/lib/teleport/* /etc/teleport.d/role.all /etc/teleport.d/use-letsencrypt
  rm -rf /opt/teleport-location
  rm -rf /etc/systemd/system/teleport.service
  ##############Assumes the role of the target account and uses Simple Token Service (sts) to create temporary AWS credentials.Then it automatically creates an AWS profile that will be storedin the AWS config ###########
  ROLE_ARN=arn:aws:iam::105828129156:role/system/sharedservices-teleport-ssm-cross-account-access-role-01
  OUTPUT_PROFILE=cross-account-shared-services-access
  aws configure set region ap-south-1 --profile $OUTPUT_PROFILE
  aws configure set output text --profile $OUTPUT_PROFILE
  echo "Assuming role $ROLE_ARN and create temporary credentials"
  sts=$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$OUTPUT_PROFILE" --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text)
  sts=($sts)
  aws configure set aws_access_key_id $${sts[0]} --profile $OUTPUT_PROFILE
  aws configure set aws_secret_access_key $${sts[1]} --profile $OUTPUT_PROFILE
  aws configure set aws_session_token $${sts[2]} --profile $OUTPUT_PROFILE
  echo "credentials stored in the profile named $OUTPUT_PROFILE"
  #environment variable for the location path of auth join tokens in the SSM parameter store
  export TOKEN="/teleport/sharedservices-teleport/tokens/node"
  export CAPIN="/teleport/sharedservices-teleport/ca-pin-hash"
  #environment variable for the latest version of teleport
  export TeleportVersion="teleport-v14.2.0-linux-amd64-bin.tar.gz"
  #environment variable of the auth server ip address
  export AUTH_SERVER_URL="02949425121.serviceurl.in"
  #To execute and store the installation files with date
  exec > /var/log/teleport-installation-files-$(date +"%Y-%m-%d").log 2>&1
  echo 'Creating install directories...'
  mkdir -p /opt/teleport-location
  mkdir -p /var/lib/teleport/auth-pin
  TELEPORTDIR=/opt/teleport-location
  #download the teleport release
  yum install perl-Digest-SHA -y
  curl https://get.gravitational.com/$TeleportVersion.sha256
  wget -c https://get.gravitational.com/$TeleportVersion -O - | tar -xz -C $TELEPORTDIR
  wget -c https://get.gravitational.com/$TeleportVersion
  ##############To compare checksums##########################
  if diff -iq <(curl "https://get.gravitational.com/$TeleportVersion.sha256")  <(shasum -a 256 "$TeleportVersion") > /dev/null ; then
      echo "checksum matches"
  else
      echo "checksum differ"
  fi
  rm $TeleportVersion
  echo "Installing teleport..."
  #install the teleport
  $TELEPORTDIR/teleport/install
  ##############To configure teleport##########################
  $TELEPORTDIR/teleport/install
  cat > /etc/systemd/system/teleport.service <<- "EOF"
  [Unit]
  Description=Teleport SSH Service
  After=network.target
  [Service]
  Type=simple
  Restart=on-failure
  EnvironmentFile=-/etc/default/teleport
  ExecStart=/usr/local/bin/teleport start --pid-file=/run/teleport.pid
  ExecReload=/bin/kill -HUP $MAINPID
  PIDFile=/run/teleport.pid
  LimitNOFILE=8192
  KillMode=process
  [Install]
  WantedBy=multi-user.target
  EOF
  touch /var/lib/teleport/token
  touch /var/lib/teleport/ca-pin
  #fetch the join token from the ssm parameter store
  aws ssm get-parameter --name $TOKEN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/token --with-decryption
  aws ssm get-parameter --name $CAPIN --region=ap-south-1 --query=Parameter.Value --profile $OUTPUT_PROFILE --output text > /var/lib/teleport/ca-pin
  #Write Teleport configuration file.
  cat >/etc/teleport.yaml <<EOF
  teleport:
      data_dir: /var/lib/teleport
      auth_token: /var/lib/teleport/token
      ca_pin: /var/lib/teleport/ca-pin
      auth_servers:
              -  $AUTH_SERVER_URL:443
      connection_limits:
          max_connections: 1000
          max_users: 250
      log:
          output: stderr
          severity: info
  auth_service:
      enabled: false
  ssh_service:
      enabled: true
      enhanced_recording:
        # Enable or disable enhanced auditing for this node. Default value: false.
        enabled: true
        command_buffer_size: 8
        # Optional: disk_buffer_size is optional with default value of 128 pages.
        disk_buffer_size: 128
        # Optional: network_buffer_size is optional with default value of 8 pages.
        network_buffer_size: 8
        # Optional: Controls where cgroupv2 hierarchy is mounted. Default value:
        # /cgroup2.
        cgroup_path: /cgroup2
  proxy_service:
      enabled: false
  EOF
  file="/etc/teleport.yaml"
  if [ ! -f "$file" ]
  then
      echo "$0: teleport configuration file '$${file}' not found."
  else 
  echo "teleport configuration file is being created" >>/var/log/teleport-installation-files-$(date +"%Y-%m-%d").log
  fi
  systemctl daemon-reload
  systemctl enable teleport
  systemctl start teleport
  systemctl status teleport
  #############To check if the teleport service is running###############
  STATUS="$(systemctl is-active teleport.service)"
  if [ "$${STATUS}" = "active" ]; then
      echo "teleport service is running"
  else
      echo "Service not running.."
      exit 1
  fi
  EOT
}
}