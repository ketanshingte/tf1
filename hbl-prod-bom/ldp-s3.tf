locals {
  #LOCAL FOR HBL
  hbl_data_insights_replication_destination_bucket_name = "${data.terraform_remote_state.ldtprod_tf_state.outputs.ldp_data_insights_hdfc_dest_s3_01}"
  hbl_data_insights_replication_destination_bucket_account_id = "${data.terraform_remote_state.ldtprod_tf_state.outputs.ldt_aws_account_id}"
  hbl_data_insights_replication_destination_bucket_kms_key_arn = "${data.terraform_remote_state.audit_tf_state.outputs.ldt_s3_kms_arn}"
}

############################################################################################################################
#################################### CREATING S3 BUCKET FOR HBL DATA INSIGHTS #####################################
############################################################################################################################

module "hbl_data_insights_src_s3_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules.git//s3"

  bucket = "${local.aws_account_name}-data-insights-src-s3-01"
  acl = "private"
  restrict_public_buckets = true
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true

  versioning = {
    enabled = true
  }

  #Server access logging
  logging = {
    target_bucket = module.hbl_server_access_logging_s3_01.s3_bucket_id
    target_prefix = "${local.aws_account_name}-data-insights-src-s3-01/"
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_s3_kms_arn}"
        sse_algorithm     = "aws:kms"
         }
    }
  }
  lifecycle_rule = [
    {
      id      = "hdfc-delete-after-3-days"
      enabled = true
      filter = {
        prefix = "hdfc"
      }
      expiration = {
        days = 3
        expired_object_delete_marker = false
      }
      noncurrent_version_expiration = {
        days = 3
        newer_noncurrent_versions = 3
      }

    }
  ]
  replication_configuration = {
    role = aws_iam_role.hbl_data_insights_src_s3_replication_role_01.arn

    rules = [
      {
        id       = "hdfc"
        status   = true
        priority = 10

        delete_marker_replication = false

        source_selection_criteria = {
          sse_kms_encrypted_objects = {
            enabled = true
          }
        }

        filter = {
          prefix = "hdfc"
        }

        destination = {
          bucket        = "arn:aws:s3:::${local.hbl_data_insights_replication_destination_bucket_name}"
          replica_kms_key_id = local.hbl_data_insights_replication_destination_bucket_kms_key_arn
          account_id         = local.hbl_data_insights_replication_destination_bucket_account_id

          access_control_translation = {
            owner = "Destination"
          }

          replication_time = {
            status  = "Enabled"
            minutes = 15
          }

          metrics = {
            status  = "Enabled"
            minutes = 15
          }
        }
      }
    ]
  tags = local.tags
  }
  tags = local.tags
}

resource "aws_s3_bucket_policy" "hbl_data_insights_src_s3_policy_01" {
  bucket       = module.hbl_data_insights_src_s3_01.s3_bucket_id
  policy       = data.aws_iam_policy_document.hbl_data_insights_src_s3_policy_01.json
}

data "aws_iam_policy_document" "hbl_data_insights_src_s3_policy_01" {
  statement {
    sid = "AllowSSLRequestsOnly"

    principals {
      type = "*"
      identifiers = ["*"]
    }
    
    effect = "Deny"

    actions = ["s3:*"]

    resources = [
      "${module.hbl_data_insights_src_s3_01.s3_bucket_arn}",
      "${module.hbl_data_insights_src_s3_01.s3_bucket_arn}/*"
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = [
        "false"
        ]
    }
  }
}

## REPLICATION IAM ROLE FOR HBL DATA INSIGHTS
resource "aws_iam_role" "hbl_data_insights_src_s3_replication_role_01" {
  name = "${local.aws_account_name}-data-insights-src-s3-replication-role-01"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "hbl_data_insights_src_s3_replication_policy_01" {
  name = "${local.aws_account_name}-data-insights-src-s3-replication-policy-01"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${module.hbl_data_insights_src_s3_01.s3_bucket_id}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl",
        "s3:GetObjectVersionForReplication",
        "s3:GetObjectVersionTagging"

      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${module.hbl_data_insights_src_s3_01.s3_bucket_id}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete",
        "s3:ReplicateTags",
        "s3:ObjectOwnerOverrideToBucketOwner"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${local.hbl_data_insights_replication_destination_bucket_name}/*"
    },
    {
        "Action": [
            "kms:Decrypt",
            "kms:GenerateDataKey"
        ],
        "Effect": "Allow",
        "Condition": {
            "StringLike": {
                "kms:ViaService": "s3.ap-south-1.amazonaws.com"
            }
        },
        "Resource": [
            "${data.terraform_remote_state.audit_tf_state.outputs.hbl_s3_kms_arn}"
        ]
    },
    {
        "Action": [
            "kms:Encrypt",
            "kms:GenerateDataKey"
        ],
        "Effect": "Allow",
        "Condition": {
            "StringLike": {
                "kms:ViaService": "s3.ap-south-1.amazonaws.com"
            }
        },
        "Resource": [
            "${local.hbl_data_insights_replication_destination_bucket_kms_key_arn}"
        ]
    }
  ]
}
POLICY
}

resource "aws_iam_policy_attachment" "hbl_data_insights_src_s3_replication_policy_attachment_01" {
  name       = "${local.aws_account_name}-data-insights-src-s3-replication-policy-attachment-01"
  roles      = [aws_iam_role.hbl_data_insights_src_s3_replication_role_01.name]
  policy_arn = aws_iam_policy.hbl_data_insights_src_s3_replication_policy_01.arn
}