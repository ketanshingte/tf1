# CREATING S3 BUCKET FOR HBL S3 SERVER ACCESS LOGGING
module "hbl_server_access_logging_s3_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//s3"

  bucket = "${local.aws_account_name}-server-access-logging-s3-01"
  acl = "private"
  restrict_public_buckets = true
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true

  versioning = {
    enabled = true
  }

#Server access logging
  logging = {
    target_bucket = module.hbl_server_access_logging_s3_02.s3_bucket_id
    target_prefix = "${local.aws_account_name}-server-access-logging-s3-01/"
  }
#Lifecycle rule
  lifecycle_rule = [
    {
        id       = "serveraccesslogs1"
        enabled = true

        expiration = {
            days = 7
            expired_object_delete_marker = false
        }

        noncurrent_version_expiration = {
            days = 7
        }
    }
  ]

#https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-server-access-logging.html
#Default server-side encryption with AWS Key Management Service (AWS KMS) keys (SSE-KMS) is not supported

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "AES256"
         }
    }
  }

   tags = merge(
    local.tags,
    { "application" = "hbl-server-access-logging-01" },
    { "teleport" = "${local.aws_account_name}-server-access-logging-s3-01" }
  )
}

resource "aws_s3_bucket_policy" "hbl_server_access_logging_s3_policy_01" {
  bucket = module.hbl_server_access_logging_s3_01.s3_bucket_id
  policy       = data.aws_iam_policy_document.hbl_server_access_logging_s3_policy_01.json
}

data "aws_iam_policy_document" "hbl_server_access_logging_s3_policy_01" {
  statement {
    sid = "AllowSSLRequestsOnly"

    principals {
      type = "*"
      identifiers = ["*"]
    }
    
    effect = "Deny"

    actions = ["s3:*"]

    resources = [
      "${module.hbl_server_access_logging_s3_01.s3_bucket_arn}",
      "${module.hbl_server_access_logging_s3_01.s3_bucket_arn}/*"
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = [
        "false"
        ]
    }
  }
  statement {
    sid = "S3 logging permissions"

    principals {
      type        = "Service"
      identifiers = ["logging.s3.amazonaws.com"]
    }

    effect = "Allow"

    actions = [
      "s3:PutObject"			
    ]

    resources = [
      "${module.hbl_server_access_logging_s3_01.s3_bucket_arn}/*"
    ]
  }
}


# CREATING S3 BUCKET FOR COLLECTING LOGS FROM SERVER LOGGING BUCKET 01
module "hbl_server_access_logging_s3_02" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//s3"

  bucket = "${local.aws_account_name}-server-access-logging-s3-02"
  acl = "private"
  restrict_public_buckets = true
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true

  versioning = {
    enabled = true
  }

#Server access logging
  logging = {
    target_bucket = module.hbl_server_access_logging_s3_01.s3_bucket_id
    target_prefix = "${local.aws_account_name}-server-access-logging-s3-02/"
  }

#Lifecycle rule
  lifecycle_rule = [
    {
        id       = "serveraccesslogs2"
        enabled = true

        expiration = {
            days = 7
            expired_object_delete_marker = false
        }

        noncurrent_version_expiration = {
            days = 7
        }
    }
  ]

#https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-server-access-logging.html
#Default server-side encryption with AWS Key Management Service (AWS KMS) keys (SSE-KMS) is not supported

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "AES256"
         }
    }
  }

   tags = merge(
    local.tags,
    { "application" = "hbl-server-access-logging-02" },
    { "teleport" = "${local.aws_account_name}-server-access-logging-s3-02" }
  )
}

resource "aws_s3_bucket_policy" "hbl_server_access_logging_s3_policy_02" {
  bucket = module.hbl_server_access_logging_s3_02.s3_bucket_id
  policy       = data.aws_iam_policy_document.hbl_server_access_logging_s3_policy_02.json
}

data "aws_iam_policy_document" "hbl_server_access_logging_s3_policy_02" {
  statement {
    sid = "AllowSSLRequestsOnly"

    principals {
      type = "*"
      identifiers = ["*"]
    }
    
    effect = "Deny"

    actions = ["s3:*"]

    resources = [
      "${module.hbl_server_access_logging_s3_02.s3_bucket_arn}",
      "${module.hbl_server_access_logging_s3_02.s3_bucket_arn}/*"
    ]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = [
        "false"
        ]
    }
  }
  statement {
    sid = "S3 logging permissions"

    principals {
      type        = "Service"
      identifiers = ["logging.s3.amazonaws.com"]
    }

    effect = "Allow"

    actions = [
      "s3:PutObject"			
    ]

    resources = [
      "${module.hbl_server_access_logging_s3_02.s3_bucket_arn}/*"
    ]
  }
}