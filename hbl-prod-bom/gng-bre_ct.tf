# # SECURITY GROUP FOR GNG BRE CT ASG
# module "hbl_gng_bre_ct_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-bre_ct-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range "
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-bre_ct-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR GNG BRE CT ASG
# module "hbl_gng_bre_ct_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-bre_ct-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCm+zw7ckEVgG5lT9VrWg5fA0VNgdjXVPpc2qns3yEveylHCVfcTeOZgQZriA9xGRDCYuGC/i4YELwWLCkZBBg6hq2bT1KrVwJ3N1geJ+0x+IatH8J2ctUR0wqADA7HCsZlHnplPPSicNlEwzri/CasuLx0Gni9mdzSVbIeMppbBAi+wwC39SjwJ2isFvuwMdlykFQiaa9GsooU9Q6wYnQ0vmrxlGNQKhH1ndkAJP5qZtyD0a/z8Q5mxjMCWGsDT/pesFEpqJKiDDZvcWcJ3LmmJDyweQD+7+9GOz6WAeQ1Bo23fdyilukJELuS0/z1CFlaFP3VRMCw2C/IsyIeGwWoeOKOua8jZ1MPho9mXnB1EExcwiDjUlMLsrYxSqJQofI8yVbWiWM1IKhsouKW53KypKtoGXaMqxrBLDhbtQNnjXmVmztg7yM5PZYJtCtMma1JglBfB2lWFcpz3AML2Mt+Sd1FXSS5kSUVzIAvYS7KxQSI3hugNEkd46jLw2q0CYk="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-bre_ct-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR GNG BRE CT ASG
# module "hbl_gng_bre_ct_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-gng-bre_ct-app-lt"
#   security_groups    = [module.hbl_gng_bre_ct_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-gng-bre_ct-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   instance_type     = "c5a.xlarge" ## TO BE CHANGED TO c6a.xlarge
#   key_name          = module.hbl_gng_bre_ct_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "gng-bre_ct-app"},
#           { "teleport" = "hbl-gng-bre_ct-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "gng-bre_ct-app"},
#           { "teleport" = "hbl-gng-bre_ct-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "gng-bre_ct-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-gng-bre_ct-app"}
#     )
# }

# #ASG FOR THE GNG BRE CT
# module "hbl_gng_bre_ct_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-gng-bre_ct-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 3
#   max_size                  = 0 # TO BE CHANGED TO 3
#   desired_capacity          = 0 # TO BE CHANGED TO 3
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_gng_bre_ct_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_gng_bre_ct_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   tags = merge(local.tags, {
#     	application = "gng-bre_ct-app"
#   },
#   { "teleport" = "hbl-gng-bre_ct-app"})
# }