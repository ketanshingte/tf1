# #LOCALS FOR MBU EDW APP
# locals {
#   ami_id_mbu_edw_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_edw_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU EDW APP CLUSTER #############
# ################################################

# #KEYPAIR MBU EDW CLUSTER
# module "hbl_mbu_edw_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-edw-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCfeE3oDVNVqZ6a9V8wJdoNv5LiO39lkM5LH9q8FiD29KLCYG8FSPaGiRWVtKwTrz6o7PlIevjrBfvbxbOq36NhkbpNsMGCPmG32mUb8tVtHuenE78sp14oIgZlVjUlbb3kM1+LfWwsixuj0sdwb1kllbhInqr9l1beCfONHt3d6s+sGeM/vgxNk4HFXZACgHF2IGf0x6b4CgFafPtgHbLnz4ujZj6WRm9iaTVgiB4ASbovEtrvoS2hTAlmZ52cIEU/dBzw18QKAPJIm3Kz7v0aUFzm88hdwPCzZjxlwrcSRgf7N+EPHsTt1ur3HBg5DAQET6Aex+gp3l34Jp7LXEiWnetgyutr0/h1veSUtoeqa0duML7IApvYnfGCb/90ZH3MDjoWAX3QCjziqzpkda44pudBx/HYlDmWlR+D4WdQtcLPOETVaE9AIvNlc8QYqztT6+Eg9kGb54Y//yMIOX+EneUBNHC6GQMfr/felRwCnZxSOA+7xD4QxUn6diWApEc="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-edw-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU EDW APP CLUSTER
# module "hbl_mbu_edw_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "6"
#   name                    = "${local.aws_account_name}-mbu-edw-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_edw_app
#   instance_type           = "m5a.xlarge" # TO BE CHANGED TO m6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_edw_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_edw_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_edw_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 80
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-edw-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-edw-app"}
#   )
# }

# # SECURITY GROUP MBU EDW APP CLUSTER
# module "hbl_mbu_edw_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-edw-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-edw-app" },
#     { "tier" = "app" }
#   )
# }