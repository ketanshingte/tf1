# #LOCALS FOR GNG RED DB
# locals {
#   ami_id_gng_red_db          = "ami-018529ba8ab22ecf5"
#   data_subnet_ids_gng_red_db = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
#   data_subnet_az_gng_red_db  = ["${local.region}a", "${local.region}b", "${local.region}c"]
#   ## GNG REDIS
#   gng_red_db_count           = 6

#   ## GNG TW REDIS
#   gng_tw_red_db_count        = 3
# }


# ################################################
# #############  GNG RED DB CLUSTER #############
# ################################################

# #KEYPAIR GNG RED CLUSTER
# module "hbl_gng_red_db_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng-red-db-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwhQc/a2b2BcrlOe0qPEWM83/2vY3b6e6navGRkD+MPYSckJNAiOB7KwaURRiZHzwsFkCS7JSn8EmUIe8YV5DxMjiMccl9BUKLXIVyjZ/N0C0bvoeJu7LI3MUlPm7UX9uyPN+eD/CRJ/LO5MEzQBd/q62sUoL7m4Ed+REYzUmhcDQRTn4+OICJdsWnBR988c+3DqoDwlREGvAPWn8cWsRdubdAavZtfOLdQBeuY2UTE7JZ1mLNEQ9+tnlnbUAv4mU1CtPbPgQA8oZoSM9mb260DTHhTZOqNudos514rMNZeq8bOLK/YtY0vn14IWNsbz8OEc7ULoYHHmYZlGSW4wPN0PLKsv9ZszbtjwgWse+fxVlnADU9XKN04tLMmqAduMp0wZxf6zBss2lcbf/HLNBSTx5YyeTPcP/kAKbUwKRCDWQ81iX85WpkD7keEhmKpLn+yeA8pIJKlSsKfiRtfazwTQ1A4p/FehSk3vTZ8aS0WZdhphQcCr+o0MVXSKUmDdU="
#   tags = merge(
#     local.tags,
#     { "application" = "gng-red-db" },
#     { "tier" = "db" }
#   )
# }

# # EC2 GNG RED DB CLUSTER REFACTORED
# module "hbl_gng_red_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = local.gng_red_db_count
#   name                    = "${local.aws_account_name}-gng-red-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_gng_red_db
#   # instance_type           = "c5a.xlarge" # TO BE CHANGED TO c6a.xlarge
#   instance_type           = "c6a.large"
#   subnet_id               = element(local.data_subnet_ids_gng_red_db, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_red_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_red_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "gng-red-db" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-gng-red-db"}
#   )
# }

# # EBS GNG RED DB CLUSTER REFACTORED
# module "hbl_gng_red_db_ec2_01_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_gng_red_db_ec2_01]
#     count                   = local.gng_red_db_count

#     availability_zone = element(local.data_subnet_az_gng_red_db, count.index)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_gng_red_db_ec2_01[count.index].ec2_complete_id
#     tags = merge({Name : "${local.aws_account_name}-gng-red-db-ec2-${format("%02d", count.index + 1)}-ebs-01"},
#     { "application" = "gng-red-db" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-gng-red-db"},
#     local.tags)  
# }


# # SECURITY GROUP GNG RED DB CLUSTER
# module "hbl_gng_red_db_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-red-db-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 7001
#       to_port     = 7002
#       protocol    = "tcp"
#       description = "Opening 7001 & 7002 port for self sg"
#       source_security_group_id = "${module.hbl_gng_red_db_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 7001
#       to_port     = 7002
#       protocol    = "tcp"
#       description = "Opening 7001 & 7002 port for gng od app asg"
#       source_security_group_id = "${module.hbl_gng_od_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 7001
#       to_port     = 7002
#       protocol    = "tcp"
#       description = "Opening 7001 & 7002 port for gng app asg"
#       source_security_group_id = "${module.hbl_gng_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 17001
#       to_port     = 17002
#       protocol    = "tcp"
#       description = "Opening 17001 & 17002 port for self sg"
#       source_security_group_id = "${module.hbl_gng_red_db_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-red-db" },
#     { "tier" = "db" }
#   )
# }


# ################################################
# #############  GNG TW RED DB CLUSTER ###########
# ################################################
# # EC2 GNG TW RED DB CLUSTER
# module "hbl_gng_tw_red_db_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = local.gng_tw_red_db_count
  
#   name                    = "${local.aws_account_name}-gng-tw-red-db-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_gng_red_db
#   instance_type           = "c5a.2xlarge" # TO BE CHANGED c6a.2xlarge 
#   subnet_id               = element(local.data_subnet_ids_gng_red_db, count.index)
#   vpc_security_group_ids  = [module.hbl_gng_tw_red_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_gng_red_db_kp_01.key_pair_key_name
#   root_volume_size      = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   volume_tags = local.tags
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "gng-tw-red-db" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-gng-red-db" }
#   )
# }

# # EBS BLOCK FOR GNG TW RED DB CLUSTER
# module "hbl_gng_tw_red_db_ebs_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#   depends_on        = [module.hbl_gng_tw_red_db_ec2_01]
#   count             = local.gng_tw_red_db_count
#   type = "gp3"
#   tags = merge({Name : "${local.aws_account_name}-gng-tw-red-db-ebs-01"},local.tags)

#   availability_zone = element(local.data_subnet_az_gng_red_db, count.index)
#   size              = 20
#   device_name       = "/dev/sdf"
#   instance_id       = module.hbl_gng_tw_red_db_ec2_01[count.index].ec2_complete_id
# }

# # SECURITY GROUP GNG TW RED DB CLUSTER
# module "hbl_gng_tw_red_db_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng-tw-red-db-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 7001
#       to_port     = 7002
#       protocol    = "tcp"
#       description = "Opening 7001 & 7002 port for self sg"
#       source_security_group_id = "${module.hbl_gng_tw_red_db_sg_01.security_group_id}"
#     },
#     {
#       from_port   = 7001
#       to_port     = 7002
#       protocol    = "tcp"
#       description = "Opening 7001 & 7002 port for gng_tw-app-asg"
#       source_security_group_id = "${module.hbl_gng_tw_app_sg.security_group_id}"
#     },
#     {
#       from_port   = 17001
#       to_port     = 17002
#       protocol    = "tcp"
#       description = "Opening 17001 & 17002 port for self sg"
#       source_security_group_id = "${module.hbl_gng_tw_red_db_sg_01.security_group_id}"
#     },
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng-tw-red-db" },
#     { "tier" = "db" }
#   )
# }
