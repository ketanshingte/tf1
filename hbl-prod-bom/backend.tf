##TERRAFORM STATE IS STORED IN S3 AND THE LOCKING HAPPENS VIA THE DYNAMODB
terraform {
  backend "s3" {
    bucket = "shared-terraformstate-infra-s3-1"
    key    = "hblprod.tfstate"
    region = "ap-south-1"
    encrypt        = true
    dynamodb_table = "sharedservices-terraformstate-infra-dynamodb-1"
    role_arn       = "arn:aws:iam::105828129156:role/terraform-admin"
  }
}
