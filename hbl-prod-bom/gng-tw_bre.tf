# # SECURITY GROUP FOR GNG TW BRE ASG
# module "hbl_gng_tw_bre_app_sg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-gng_tw-bre-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "gng_tw-bre-app" },
#     { "tier" = "app" }
#   )
# }

# # KEYPAIR FOR GNG TW BRE ASG
# module "hbl_gng_tw_bre_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-gng_tw-bre-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCldIH7f05kjN6zgr46MBiLWgS4Ln0F1mvLjMKGdwx66LzVwz3BT8vALNRhOEzVd2j769Q/k7Od2hh7nA9MuJsUSfTmzti2Dop83PFKGPHmITTYND2WUIOPKGreQ+CjF/PBuKoVrl6O4m/YSTFP698Q26s+eboM0Asqcu6l0LyUxhwTTD+IZQHK9ERw6uOgmPFbJ4u3cwVQUfiM07wqZoEPDq9Rxa5grm47wp8PkVhUj1DeYMHgTtNWUyn0EtMDwcIr/Z2XVPOjmF1SUxlBP5Vk7awdzPVLgu93HIvflT+Ikr9vkQrcYDqKlBwWc7dwFpi9VgTSM7i4ADgtATX1oJ3hy/Pr2vI5Kp5iNwjN+Xq85zaTQ4zSqah6z8c63Yx7SeDb9T+tNDvepb3QUy3kNBw8aKORVvCzhymTEpIWRRTXPPJeMgdOjfdBEW04onw9b06x1UnwRAX2ozFQ6KlRGKutHSfJ7ydDNT9mGq7koRu9hQye+IJEAWb/GPv2+8uah5k="
#   tags = merge(
#     local.tags,
#     { "application" = "gng_tw-bre-app" },
#     { "tier" = "app" }
#   )
# }

# #LAUNCH TEMPLATE FOR GNG TW BRE ASG
# module "hbl_gng_tw_bre_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-gng_tw-bre-app-lt"
#   security_groups    = [module.hbl_gng_tw_bre_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false 
#   launch_template_name        = "${local.aws_account_name}-gng_tw-bre-app-lt-01"
#   image_id          = "ami-0143cc1040af5cb1c"
#   #instance_type     = "c6a.2xlarge" #TO BE CHANGED TO THIS CONFIGURATION WHEN PROD GOES LIVE
#   instance_type     = "c6a.xlarge"
#   key_name          = module.hbl_gng_tw_bre_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
#   block_device_mappings = [
#     {
#       # Root volume
#       device_name = "/dev/xvda"
#       no_device   = 0
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 20
#         volume_type           = "gp3"
#       }
#       },
#       {
#       device_name = "/dev/sda1"
#       no_device   = 1
#       ebs = {
#         delete_on_termination = true
#         encrypted             = true
#         volume_size           = 30
#         volume_type           = "gp3"
#       }
#     }
#   ]
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           {"application" = "gng_tw-bre-app"},
#           { "teleport" = "hbl-gng_tw-bre-app"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "gng_tw-bre-app"},
#           { "teleport" = "hbl-gng_tw-bre-app"}
#         )
#       },
#     ]

#   tags = merge(
#     local.tags,
#       {"application" = "gng_tw-bre-app"},
#       {"tier" = "target-group"},
#       { "teleport" = "hbl-gng_tw-bre-app"}
#     )
# }

# #ASG FOR THE GNG TW BRE
# module "hbl_gng_tw_bre_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-gng_tw-bre-app-asg-01"
#   use_name_prefix = false

#   min_size                  = 0 # TO BE CHANGED TO 4
#   max_size                  = 0 # TO BE CHANGED TO 4
#   desired_capacity          = 0 # TO BE CHANGED TO 4
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_gng_tw_bre_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_gng_tw_bre_app_lt.launch_template_name
#   launch_template_version = "$Latest"
  
#   # LOADBALANCER -  FROM LOADBALANCER MODULE
#   target_group_arns = [module.hbl_shared_prod_app_alb.target_group_arns[12]]
  
#   tags = merge(local.tags, {
#     	application = "gng_tw-bre-app"
#   },
#   { "teleport" = "hbl-gng_tw-bre-app"})
# }