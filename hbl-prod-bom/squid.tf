# ##CREATING THE SECURITY GROUP USED WITH SQUID ASG
# module "hbl_squid_app_sg" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name        = "${local.aws_account_name}-squid-app-sg-01"
#   description = "Security Group for squid"
#   vpc_id      = module.hbl_networking_vpc_01.vpc_id
#   use_name_prefix = false
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 3128
#       to_port     = 3128
#       protocol    = "tcp"
#       description = "allowing port 3128 to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
  
#  egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "Allow all outgoing"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]

#   tags = merge(
#     local.tags,
#     {"application" = "squid"},
#   )
# }

# # KEYPAIR FOR SQUID SERVER
# module "hbl_squid_app_kp" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-squid-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFeYYSCeohhJdk5e9RYqYMA0yQAqw1T4+6mn4Y4gW55GXPxXKOuKc2Y0hN4uOI3y3Y1icLhK+nnHSe57nWf/N4+g5nLyyp/lNCwyzjryxldyn1CVYbLqTN/UQJXtt7jPMjKe3N1+WPcOOXdXLcyGpecRmj4et7w3Yl0OG7D5W4Mgn7/EiZE+KXp/f/Da7/K7ZIGEBVqNGtJ0jnHogQRa8TbIeH9QCEUU0NW9YNCXafaEQz+NftArJ7jSXgOEga6Ug4VjJnWc0HxDCB7yqSKIIsch+xYKv3zGJc/kf859lp/3he2SlltQaIGnJ/wT9YUL3iLwTOrITyFieSpEuH/8FuYMLMWYXx/i9CeFJEP72wUo8qaowo5ylKoDmLnp7BjV4vPACVBFFGcuRR+79eFO+995u8l4kMxZE1noL/z9lQ/wcIEMzyvlwTHWPFpDXRRfxDIqUFxp8J4q+UXOQYmOfRZzzUt6CXUQliQTjntMawkUzUKJRKV6uDs5gOlDLuPlc="
#   tags = merge(
#     local.tags,
#     { "application" = "squid" },
#     { "tier" = "app" }
#   )
# }

# ##CREATING THE SQUID LAUNCH TEMPLATE
# module "hbl_squid_app_lt" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//launch-template"
#   # AUTOSCALING GROUP
#   create = false
 
#   # LAUNCH TEMPLATE
#   name = "hbl-squid-app-lt-01"
#   security_groups    = [module.hbl_squid_app_sg.security_group_id]
#   create_launch_template	  = true
#   update_default_version      = true
#   launch_template_use_name_prefix = false
#   launch_template_name        = "${local.aws_account_name}-squid-app-lt-01"
#   image_id          = "ami-0ef62c01af2e63a97"
#   instance_type     = "t3a.small"
#   key_name = module.hbl_squid_app_kp.key_pair_key_name
#   iam_instance_profile_arn = aws_iam_instance_profile.ec2_access_instance_profile.arn
  
#   user_data        = base64encode(var.teleport_userdata_asg["userdata"])
#   metadata_options = {
#     http_endpoint               = "enabled"
#     http_tokens                 = "required"
#     http_put_response_hop_limit = 32
#     instance_metadata_tags      = "enabled"
#   }
#   tag_specifications = [
#       {
#         resource_type = "instance"
#         tags = merge(
#           local.tags,
#           {"application" = "squid"},
#           { "teleport" = "hbl-squid"}
#         )
#       },
#       {
#         resource_type = "volume"
#         tags = merge(
#           local.tags,
#           {"application" = "squid"},
#           {"Name" = "squid"},
#           { "teleport" = "hbl-squid"}
#         )
#       },
#     ]


#   tags = merge(
#     local.tags,
#       {"application" = "squid"},
#       {"tier" = "target-group"}
#     )
# }

# ##CREATING THE SQUID AUTOSCALING GROUP
# module "hbl_squid_app_asg" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//asg"

#   # AUTOSCALING GROUP
#   name = "${local.aws_account_name}-squid-app-asg-01"
#   min_size                  = 0 # TO BE CHANGED TO 1
#   max_size                  = 0 # TO BE CHANGED TO 1
#   desired_capacity          = 0 # TO BE CHANGED TO 1
#   vpc_zone_identifier       = aws_subnet.app[*].id
#   enable_monitoring         = true
#   create_scaling_policy     = false
#   create_custom_scaling_policy_alarm = false
#   use_name_prefix = false
#   enabled_metrics = [
#     "GroupDesiredCapacity",
#     "GroupInServiceCapacity",
#     "GroupPendingCapacity",
#     "GroupMinSize",
#     "GroupMaxSize",
#     "GroupInServiceInstances",
#     "GroupPendingInstances",
#     "GroupStandbyInstances",
#     "GroupStandbyCapacity",
#     "GroupTerminatingCapacity",
#     "GroupTerminatingInstances",
#     "GroupTotalCapacity",
#     "GroupTotalInstances"]
#   termination_policies      = ["OldestLaunchTemplate"]
#   security_groups = [module.hbl_squid_app_sg.security_group_id]

#   # REFERENCING LAUNCH TEMPLATE NAME FROM CUSTOM MODULE LAUNCH TEMPLATE
#   launch_template = module.hbl_squid_app_lt.launch_template_name
#   launch_template_version = "$Latest"

#   # LOADBALANCER -  FROM LOADBALANCER MODULE ## NEEDS TO BE UPDATED
#   # target_group_arns = [module.hbl_app_nlb.target_group_arns[7]]

#   tags = merge(local.tags,
#     {application = "squid"},
#     { "teleport" = "hbl-squid"}
#   )
# }
