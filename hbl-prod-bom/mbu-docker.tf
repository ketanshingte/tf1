# #LOCALS FOR MBU DOCKER LEADER APP
# locals {
#   ami_id_mbu_docker_app          = "ami-060dcc5a77c84080f" 
#   app_subnet_ids_mbu_docker_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
#   app_subnet_az_mbu_docker_app  = ["${local.region}a", "${local.region}b", "${local.region}c"]
#   mbu_docldr_count = 2
#   mbu_docwkr_count = 14
# }

# ##########################################################
# #############  MBU DOCKER LEADER APP CLUSTER #############
# ##########################################################

# #KEYPAIR MBU DOCKER CLUSTER
# module "hbl_mbu_docker_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-docker-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCy5cjpiZMhW9KAufH85gXLBnUfLgTM0ANTdRZis2QKcLoEu2PZMM2QtiYwgpMSxFMYf61GENwUZ+6igdnvbaZbDO9JwpQJIhYC1hPKMRyQB8PvLgTbXZHcljmXdJDOJkkWdENgVePem36SM/vaixYWWIvxwJaqxruqorRLxuTXCwL55FlEQKLJvAAd5uADqx0r6NtO2mXZhyqHc+6JOTqkU8QCi8JMgkIjCkxnhG/dPPK7X9FHx0ty0KEyDe0eF+99JomWZqL5mVk45Lq+GsgJGDmjixHw7zZXXon11OFxeUWIgtfl7caZiAzI968DWZQNACzUWG49Od1sUOL2HRT3RlHCcR5NlAq+Qf9lXAPWT3JkVYGzj/6R4U9tFME5b6f3HBTiVZlBfTeHrPaFEORDkhxP7w5gpJn9GHNpK9XaeFGrC9nIAY6BCSQDi+o0Qol+hje7R1cex35GC1FWOTUNuNN2cWjo5CuydTdpin4v3PkHq9sqbliyaBJx0MCkHgs="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-docker-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU DOCKER LEADER APP CLUSTER
# module "hbl_mbu_docldr_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = local.mbu_docldr_count
#   name                    = "${local.aws_account_name}-mbu-docldr-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_docker_app
#   instance_type           = "c5a.xlarge" ## TO BE UPDATE TO c6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_docker_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_docldr_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_docker_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-docker-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-docker-app"}
#   )
# }

# # EBS VOLUME MBU DOCKER LEADER APP CLUSTER
# module "hbl_mbu_docldr_app_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mbu_docldr_app_ec2_01]
#     count = local.mbu_docldr_count

#     availability_zone = element(local.app_subnet_az_mbu_docker_app, count.index)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_mbu_docldr_app_ec2_01[count.index].ec2_complete_id  
# }


# # SECURITY GROUP MBU DOCKER LEADER APP CLUSTER
# module "hbl_mbu_docldr_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-docldr-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 8069
#       to_port     = 8069
#       protocol    = "tcp"
#       description = "allow 8069 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 8914
#       to_port     = 8914
#       protocol    = "tcp"
#       description = "allow 8914 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 9098
#       to_port     = 9098
#       protocol    = "tcp"
#       description = "allow 9098 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 6016
#       to_port     = 6016
#       protocol    = "tcp"
#       description = "allow 6016 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-docker-app" },
#     { "tier" = "app" }
#   )
# }

# ##########################################################
# #############  MBU DOCKER WORKER APP CLUSTER #############
# ##########################################################

# # EC2 MBU DOCKER WORKER APP CLUSTER
# module "hbl_mbu_docwkr_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = local.mbu_docwkr_count
#   name                    = "${local.aws_account_name}-mbu-docwkr-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_docker_app
#   instance_type           = "c5a.4xlarge" ## TO BE UPDATE TO c6a.4xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_docker_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_docwkr_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_docker_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-docker-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-docker-app"}
#   )
# }

# # EBS VOLUME MBU DOCKER WORKER APP CLUSTER
# module "hbl_mbu_docwkr_app_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_mbu_docwkr_app_ec2_01]
#     count = local.mbu_docwkr_count

#     availability_zone = element(local.app_subnet_az_mbu_docker_app, count.index)
#     size              = 20
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_mbu_docwkr_app_ec2_01[count.index].ec2_complete_id  
# }

# # SECURITY GROUP MBU DOCKER WORKER APP CLUSTER
# module "hbl_mbu_docwkr_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-docwkr-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 8069
#       to_port     = 8069
#       protocol    = "tcp"
#       description = "allow 8069 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 8914
#       to_port     = 8914
#       protocol    = "tcp"
#       description = "allow 8914 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 9098
#       to_port     = 9098
#       protocol    = "tcp"
#       description = "allow 9098 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#     {
#       from_port   = 6016
#       to_port     = 6016
#       protocol    = "tcp"
#       description = "allow 6016 port to summarized ip range for app subnet"
#       cidr_blocks = "10.26.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-docker-app" },
#     { "tier" = "app" }
#   )
# }