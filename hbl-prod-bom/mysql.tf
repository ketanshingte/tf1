# LOCALS FOR MYSQL
locals {
  ami_id_mysql          = "ami-0f272a9818dbebc28"
  data_subnet_ids_mysql = ["${aws_subnet.data[0].id}", "${aws_subnet.data[1].id}", "${aws_subnet.data[2].id}"]
  data_subnet_az_mysql  = ["${local.region}a", "${local.region}b", "${local.region}c"]
  lms_pdt_mysql_count = 1
}

##############################
######  MYSQL CLUSTER ########
##############################

# KEYPAIR FOR MYSQL CLUSTER 
module "hbl_lms_pdt_mysql_db_kp_01" {
  source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
  key_name   = "${local.aws_account_name}-lms-pdt-mysql-db-kp-01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDM/MuDZlmw1oC0njFxIENV67QoiR8IZsa3To5bOlmdWfjc8OJ062OAwG1f7Wk9gBqretalAGKuE2NnynUqzPOZZfjSeEtUgMDJCj0tOZ8LmZcHSaaeODOi7q4E9bCCfsuxOVmy/pxDTXGT1VOQL0G3i0FSyobAL8aLrgGGDOPUNZf2LR03i9e2zenO1R06YK+A17zusYtr2zO3MHyQmyZ5LX96Pq+G2kGLFptqbDN1J6DIoV3nlhv2oxDE1xcHL3XVkpU2XTgVrzuwjcbgxkMqzOQQlbrsQxQlV7mjXrdByWme+nQr+/mdc2j4f57NKs9ZX4kumg+eCfiQt2D1z3MeMgONmynQfWa8f0tZzH1vP13Vy+EakkpknjBEMR7+QZbDgAdQn4a6sjdHNW2kMdjhizIGMOc296w4rxWN4jqMZfTZjRJ0rl2cm2PSRjkhT6rJT3VSpOs0lTgnApvtQgF7fUaxfLw7NuB6XJs15xTXeHwaztwsQozgXgLM49xMmL8="
  tags = merge(
    local.tags,
    { "application" = "mysql" },
    { "tier" = "db" }
  )
}

# SECURITY GROUP FOR MYSQL CLUSTER
module "hbl_lms_pdt_mysql_db_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-lms-pdt-mysql-db-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  ingress_with_source_security_group_id = [
    # {
    #   from_port   = 1880
    #   to_port     = 1880
    #   protocol    = "tcp"
    #   description = "Opening port 1880 to gng_lms_app security group"
    #   source_security_group_id = "${module.hbl_gng_lms_app_sg.security_group_id}"
    # }
    ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  tags = merge(
    local.tags,
    { "application" = "mysql" },
    { "tier" = "db" }
  )
}

# EC2 MYSQL CLUSTER INSTANCE 01
module "hbl_lms_pdt_mysql_db_ec2_01" {
  source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
  count                   = local.lms_pdt_mysql_count
  name                    = "${local.aws_account_name}-lms_pdt-mysql-db-ec2-${format("%02d", count.index + 1)}"
  ami                     = local.ami_id_mysql
  instance_type           = "c6a.2xlarge"
  subnet_id               = element(local.data_subnet_ids_mysql, count.index)
  vpc_security_group_ids  = [module.hbl_lms_pdt_mysql_db_sg_01.security_group_id]
  iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
  user_data               = base64encode(var.teleport_userdata["userdata"])
  key_name                = module.hbl_lms_pdt_mysql_db_kp_01.key_pair_key_name
  root_volume_size = 20
  enable_volume_tags      = true
  disable_api_termination = true
  tags = merge(
    local.tags,
    { "application" = "mysql" },
    { "tier" = "db" },
    { "teleport" = "hbl-lms-pdt-mysql"}
  )
}

# EBS VOLUME FOR MYSQL CLUSTER INSTANCE 01
module "hbl_lms_pdt_mysql_db_ebs_01" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_lms_pdt_mysql_db_ec2_01]
    count = local.lms_pdt_mysql_count
    availability_zone = element(local.data_subnet_az_mysql, count.index)
    size              = 50
    device_name = "/dev/sdf"
    instance_id = module.hbl_lms_pdt_mysql_db_ec2_01[count.index].ec2_complete_id  
}

# EBS VOLUME FOR MYSQL CLUSTER INSTANCE 01
module "hbl_lms_pdt_mysql_db_ebs_02" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_lms_pdt_mysql_db_ec2_01]
    count = local.lms_pdt_mysql_count
    availability_zone = element(local.data_subnet_az_mysql, count.index)
    size              = 20
    device_name = "/dev/sdg"
    instance_id = module.hbl_lms_pdt_mysql_db_ec2_01[count.index].ec2_complete_id  
}

# EBS VOLUME FOR MYSQL CLUSTER INSTANCE 01
module "hbl_lms_pdt_mysql_db_ebs_03" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
    depends_on = [module.hbl_lms_pdt_mysql_db_ec2_01]
    count = local.lms_pdt_mysql_count
    availability_zone = element(local.data_subnet_az_mysql, count.index)
    size              = 50
    device_name = "/dev/sdh"
    instance_id = module.hbl_lms_pdt_mysql_db_ec2_01[count.index].ec2_complete_id  
}

# # EC2 MYSQL CLUSTER INSTANCE 02
# module "hbl_lms_pdt_mysql_db_ec2_02" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   name                    = "${local.aws_account_name}-lms_pdt-mysql-db-ec2-02"
#   ami                     = local.ami_id_mysql
#   instance_type           = "c6a.2xlarge"
#   subnet_id               = element(local.data_subnet_ids_mysql, 1)
#   vpc_security_group_ids  = [module.hbl_lms_pdt_mysql_db_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   user_data               = base64encode(var.teleport_userdata["userdata"])
#   key_name                = module.hbl_lms_pdt_mysql_db_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   tags = merge(
#     local.tags,
#     { "application" = "mysql" },
#     { "tier" = "db" },
#     { "teleport" = "hbl-lms-pdt-mysql"}
#   )
# }

# # EBS VOLUME FOR MYSQL CLUSTER INSTANCE 02
# module "hbl_lms_pdt_mysql_db_ec2_02_ebs_01" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_lms_pdt_mysql_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mysql, 1)
#     size              = 50
#     device_name = "/dev/sdf"
#     instance_id = module.hbl_lms_pdt_mysql_db_ec2_02.ec2_complete_id  
# }

# # EBS VOLUME FOR MYSQL CLUSTER INSTANCE 02
# module "hbl_lms_pdt_mysql_db_ec2_02_ebs_02" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_lms_pdt_mysql_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mysql, 1)
#     size              = 20
#     device_name = "/dev/sdg"
#     instance_id = module.hbl_lms_pdt_mysql_db_ec2_02.ec2_complete_id  
# }

# # EBS VOLUME FOR MYSQL CLUSTER INSTANCE 02
# module "hbl_lms_pdt_mysql_db_ec2_02_ebs_03" {
#     source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ebs"
#     depends_on = [module.hbl_lms_pdt_mysql_db_ec2_02]
#     availability_zone = element(local.data_subnet_az_mysql, 1)
#     size              = 50
#     device_name = "/dev/sdh"
#     instance_id = module.hbl_lms_pdt_mysql_db_ec2_02.ec2_complete_id  
# }

