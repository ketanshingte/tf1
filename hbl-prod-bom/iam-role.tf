#CREATING THE ROLE WHICH ALLOWS ACCESS OF EC2 INSTANCES WITHOUT SSH KEYS ie VIA SSM SERVICE
data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ec2_access_role" {
  name                = "${local.aws_account_name}-ec2-access-role-01"
  path               = "/system/"
  assume_role_policy  = data.aws_iam_policy_document.instance_assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"]
  tags = merge(local.tags,
  {"application"="session-manager"}
  )
}

resource "aws_iam_instance_profile" "ec2_access_instance_profile" {
  name = "${local.aws_account_name}-ec2-access-role-01"
  role = aws_iam_role.ec2_access_role.name
  tags = merge(local.tags,
  {"application"="session-manager"}
  )
}

## CREATING ECR READ POLICY FOR DOCKER SWARM
resource "aws_iam_role_policy" "ec2_access_role_ecr_read_policy" {
  name = "${local.aws_account_name}-ec2-access-role-ecr-read-policy-01"
  role = aws_iam_role.ec2_access_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:BatchGetImage",
                "ecr:ListTagsForResource",
                "ecr:DescribeImageScanFindings"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}

## CREATING CUSTOM SSM ASSUME ROLE POLICY FOR CROSS ACCOUNT ACCESS
resource "aws_iam_role_policy" "assume_role_policy_for_cross_account" {
  name = "${local.aws_account_name}-assume-role-cross-account-policy-01"
  role = aws_iam_role.ec2_access_role.id
 policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "*"
        }
    ]
}
EOF
}

#CREATING THE ROLE WHICH ALLOWS ACCESS OF AWS BACKUP FOR BACKUP POLICIES 
data "aws_iam_policy_document" "backup_assume_role_policy" {
  statement {
    actions       = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "hbl_backup_access_role" {
  name                = "${local.aws_account_name}-backup-access-role-01"
  assume_role_policy  = data.aws_iam_policy_document.backup_assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup",
                        "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"]
  tags                = merge(local.tags,
                        {"application"="aws-backup"}
                        )
}

###################################
## IAM ROLE FOR PACKER AMI CREATION
###################################
resource "aws_iam_role" "hbl_packer_cross_account_role" {
  name = "${local.aws_account_name}-packer-cross-account-role-01"
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::105828129156:root"
            },
            "Action": "sts:AssumeRole"
        }
    ]
  })
  inline_policy {
    name = "${local.aws_account_name}-packer-cross-account-policy-01"
    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "kms:Decrypt",
                "ec2:DescribeInstances",
                "ec2:CreateKeyPair",
                "ec2:DescribeRegions",
                "ec2:CreateImage",
                "ec2:CopyImage",
                "ec2:DescribeSnapshots",
                "kms:ReEncrypt*",
                "ec2:DeleteVolume",
                "kms:Encrypt",
                "ec2:CreateSecurityGroup",
                "ec2:DescribeVolumes",
                "ec2:CreateSnapshot",
                "kms:DescribeKey",
                "kms:CreateGrant",
                "ec2:DescribeInstanceStatus",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:TerminateInstances",
                "ec2:DescribeTags",
                "ec2:CreateTags",
                "ec2:RegisterImage",
                "ec2:RunInstances",
                "ec2:StopInstances",
                "ec2:DescribeSecurityGroups",
                "ec2:CreateVolume",
                "ec2:DescribeImages",
                "ec2:GetPasswordData",
                "ec2:DescribeSecurityGroupRules",
                "ec2:DescribeImageAttribute",
                "ec2:DeleteSecurityGroup",
                "kms:GenerateDataKey",
                "ec2:DescribeSubnets",
                "ec2:DeleteKeyPair"
            ],
            "Resource": "${data.terraform_remote_state.audit_tf_state.outputs.hbl_ebs_kms_arn}"
        }
    ]
    })
  }
tags = local.tags
}

####################################################################################
## IAM ROLE FOR SSM DOCUMENT TO AASUME ROLE FOR WAZUH AGENT AMI CONFIGURATION IN EC2
####################################################################################
data "aws_iam_policy_document" "hbl_wazuh_ami_ssm_doc_assume_role_policy" {
  statement {
     actions = ["sts:AssumeRole"]

     principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "hbl_wazuh_ami_ssm_doc_role" {
  name                = "${local.aws_account_name}-wazuh-ami-ssm-doc-role-01"
  path                = "/"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonSSMAutomationRole"]
  assume_role_policy  = data.aws_iam_policy_document.hbl_wazuh_ami_ssm_doc_assume_role_policy.json
  tags = local.tags
}

## CREATING WAZUH AGENT AMI CONFIGURATION POLICY FOR SSM DOCUMENT
resource "aws_iam_role_policy" "hbl_wazuh_ami_ssm_doc_policy" {
  name = "${local.aws_account_name}-wazuh-ami-ssm-doc-policy-01"
  role = aws_iam_role.hbl_wazuh_ami_ssm_doc_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "iam:PassRole"
            ],
            "Resource": "arn:aws:iam::239362962221:role/system/hbl-ec2-access-role-01"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "kms:decrypt",
                "kms:CreateGrant",
                "kms:GenerateDataKey",
                "kms:ReEncrypt*"
            ],
            "Resource": "arn:aws:kms:ap-south-1:122720288396:key/mrk-d1ec3d2ec1244f5198c62dfb4b2e0579"
        }
    ]
}
EOF

}