locals {
  sns_subscriptions_email_list = [ "harshjm@lentra.ai", "manju@lentra.ai" ]
  sns_subscriptions_security_email_list = [ "harshjm@lentra.ai", "manju@lentra.ai" ]
}
## CREATING SNS TOPIC AND SUBSCRIPTION FOR ALERTS
module "hbl_alerts_sns_topic_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//sns"
  
  #SNS TOPIC
  name         = "${local.aws_account_name}-alerts-sns-topic-01"
  display_name = "${local.aws_account_name}-alerts" 

  kms_master_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_others_kms_arn}"
  tags = local.tags

  #SNS TOPIC SUBSCRIPTION
  endpoint = local.sns_subscriptions_email_list  ## MAKE SURE TO CONFIRM SUBSCRIPTION BEFORE DESTROYING
}


## CREATING SNS TOPIC AND SUBSCRIPTION FOR SECURITY ALERTS
module "hbl_security_alerts_sns_topic_01" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//sns"
  
  #SNS TOPIC
  name         = "${local.aws_account_name}-security-alerts-sns-topic-01"
  display_name = "${local.aws_account_name}-security-alerts" 

  kms_master_key_id = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_others_kms_arn}"
  tags = local.tags

  #SNS TOPIC SUBSCRIPTION
  endpoint = local.sns_subscriptions_security_email_list  ## MAKE SURE TO CONFIRM SUBSCRIPTION BEFORE DESTROYING
}