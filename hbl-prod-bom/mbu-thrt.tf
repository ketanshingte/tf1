# #LOCALS FOR MBU THRT APP
# locals {
#   ami_id_mbu_thrt_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_thrt_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU THRT APP CLUSTER #############
# ################################################

# #KEYPAIR MBU THRT CLUSTER
# module "hbl_mbu_thrt_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-thrt-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDJvqLpBoM/VnXQVoK7HaCx56r8OslyAExkzklCkZl3/CUnYGf8JVDfz8EtT19lrlgtbrU+sn2N7/e1JmADW6cwxKhN129qks9xZnfxJoqs2yHKTwnib3Nv+PnBdfzgKBjbSWqzpe5U5sCBj/QMJizj8vVfUzFRERCjIGVALZ3pTLdcOgnzfrtMDuMh+87abG/2OnN54OFk58VDz42QIefTkLMt3FgwECZE5qL+oiaGpoAtUpb5XQWEHxkWQQZCWo1tbW2VQNBKryU1f8XpGzBwBcMv+537G5Q7MXVkMtzevo2gewUsVxL7lGsC1LM33zIFmNCNJNrtZMIFHmH0qS6o9GETpsGxD581PLAUcpyX4LeKmUg1SzZxhq8rzuuL8kda9T1j96Wt6qMP2hSxIhcsW0WzOes8KENjRRTjrn65iElWCfGk5m/mKYWXHiAx4QN4P308K289tn+OIF0cVDM4y+M1Tp4In4N+KzqxYfGy8TpqgrXjhDMbpx9j07Lolc8="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-thrt-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU THRT APP CLUSTER
# module "hbl_mbu_thrt_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "2"
#   name                    = "${local.aws_account_name}-mbu-thrt-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_thrt_app
#   instance_type           = "m5a.xlarge" # TO BE CHANGED TO m6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_thrt_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_thrt_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_thrt_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 60
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-thrt-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-thrt-app"}
#   )
# }

# # SECURITY GROUP MBU THRT APP CLUSTER
# module "hbl_mbu_thrt_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-thrt-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-thrt-app" },
#     { "tier" = "app" }
#   )
# }
