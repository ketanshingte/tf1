## HBL LENTRA DC DNS RESOLVER
module "hbl_lentradc_dns_resolver_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//route53/resolver-endpoint"

  direction          = "INBOUND"
  name               = "${local.aws_account_name}-lentradc-dns-resolver-01"
  security_group_ids = [module.hbl_lentradc_dns_resolver_sg_01.security_group_id]
  tags = merge(
    local.tags,
    { "tier" = "networking" }
  )

  ip_address = [
    {
      ip        = "10.26.13.100"
      subnet_id = "${aws_subnet.data[0].id}"
    },
    {
      ip        = "10.26.14.100"
      subnet_id = "${aws_subnet.data[1].id}"
    },
    {
      ip        = "10.26.15.100"
      subnet_id = "${aws_subnet.data[2].id}"
    },
  ]
}

# SECURITY GROUP HBL LENTRA DC RESOLVER
module "hbl_lentradc_dns_resolver_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-lentradc-dns-resolver-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 53
      to_port     = 53
      protocol    = "tcp"
      self        = true
      description = "Opening port 53 for lentradc"
      cidr_blocks = "172.30.58.8/29" #SUMMARY OF THE 3 DC DNS IPs. 172.30.58.10, 172.30.58.11, 172.30.58.12
    },
    {
      from_port   = 53
      to_port     = 53
      protocol    = "udp"
      self        = true
      description = "Opening port 53 for lentradc"
      cidr_blocks = "172.30.58.8/29" #SUMMARY OF THE 3 DC DNS IPs. 172.30.58.10, 172.30.58.11, 172.30.58.12
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = merge(
    local.tags,
    { "tier" = "networking" }
  )
}