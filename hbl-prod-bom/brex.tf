# #LOCALS FOR BREX APP
# locals {
#   ami_id_brex_app          = "ami-01d5d873fc42c8b6d"
#   app_subnet_ids_brex_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  BREX APP CLUSTER ################
# ################################################

# #KEYPAIR BREX CLUSTER
# module "hbl_brex_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-brex-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDt5+bm9y4CjAKToGQ6HyPwaSKNpuAeoGPhC/DvL2IVZ2nUaHa6g/YvSQY+NRsAJXFS8WoRGcCvbG1OH8xIEOeVqRRekIMk54mK5dm/gz8+2qGgAK9x883qSyGXM/zlo9qC4jceu8Qh/bp/4awFo89cf1eD6ONniVNmS5CHaOF8cNaaA+JDvlQPYC6Tmnob745mup8sJfPSyV8HkZ8NiAvRxLeKu0Av8Numufj32l8hU7ZoLoSBIXrKMR+2If5DtBMwkw2eN8vFR1PdczUa/eCdLtky46YGULuax/IKU7LlUIgnCutxSKbCHyqWOa5CkTG7y2uIs2o//5oaM3CjROMbkcu87D/iMWIGXQczarrUC3kEubTf3nNWxszc4QysectPSrD8abrkXJU2SY58L496pfjjh59esU5qC6AJrZM1hQI/ucQ99TWTNGoK5eph1grKEgPzDxV3RyswX1I1F4K7cuVBagjurjk+lET39pozfbNvGlUvJq8WAC5aYaryEIM="
#   tags = merge(
#     local.tags,
#     { "application" = "brex-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 BREX APP CLUSTER
# module "hbl_brex_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "6"
#   name                    = "${local.aws_account_name}-brex-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_brex_app
#   instance_type           = "m5a.xlarge" # TO BE CHANGED TO m6a.xlarge
#   subnet_id               = element(local.app_subnet_ids_brex_app, count.index)
#   vpc_security_group_ids  = [module.hbl_brex_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_brex_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "brex-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-brex-app"}
#   )
# }

# # SECURITY GROUP BREX APP CLUSTER
# module "hbl_brex_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-brex-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "brex-app" },
#     { "tier" = "app" }
#   )
# }