# #LOCALS FOR MBU SOA APP
# locals {
#   ami_id_mbu_soa_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_soa_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU SOA APP CLUSTER ############
# ################################################

# #KEYPAIR MBU SOA CLUSTER
# module "hbl_mbu_soa_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-soa-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+oW1xDs6QqChWaWNPCGHWG/AvX4KXfRBmgSOxiay2phQY67UlQ3NBK2xCDqgwIVvbFzIzGtEyZaORa1cSQi+2kWPL5IvnMDSuOPvwmWK2Y+F409IhgkdcjzYg1rrJcCNWmG/Oqyf5o8zzSgA9WRhr259eUWVmZ6FTMfnCUDzIOCZ+aYr0C5qU7vjzPqdCSHkuP8bTMGfTELKVMPYdNlb8stHbVkTBput7IqFm9z/9snh4Mjk4FO+zhxphYi2hD0IHY1eDs5q6CXyeYXpEQz5T2RIn+NtUkM0GgtsKAWtsAr9OlZMKfupWsMUpruBtvG1c0OeI6TKJA6LaLnjsnodtApY0mDHhel7jzz2D8AdJ64zqtxbv0u+dST3F1C+2suR4sVi0Y283JMIlSD+iVcmwImk5i+eiqEc0BHC05FKTY+riReQOAs94OErgAeNnzDq9UjR2argIVXgO407yIDkstbT9KcYasd83jEDSrdsqVUx8E7b41LO4Y5CXraHUd0c="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-soa-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU SOA APP CLUSTER
# module "hbl_mbu_soa_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "2"
#   name                    = "${local.aws_account_name}-mbu-soa-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_soa_app
#   instance_type           = "c5a.2xlarge" # TO BE CHANGED TO c6a.2xlarge
#   subnet_id               = element(local.app_subnet_ids_mbu_soa_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_soa_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_soa_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-soa-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-soa-app"}
#   )
# }

# # SECURITY GROUP MBU SOA APP CLUSTER
# module "hbl_mbu_soa_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-soa-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-soa-app" },
#     { "tier" = "app" }
#   )
# }