######ALL ONE TIME IMPLEMENTATION ACTIVITIES GOES HERE### 

###VPC ATTACHMENT - ATTACHING THE VPC TO THE TRANSIT GATEWAY
module "hbl_networking_vpc_01_tgw_attachment" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//tgw-attachment"

  vpc_id             = module.hbl_networking_vpc_01.vpc_id
  subnet_ids         = module.hbl_networking_vpc_01.tgw_subnets
 # transit_gateway_id = data.terraform_remote_state.tgw.outputs.network_common_transit_gateway_id #Needs to fix permission in S3 bucket to get this working. 
  appliance_mode_support = "enable"
  dns_support = "enable"
  transit_gateway_default_route_table_association = true # THIS IS USELESS IN THE CROSSACCOUNT VPC ATTACHMENT. SETTING IT TO TRUE TO AVOID THE ISSUE MENTIONED IN https://github.com/hashicorp/terraform-provider-aws/issues/8600
  transit_gateway_default_route_table_propagation = true # THIS IS USELESS IN THE CROSSACCOUNT VPC ATTACHMENT. SETTING IT TO TRUE TO AVOID THE ISSUE MENTIONED IN https://github.com/hashicorp/terraform-provider-aws/issues/8600
  
  tags = merge(local.tags,
          {Name = "hbl-networking-vpc-01",
           "application" = "transit-gateway"}
        ) 
}

####CREATING KMS GRANT FOR THE AUTOSCALING 
resource "aws_kms_grant" "kms_grant_autoscaling" {
  name              = "kms-grant-autoscaling"
  key_id            = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_ebs_kms_arn}"
  grantee_principal = "arn:aws:iam::${local.aws_account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  operations        = ["Encrypt","Decrypt", "ReEncryptFrom", "ReEncryptTo", "GenerateDataKey", "GenerateDataKeyWithoutPlaintext", "DescribeKey", "CreateGrant"]
}

## CREATING ROUTE 53 PRIVATE HOSTED ZONE VPC ASSOCIATION
module "network_route53_zone_association_serviceurl_aws" {
  depends_on = [module.hbl_networking_vpc_01]
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//route53//association"

  vpc_id = module.hbl_networking_vpc_01.vpc_id
  vpc_region = local.region
  zone_id = "${data.terraform_remote_state.network_tf_state.outputs.network_route53_private_zone_aws_serviceurl_internal_zone_id}"
}

## CREATING SECURITY GROUP SHARED COMMON
module "hbl_shared_common_sg_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
  name   = "${local.aws_account_name}-shared-common-sg-01"
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "allow ssh port to sharedservices app subnet range for jenkins"
      cidr_blocks = "10.81.8.0/21"
    },
    {
      from_port   = 10050
      to_port     = 10050
      protocol    = "tcp"
      description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
      cidr_blocks = "10.81.8.0/21"
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = -1
      description = "allow all traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = local.tags
}

## CREATING ROUTE 53 RESOLVER QUERY LOG CONFIG AND ASSOCIATION
module "hbl_r53_resolver_query_log_config" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//route53//resolver-query-log-config"

    name = "${local.aws_account_name}-r53-resolver-query-log-config-01"
    destination_arn = "${data.terraform_remote_state.logs_tf_state.outputs.logs_r53_resolver_query_log_s3_bucket_arn}"
    tags = local.tags
}

module "hbl_r53_resolver_query_log_config_association" {
    source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//route53//resolver-query-log-config-association"

    resolver_query_log_config_id = module.hbl_r53_resolver_query_log_config.id
    resource_id = module.hbl_networking_vpc_01.vpc_id
}

### CREATING IAM PASSWORD POLICY
module "hbl_aws_iam_password_policy"{
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//iam//sub-module//iam-password-policy"
  minimum_password_length        = 14
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
  password_reuse_prevention      = 24
  hard_expiry                    = true
  max_password_age               = 90
}

## BLOCKING S3 ALL PUBLIC ACCESS
resource "aws_s3_account_public_access_block" "aws_s3_account_public_access_block" {
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}