resource "aws_ebs_encryption_by_default" "ebs_encryption" {
  enabled = true
}

resource "aws_ebs_default_kms_key" "ebs_encryption_key" {
  key_arn = "${data.terraform_remote_state.audit_tf_state.outputs.hbl_ebs_kms_arn}"
}
