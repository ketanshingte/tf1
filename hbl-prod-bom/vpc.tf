### CREATING THE VPC FOR THE HBL PRODUCTION ACCOUNT
module "hbl_networking_vpc_01" {
  source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//vpc"

  name = "${local.aws_account_name}"
  cidr = "10.${local.vpc_unique_cidr}.0.0/16"
  azs = ["${local.region}a", "${local.region}b", "${local.region}c"]
  tags = merge(local.tags, {
        tier = "networking"
  })
  vpc_tags = {
    Name= "${local.aws_account_name}-networking-vpc-01"
  }

  ## VPC FLOW LOGS
  enable_flow_log           = true
  flow_log_destination_type = "s3"
  flow_log_destination_arn  = "${data.terraform_remote_state.logs_tf_state.outputs.logs_vpc_flow_log_s3_bucket_arn}/${local.aws_account_name}"
  flow_log_traffic_type     = "REJECT"

# TRANSIT GATEWAY SUBNET RELATED VARIABLES
  tgw_subnets = ["10.${local.vpc_unique_cidr}.1.0/24", "10.${local.vpc_unique_cidr}.2.0/24", "10.${local.vpc_unique_cidr}.3.0/24"]

# LOADBALANCER SUBNET RELATED VARIABLES
  private_subnets     = ["10.${local.vpc_unique_cidr}.4.0/24", "10.${local.vpc_unique_cidr}.5.0/24", "10.${local.vpc_unique_cidr}.6.0/24"]
  private_dedicated_network_acl = true
  private_subnet_suffix = "loadbalancer"
  private_subnet_tags = merge(
    local.tags,
    {"application" = "loadbalancer"},
  )

}

################################################################################################
### ADDITIONAL BLOCK FOR THE CREATION OF WEB SUBNETS BASED
resource "aws_subnet" "web" {
  count = length(var.azs)

  vpc_id                          = module.hbl_networking_vpc_01.vpc_id
  cidr_block                      = var.web_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null

  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-web-%s",
        element(var.azs, count.index),
      )
    },
    local.tags,
    {"application" = "web"},

  )
}

resource "aws_route_table" "web" {
  vpc_id   = module.hbl_networking_vpc_01.vpc_id
  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-web-subnets",
      )
    },
    local.tags,
    {"application" = "web"},
  )
}

resource "aws_route" "all_web_traffic_to_tgw" {
  route_table_id         = aws_route_table.web.id
  destination_cidr_block = "0.0.0.0/0"
  transit_gateway_id   = data.terraform_remote_state.network_tf_state.outputs.network_common_transit_gateway_id

  timeouts {
    create = "5m"
  }
  
  depends_on = [module.hbl_networking_vpc_01_tgw_attachment]

}

resource "aws_route_table_association" "web" {
  count = length(var.web_subnets) > 0 ? length(var.web_subnets) : 0

  subnet_id = element(aws_subnet.web[*].id, count.index)
  route_table_id = element(
    aws_route_table.web[*].id,
    count.index,
  )
}

resource "aws_network_acl" "web" {
  count =  length(var.web_subnets) > 0 ? 1 : 0
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  subnet_ids = aws_subnet.web[*].id

  tags = merge(
    { "Name" = "${local.aws_account_name}-web-subnets" },
    local.tags,
    {"application" = "na"},
  )
}

resource "aws_network_acl_rule" "web_inbound" {
  count = length(var.web_subnets) > 0 ? length(var.web_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.web[0].id

  egress          = false
  rule_number     = var.web_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.web_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.web_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.web_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.web_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.web_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.web_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.web_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.web_inbound_acl_rules[count.index], "ipv6_cidr_block", null)

}

resource "aws_network_acl_rule" "web_outbound" {
  count = length(var.web_subnets) > 0 ? length(var.web_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.web[0].id

  egress          = true
  rule_number     = var.web_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.web_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.web_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.web_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.web_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.web_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.web_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.web_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.web_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

################################################################################################
### ADDITIONAL BLOCK FOR THE CREATION OF APP SUBNETS
resource "aws_subnet" "app" {
  count = length(var.azs)

  vpc_id                          = module.hbl_networking_vpc_01.vpc_id
  cidr_block                      = var.app_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null

  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-app-%s",
        element(var.azs, count.index),
      )
    },
    local.tags,
    {"application" = "app"},

  )
}

resource "aws_route_table" "app" {
  vpc_id   = module.hbl_networking_vpc_01.vpc_id
  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-app-subnets",
      )
    },
    local.tags,
    {"application" = "app"},
  )
}

resource "aws_route" "all_app_traffic_to_tgw" {
  route_table_id         = aws_route_table.app.id
  destination_cidr_block = "0.0.0.0/0"
  transit_gateway_id   = data.terraform_remote_state.network_tf_state.outputs.network_common_transit_gateway_id

  timeouts {
    create = "5m"
  }

  depends_on = [module.hbl_networking_vpc_01_tgw_attachment]
}

resource "aws_route_table_association" "app" {
  count = length(var.app_subnets) > 0 ? length(var.app_subnets) : 0

  subnet_id = element(aws_subnet.app[*].id, count.index)
  route_table_id = element(
    aws_route_table.app[*].id,
    count.index,
  )
}

resource "aws_network_acl" "app" {
  count =  length(var.app_subnets) > 0 ? 1 : 0
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  subnet_ids = aws_subnet.app[*].id

  tags = merge(
    { "Name" = "${local.aws_account_name}-app-subnets" },
    local.tags,
    {"application" = "na"},
  )
}

resource "aws_network_acl_rule" "app_inbound" {
  count = length(var.app_subnets) > 0 ? length(var.app_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.app[0].id

  egress          = false
  rule_number     = var.app_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.app_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.app_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.app_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.app_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.app_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.app_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.app_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.app_inbound_acl_rules[count.index], "ipv6_cidr_block", null)

}

resource "aws_network_acl_rule" "app_outbound" {
  count = length(var.app_subnets) > 0 ? length(var.app_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.app[0].id

  egress          = true
  rule_number     = var.app_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.app_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.app_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.app_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.app_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.app_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.app_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.app_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.app_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}



################################################################################################
### ADDITIONAL BLOCK FOR THE CREATION OF DATA SUBNETS
resource "aws_subnet" "data" {
  count = length(var.azs)

  vpc_id                          = module.hbl_networking_vpc_01.vpc_id
  cidr_block                      = var.data_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null

  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-data-%s",
        element(var.azs, count.index),
      )
    },
    local.tags,
    {"application" = "data"},

  )
}

resource "aws_route_table" "data" {
  vpc_id   = module.hbl_networking_vpc_01.vpc_id
  tags = merge(
    {
      "Name" = format(
        "${local.aws_account_name}-data-subnets",
      )
    },
    local.tags,
    {"application" = "data"},
  )
}

resource "aws_route" "all_data_traffic_to_tgw" {
  route_table_id         = aws_route_table.data.id
  destination_cidr_block = "0.0.0.0/0"
  transit_gateway_id   = data.terraform_remote_state.network_tf_state.outputs.network_common_transit_gateway_id

  timeouts {
    create = "5m"
  }
  depends_on = [module.hbl_networking_vpc_01_tgw_attachment]
}

resource "aws_route_table_association" "data" {
  count = length(var.data_subnets) > 0 ? length(var.data_subnets) : 0

  subnet_id = element(aws_subnet.data[*].id, count.index)
  route_table_id = element(
    aws_route_table.data[*].id,
    count.index,
  )
}

resource "aws_network_acl" "data" {
  count =  length(var.data_subnets) > 0 ? 1 : 0
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  subnet_ids = aws_subnet.data[*].id

  tags = merge(
    { "Name" = "${local.aws_account_name}-data-subnets" },
    local.tags,
    {"application" = "na"},
  )
}

resource "aws_network_acl_rule" "data_inbound" {
  count = length(var.data_subnets) > 0 ? length(var.data_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.data[0].id

  egress          = false
  rule_number     = var.data_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.data_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.data_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.data_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.data_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.data_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.data_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.data_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.data_inbound_acl_rules[count.index], "ipv6_cidr_block", null)

}

resource "aws_network_acl_rule" "data_outbound" {
  count = length(var.data_subnets) > 0 ? length(var.data_inbound_acl_rules) : 0
  network_acl_id = aws_network_acl.data[0].id

  egress          = true
  rule_number     = var.data_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.data_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.data_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.data_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.data_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.data_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.data_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.data_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.data_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

# DEFAULT SECURITY GROUP INBOUND AND OUTBOUND RULES OF HBL VPC
resource "aws_default_security_group" "default_hbl_sg_01" {
  vpc_id = module.hbl_networking_vpc_01.vpc_id
  ingress = []
  egress  = []
}