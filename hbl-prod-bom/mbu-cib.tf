# #LOCALS FOR MBU CIB APP
# locals {
#   ami_id_mbu_cib_app          = "ami-0143cc1040af5cb1c"
#   app_subnet_ids_mbu_cib_app = ["${aws_subnet.app[0].id}", "${aws_subnet.app[1].id}", "${aws_subnet.app[2].id}"]
# }


# ################################################
# #############  MBU CIB APP CLUSTER #############
# ################################################

# #KEYPAIR MBU CIB CLUSTER
# module "hbl_mbu_cib_app_kp_01" {
#   source     = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//keypair"
#   key_name   = "${local.aws_account_name}-mbu-cib-app-kp-01"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6gPbgxoYEqSkKSWIv/q7PXqkNoIhHh/xRKF46LLzi7PXm1kT1Onz6qls8zjpLsQrfKfhcXsYWIFgEFf1r38YaZmkRqEbEX1XE+VDqMQO810VMg+zs31q28etits6K6zrNF0HAHN17ivwU2xUAexU7yEJl5vTdujSIm3l3T23Df6A3i8b7ygyYJTtImM/uKikNTjDxRFMwryloc6MF0MILZh7LnUtlxspPJ0m+n8UWZiT12426xnnnnEq6Z61CeemRSHThy0E59qo/cuB95XNY4HARDXkdJC0Ad8MI9tnpI7GSgBcxbBca/XWm2SomnBCtL/l/uKR5QgskxJj0AYQ7NEDIa9E6dO/LLJZ66uAfPNGcqZTZ5phm6ioAcvpx1JgUfpny1742WasmwVq1Et9pVkSz4VJ/qXqH6rBv7HWShce0HWfMNNzpXpvk0pClW3yxSzYBK7xA2maZRzcG+NwMt1s+HY7ICYKSpGkKZKjYVQwHnm0uWHjxeOyDRp4Gybs="
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-cib-app" },
#     { "tier" = "app" }
#   )
# }

# # EC2 MBU CIB APP CLUSTER
# module "hbl_mbu_cib_app_ec2_01" {
#   source                  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//ec2"
#   count                   = "4"
#   name                    = "${local.aws_account_name}-mbu-cib-app-ec2-${format("%02d", count.index + 1)}"
#   ami                     = local.ami_id_mbu_cib_app
#   instance_type           = "c5a.large" # TO BE CHANGED TO c6a.large
#   subnet_id               = element(local.app_subnet_ids_mbu_cib_app, count.index)
#   vpc_security_group_ids  = [module.hbl_mbu_cib_app_sg_01.security_group_id]
#   iam_instance_profile    = aws_iam_instance_profile.ec2_access_instance_profile.name
#   key_name                = module.hbl_mbu_cib_app_kp_01.key_pair_key_name
#   root_volume_size = 20
#   enable_volume_tags      = true
#   disable_api_termination = true
#   user_data_base64        = base64encode(var.teleport_userdata["userdata"])
#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp3"
#       volume_size = 30
#       encrypted   = true
#     }
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-cib-app" },
#     { "tier" = "app" },
#     { "teleport" = "hbl-mbu-cib-app"}
#   )
# }

# # SECURITY GROUP MBU CIB APP CLUSTER
# module "hbl_mbu_cib_app_sg_01" {
#   source = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//security-group"
#   name   = "${local.aws_account_name}-mbu-cib-app-sg-01"
#   vpc_id = module.hbl_networking_vpc_01.vpc_id
#   ingress_with_source_security_group_id = [
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       description = "Opening 443 port for alb sg"
#       source_security_group_id = "${module.hbl_prod_app_alb_sg_01.security_group_id}"
#     }
#   ]
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       description = "allow ssh port to sharedservices app subnet range for jenkins"
#       cidr_blocks = "10.81.8.0/21"
#     },
#     {
#       from_port   = 10050
#       to_port     = 10050
#       protocol    = "tcp"
#       description = "allow 10050 port to sharedservices app subnet summarized ip range for zabbix"
#       cidr_blocks = "10.81.8.0/21"
#     },
#   ]
#   egress_with_cidr_blocks = [
#     {
#       from_port   = -1
#       to_port     = -1
#       protocol    = -1
#       description = "allow all traffic"
#       cidr_blocks = "0.0.0.0/0"
#     },
#   ]
#   tags = merge(
#     local.tags,
#     { "application" = "mbu-cib-app" },
#     { "tier" = "app" }
#   )
# }