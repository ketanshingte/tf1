## CREATING NETWORK LOADBALANCER USED FOR ALL HBL APPLICATIONS
## ACCESS LOGS FOR THIS NLB IS DISABLED BECAUSE NLB DOESN'T SUPPORT ACCESS LOGS WITH TCP LISTNERS

module "hbl_shared_prod_app_nlb" {
  source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//loadbalancer"

  name = "${local.aws_account_name}-shared-prod-app-nlb"
  load_balancer_type = "network"
  internal      = true

  vpc_id             = module.hbl_networking_vpc_01.vpc_id
  subnets = module.hbl_networking_vpc_01.private_subnets
  enable_cross_zone_load_balancing = true

#   access_logs = {
#     bucket = "${data.terraform_remote_state.logs_tf_state.outputs.logs_loadbalancer_access_logs_s3_bucket_id}"
#     prefix = "${local.aws_account_name}/nlb/shared-hbl-app"
#   }

  # ATTRIBUTES
  enable_deletion_protection       = true

  
target_groups = [
    {
      name       = "${local.aws_account_name}-gng-nginx-irp-cluster-tg"
      backend_protocol   = "TCP"
      backend_port       = 80
      target_type        = "instance"
      preserve_client_ip = true
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
      }
    },
  ]

  http_tcp_listeners = [
    # Forward action is default, either when defined or undefined
    {
      port               = 80
      protocol           = "TCP"
     # certificate_arn    = module.serviceurl_acm.acm_certificate_arn
      action_type        = "forward"
      target_group_index = 0
    },
  ]

  tags = local.tags
  lb_tags = {
          tier = "loadbalancer"
      }

  target_group_tags = {
          application = "nginx"
          tier = "target-group"
      }
}

# # CREATING CLOUDWATCH ALARMS FOR NLB
# module "hbl_nlb_ClientTLSNegotiationErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-nlb-ClientTLSNegotiationErrorCount-alarm-01" 
#   alarm_description   = "ClientTLSNegotiationErrorCount nlb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_nlb.lb_arn_suffix
#   }

#   namespace   = "AWS/NetworkELB"
#   metric_name = "ClientTLSNegotiationErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_nlb_PortAllocationErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-nlb-PortAllocationErrorCount-alarm-01" 
#   alarm_description   = "PortAllocationErrorCount nlb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_nlb.lb_arn_suffix
#   }

#   namespace   = "AWS/NetworkELB"
#   metric_name = "PortAllocationErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_nlb_TargetTLSNegotiationErrorCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-nlb-TargetTLSNegotiationErrorCount-alarm-01" 
#   alarm_description   = "TargetTLSNegotiationErrorCount nlb alarm"
#   comparison_operator = "GreaterThanThreshold"
#   evaluation_periods  = 1
#   threshold           = 50
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_nlb.lb_arn_suffix
#   }

#   namespace   = "AWS/NetworkELB"
#   metric_name = "TargetTLSNegotiationErrorCount"
#   statistic   = "Sum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }

# module "hbl_nlb_UnHealthyHostCount_alarm_01" {
#   source  = "git::https://gitlab.serviceurl.in/lentra/cloud-ops/aws/terraform/modules-prod.git//cloudwatch/metric-alarm"

#   alarm_name          = "${local.aws_account_name}-nlb-UnHealthyHostCount-alarm-01" 
#   alarm_description   = "UnHealthyHostCount nlb alarm"
#   comparison_operator = "GreaterThanOrEqualToThreshold"
#   evaluation_periods  = 1
#   threshold           = 1
#   period              = 300

#   dimensions = {
#     LoadBalancer = module.hbl_shared_prod_app_nlb.lb_arn_suffix
#   }

#   namespace   = "AWS/NetworkELB"
#   metric_name = "UnHealthyHostCount"
#   statistic   = "Maximum"
#   tags = local.tags
#   alarm_actions = [module.hbl_alerts_sns_topic_01.sns_topic_arn]
# }