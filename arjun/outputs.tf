# web_server_sg Security Group Outputs
output "web_server_sg_group_id" {
  description = "The ID of the security group"
  value       = module.web_server_sg.security_group_id
}
output "web-server_public_ip" {
  description = "List of Public ip address assigned to the instances"
  value       = module.ec2_complete.public_ip
}
output "private_subnet_id" {
  description = "List of Public ip address assigned to the instances"
  value       = module.vpc.private_subnets
}
output "public_subnet_id" {
  description = "List of Public ip address assigned to the instances"
  value       = module.vpc.public_subnets
}
output "vpc_id" {
  description = "List of Public ip address assigned to the instances"
  value       = module.vpc.vpc_id
}