#https://registry.terraform.io/modules/terraform-aws-modules/ec2-instance/aws/latest
module "ec2_complete" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.5.0"

  name                        = "Web_App"
  #ami                         = "ami-090f600a8e3543cec" ##WEB_AMI
  ami                         = "ami-0e1d06225679bc1c5" ##Default
  instance_type               = "t2.micro"
  key_name = "Akshay"
  vpc_security_group_ids = [module.web_server_sg.security_group_id]

  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      volume_size = 8
    },
  ]

   user_data     = "${file("install_httpd.sh")}"

}