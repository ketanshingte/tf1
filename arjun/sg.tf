########## Sercurity Group ########################
#https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/latest
module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "web_server_sg"
  description = "Security group for web-server"
  vpc_id      = "vpc-05e7fbfd814c6cd5a"

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "HTTP Port"
      cidr_blocks = "0.0.0.0/0"
     }
    ,
        {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "SSH Port"
      cidr_blocks = "0.0.0.0/0"
    }
   ]
}

